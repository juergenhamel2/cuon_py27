#!/usr/bin/python
# -*- coding: utf-8 -*- 
####### coding=utf-8

import bottle
from bottle import route, run,  request,  response
import time
import datetime
import ConfigParser

from xmlrpclib import ServerProxy

from xml.dom import minidom
from xml.dom.minidom import parse, parseString
from xml.dom import Node
import urllib2, urllib
from xml.dom.minidom import Document
import copy
from collections import Mapping
try:
    from Crypto.Cipher import AES
except:
    print 'No Crypto-Module found -- !'
import hashlib


Host = None
Port = None
Protocol = "http"


try:
    cps = ConfigParser.ConfigParser()
            
    cps.readfp(open( '/etc/cuon/server.ini'))
except:
    cps = None

# print "cps = ", cps            
if cps and cps.has_option("WEBSERVICES","HOST"):
   try:
       Host = cps.get("WEBSERVICES","HOST").strip()
       Port = cps.get("WEBSERVICES","PORT").strip()
       # print 'options = ',Host, Port
   except:
       Host = None
       Port = 0



class dict2xml(object):
   

    def __init__(self, structure):
        self.doc     = Document()
        if len(structure) == 1:
            rootName    = str(structure.keys()[0])
            self.root   = self.doc.createElement(rootName)

            self.doc.appendChild(self.root)
            self.build(self.root, structure[rootName])
           

    def build(self, father, structure):
        if type(structure) == dict:
            for k in structure:
                tag = self.doc.createElement(k)
                father.appendChild(tag)
                self.build(tag, structure[k])

        elif type(structure) == list:
            grandFather = father.parentNode
            tagName     = father.tagName
            grandFather.removeChild(father)
            for l in structure:
                tag = self.doc.createElement(tagName)
                self.build(tag, l)
                grandFather.appendChild(tag)

        else:
            data    = str(structure)
            tag     = self.doc.createTextNode(data)
            father.appendChild(tag)

    def display(self):
        print self.doc.toprettyxml(indent="  ")

    def getXML(self):
        return self.doc






class myxmlrpc:
    
    def __init__(self):


        sServer = Protocol + "://" + Host + ":" + "7000"
        self.server = ServerProxy(sServer,allow_none = 1)

    def NotTextNodeError(self):
        pass

    def getTextFromNode(self, node):
        """
        scans through all children of node and gathers the
        text. if node has non-text child-nodes, then
        NotTextNodeError is raised.
        """
        t = ""
        for n in node.childNodes:
            if n.nodeType == n.TEXT_NODE:
                t += n.nodeValue
            else:
                raise NotTextNodeError
        # print "text-node = ", t
        return t
    
    
    def nodeToDic(self, node):
        """
        nodeToDic() scans through the children of node and makes a
        dictionary from the content.
            1. three cases are differentiated.
                  - if the node contains no other nodes, it is a text-node
                    and {nodeName:text} is merged into the dictionary.
                  - if the node has the attribute "method" set to "true",
                    then it's children will be appended to a list and this
                    list is merged to the dictionary in the form: {nodeName:list}.
                  - else, nodeToDic() will call itself recursively on
                    the nodes children (merging {nodeName:nodeToDic()} to
                    the dictionary).
        """
        #print "nodeToDic", node.toxml()

        dic = {} 
        for n in node.childNodes:
            #print "node <-> ELEMENT", n.nodeType,  n.ELEMENT_NODE
            #print "node is ", n.toxml()
            #print "node-name = ", n.nodeName

            if n.nodeName == "struct":
                for n1 in n.childNodes:
                    #print "n1 nodename =", n1.nodeName
                    if n1.nodeName == "member":
                       for n2 in n1.childNodes:
                           #print "n2 nodename =", n2.nodeName
                           if n2.nodeName == "name":
                               dicA = self.getTextFromNode(n2)
                               #print "dicA = ", dicA
                           elif  n2.nodeName == "value":
                               for n3 in n2.childNodes:
                                   #print "n3 = ", n3.toxml()
                                   if n3.nodeName == "string":
                                       #print "found string value "
                                       dicB = self.getTextFromNode(n3)
                                       dic[dicA]=dicB[1:len(dicB)-1]
                                   
                    
            #print "multiple = ",  n.getAttribute("multiple")
            #if n.getAttribute("multiple") == "true" or n.getAttribute("multiple")== None :
                
             #   # node with multiple children:
              #  # put them in a list
               # l = []
                #for c in n.childNodes:
                 #   if c.nodeType != n.ELEMENT_NODE:
                  #      continue
                   # l.append(nodeToDic(c))
                   # dic.update({n.nodeName:l })
                   # continue
        
                #try:

                 #   print "get text node"
                  #  text = getTextFromNode(n)
                   # print "result text = ", text

                #except NotTextNodeError:
                    # 'normal' node
                 #   dic.update({n.nodeName:nodeToDic(n)})
                 #   continue
                
                # text node
                #dic.update({n.nodeName:text})
                #continue
        return dic
  
    def insertStruct(self, dicStruct):
        
        dicRet = {"struct":{} }
        iZ = 0
        for key in dicStruct.iterkeys():
            #print "dicRet in for = ", dicRet
            dicRet["struct"]["member["+`iZ`+"]"] = {"name":key}
            #,{"value":{"string":dicStruct[key]}}
            iZ += 1

            
        
        return dicRet


    def serialize(self, root):

        xml = ''
        for key in root.keys():
            if isinstance(root[key], dict):
                xml = '%s<%s>\n%s</%s>\n' % (xml, key, self.serialize(root[key]), key)
            elif isinstance(root[key], list):
                xml = '%s<%s>' % (xml, key)
                for item in root[key]:
                    xml = '%s%s' % (xml, self.serialize(item))
                xml = '%s</%s>' % (xml, key)
            else:
                value = root[key]
                xml = '%s<%s>%s</%s>\n' % (xml, key, value, key)
        return xml

    def readDocument(self, filename):
        #    reader = PyExpat.Reader()
        #    doc = reader.fromUri(filename)
        #build a DOM tree from the file
        #self.out("filename = " + `filename`)
        doc = None
        try:
            doc = minidom.parse(filename)
        except Exception, param:
                print 'unknown exception by read XML-document'
                # print `Exception`
                # print `param`
        
        #self.out("Document =  " + doc.toxml() )


        return  doc
              
    def readXmlString(self,  sXml):
        #print  "xml-String = ",urllib2.unquote(sXml)
        #print "parseXml = ", parseString(sXml)
        return parseString(urllib2.unquote(sXml))
        
    def getRootNode(self, doc):
        return doc.childNodes

    def printXml(self, xml1):
        print xml1.toxml()

 

    def callRP(self,  rp, *c):

        t1 = time.mktime(time.localtime())
        print "callRP " + rp 
        s = 'r = self.server.' + rp + '('
        for i in c:
            s = s + `i` + ', '
            #print '-------------------------------------------------'
            #print 'i = ', `i`
            #print '-------------------------------------------------'

        if len(c) > 0:
            s = s[0:len(s) -2]
        s = s + ')'
        # print 's = ',  s
        startRP = True
        rp_tries = 0
        #print 'Server by connection: ', self.getServer()
        #print 'Servercall by Connection: ', s
        #s = bz2.compress(s)
        #s = base64.encodestring(s)
        while startRP:
            try:
                exec s
                #print 'start this: ',  s
                startRP = False

            except IOError, param:
                print 'IO-Error'
                #print param

            except KeyError, param:
                print 'KEY-Error'
                # print param

            except Exception, param:
                print 'unknown exception'
                # print `Exception`
                # print param[0:200]

            if startRP:
                print 'error, next try'

                rp_tries = rp_tries + 1

                if rp_tries > 5:
                    startRP = False
                else:
                    print ' wait for 2 sec. '
                    print ' Try :' + `rp_tries`
                    time.sleep(2)
        if r and r == 'NONE':
            r = None
        print  '<-------xmlrpc need: ' + ` time.mktime(time.localtime()) -t1` 
        return    r


    def test(self):
        print "test it"

    def test2(self):
        return "test it2"


codes = {
" " : "%20",
"!" : "%21",
"\"" : "%22",
"#" : "%23",
"$" : "%24",
"%" : "%25",
"&" : "%26",
"'" : "%27",
"(" : "%28",
")" : "%29",
"*" : "%2A",
"+" : "%2B",
"," : "%2C",
"-" : "%2D",
"." : "%2E",
"/" : "%2F",
":" : "%3A",
";" : "%3B",
"<" : "%3C",
"=" : "%3D",
">" : "%3E",
"?" : "%3F",
"@" : "%40",
"[" : "%5B",
"\\" : "%5C",
"]" : "%5D",
"^" : "%5E",
"_" : "%5F",
"`" : "%60",
"{" : "%7B",
"|" : "%7C",
"}" : "%7D",
"~" : "%7E",
" " : "%7F",
"€" : "%80",
" " : "%81",
"‚" : "%82",
"ƒ" : "%83",
"„" : "%84",
"…" : "%85",
"†" : "%86",
"‡" : "%87",
"ˆ" : "%88",
"‰" : "%89",
"Š" : "%8A",
"‹" : "%8B",
"Œ" : "%8C",
" " : "%8D",
"Ž" : "%8E",
" " : "%8F",
" " : "%90",
"‘" : "%91",
"’" : "%92",
"“" : "%93",
"”" : "%94",
"•" : "%95",
"–" : "%96",
"—" : "%97",
"˜" : "%98",
"™" : "%99",
"š" : "%9A",
"›" : "%9B",
"œ" : "%9C",
" " : "%9D",
"ž" : "%9E",
"Ÿ" : "%9F",
" " : "%A0",
"¡" : "%A1",
"¢" : "%A2",
"£" : "%A3",
" " : "%A4",
"¥" : "%A5",
"|" : "%A6",
"§" : "%A7",
"¨" : "%A8",
"©" : "%A9",
"ª" : "%AA",
"«" : "%AB",
"¬" : "%AC",
"¯" : "%AD",
"®" : "%AE",
"¯" : "%AF",
"°" : "%B0",
"±" : "%B1",
"²" : "%B2",
"³" : "%B3",
"´" : "%B4",
"µ" : "%B5",
"¶" : "%B6",
"·" : "%B7",
"¸" : "%B8",
"¹" : "%B9",
"º" : "%BA",
"»" : "%BB",
"¼" : "%BC",
"½" : "%BD",
"¾" : "%BE",
"¿" : "%BF",
"À" : "%C0",
"Á" : "%C1",
"Â" : "%C2",
"Ã" : "%C3",
"Ä" : "%C4",
"Å" : "%C5",
"Æ" : "%C6",
"Ç" : "%C7",
"È" : "%C8",
"É" : "%C9",
"Ê" : "%CA",
"Ë" : "%CB",
"Ì" : "%CC",
"Í" : "%CD",
"Î" : "%CE",
"Ï" : "%CF",
"Ð" : "%D0",
"Ñ" : "%D1",
"Ò" : "%D2",
"Ó" : "%D3",
"Ô" : "%D4",
"Õ" : "%D5",
"Ö" : "%D6",
" " : "%20", #%D7
"Ø" : "%D8",
"Ù" : "%D9",
"Ú" : "%DA",
"Û" : "%DB",
"Ü" : "%DC",
"Ý" : "%DD",
"Þ" : "%DE",
"ß" : "%DF",
"à" : "%E0",
"á" : "%E1",
"â" : "%E2",
"ã" : "%E3",
"ä" : "%E4",
"å" : "%E5",
"æ" : "%E6",
"ç" : "%E7",
"è" : "%E8",
"é" : "%E9",
"ê" : "%EA",
"ë" : "%EB",
"ì" : "%EC",
"í" : "%ED",
"î" : "%EE",
"ï" : "%EF",
"ð" : "%F0",
"ñ" : "%F1",
"ò" : "%F2",
"ó" : "%F3",
"ô" : "%F4",
"õ" : "%F5",
"ö" : "%F6",
"÷" : "%F7",
"ø" : "%F8",
"ù" : "%F9",
"ú" : "%FA",
"û" : "%FB",
"ü" : "%FC",
"ý" : "%FD",
"þ" : "%FE",
"ÿ" : "%FF",
"" : "%00"
}


@route("/etc/?")
def forbidden():
    pass



@route('/')
def setRootSite():
    #return send_file('index.html', root='html/')
    abort(401, "Sorry, access denied.")

 

@route('/sayVersion4/:sVersion', method='POST')
def sayVersion4(sVersion):
    print 'sayVersion',  request
   
    return  '''<html><body>ok</body></html>''' 


@route('/sayVersion/', method='POST')
def sayVersion():
    print 'sayVersion',  request
   
    return  '''<html><body>ok</body></html>''' 


@route('/call/', method = 'POST')
def call():
    print "call"
    #print "env =",  request.environ
    xRet=None
    env22 =  request.environ['wsgi.input'].read(int(request.environ['CONTENT_LENGTH'])) 
    print "env22 ", env22

    liParams = env22[1:].split("&")
    #print liParams
    sCall = ""
    sParams = ""

    for i in liParams:
        liI = i.split("=")
        liI[0] = liI[0].strip()
        if liI[0].strip() == 'method_xmlrpc':
            sCall = liI[1].strip()
            #print sCall
            print
        else:
            
            print "liI 0-6 = ", liI[0][0:6]
            if liI[0][0:6] == "sparam":
                print "found sParam"
                s =  liI[1]

                for aCode in codes: 
                    s = s.replace(codes[aCode],aCode)
               
                sParams += "'" + s + "' , "
                
            elif liI[0][0:6] == "iparam":
                sParams +=  liI[1] + " , "
    
            elif liI[0][0:6] == "xparam":
                print "found xparam"
                s1 = myxmlrpc().readXmlString(liI[1])
                #print "s1 = ", s1
                #s2 = myxmlrpc().getRootNode(s1)
                #print "s2 = ", s2
                s3 =  myxmlrpc().nodeToDic(s1)
                #print "s3 = ", s3
                sParams += `s3`  + " , "
            
    sParams = sParams[0:len(sParams)-2]


    if sCall == "Database.createSessionID":

        m = hashlib.sha512("ieZafie7Char")
        exeCall = "xRet = myxmlrpc().callRP('" + sCall +"','Aquawiki','" + m.hexdigest()+  "')"
    else:

        exeCall = "xRet = myxmlrpc().callRP('" + sCall +"', " + sParams + ")"
    print "exeCall = ", exeCall
    #print "myRpc = ", myRPC
    #myxmlrpc().test()
    #myxmlrpc().test()
    #print  myxmlrpc().test2()

    #xRet = myxmlrpc().callRP('Database.createSessionID', 'test1' , 'test1' )
    

    exec exeCall
    print "xret = ", xRet
    if isinstance(xRet,(str,basestring,int, long, float, complex)):
        return xRet
    else:
        #xml1 = myxmlrpc().insertStruct(xRet)
        #print
        #print xml1
        if isinstance(xRet,(list) ):
            xRet = xRet[0]
        #print xRet
        

        return xRet


if Host and Port:
    run(port=int(Port), host=Host, reloader=False ) # This starts the HTTP server

