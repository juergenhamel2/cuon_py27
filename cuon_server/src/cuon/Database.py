# coding=utf-8
##Copyright (C) [2003-2012]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

from twisted.web import xmlrpc
import os
import sys
import time
import types
import random   
import xmlrpclib
import string
from basics import basics
from cuon.SQL import SQL
from ConfigParser import ConfigParser
import cPickle
import cuon.Windows.setOfEntries 
import hashlib
import StringIO


class Database(xmlrpc.XMLRPC, SQL):
    def __init__(self):
        basics.__init__(self)
        SQL.__init__(self)
        self.firstRecord = None
        
    def xmlrpc_getAnroidInfo(self, type, program, Major, Minor, Rev):
        sRet = False
        if self.Android[type][program]['Major'] < Major:
            return sRet
        elif self.Android[type][program]['Minor'] < Minor:
            return sRet
        elif self.Android[type][program]['Rev'] < Rev:
            return sRet    
        return True
        
    def xmlrpc_loadOneData(self,  sTableName, record, dicUser,  dicDetail = None): 
        print "Start One Data"
        liRecords, firstRecord,  ID = self.xmlrpc_loadData( sTableName, record, dicUser,  dicDetail)
        print "this is one Data",  liRecords
        if liRecords:
            return liRecords
        else: 
            return [{"SQL-ERROR":"NONE"}]
            
     
    def xmlrpc_loadData(self,  sTableName, record, dicUser,  dicDetail = None): 
        self.sStatus = ''
        liRecords = []
        ID = 0 
        #print "data at loadData = ",  sTableName, record, dicUser,  dicDetail 
       
        
        try:
            assert record >= 0 and  (isinstance(record, types.IntType) or isinstance(record, types.LongType))

            if dicDetail:
                dicColumns = dicDetail
            else:
                dicColumns = {}
                result = self.xmlrpc_executeNormalQuery("select column_name,  data_type  from information_schema.columns where table_name = '" + sTableName +"' ",  dicUser)
                #print result
                for i in result:
                    #print 'column = ', i
                    dicColumns[i["column_name"]]= i["data_type"]
                        

            # self.out( '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++***')
            # self.out( self.table.getName())
            # self.out( str(self.table))
            # self.out( 'len von dicUser' + str(len(self.dicUser)) + ' --> ' + str(self.dicUser))
            # self.out( 'diccolumns = ')
            # self.out( `dicColumns`)

            if sTableName == "dms":
                del dicColumns["document_image"]
           
                
            liRecords = self.xmlrpc_loadRecord( sTableName, record, dicColumns , dicUser)
            #print "len of liRecords = ",  len(liRecords)
            
            firstRecord = {}
            if liRecords and liRecords not in self.liSQL_ERRORS:
                for r in range(len(liRecords)) :
                     record =  liRecords[r]
                     for key in record.keys():
                         if  isinstance(record[key], types.StringType):
                             pass
                             #record[key] =  unicode(record[key], 'utf-7')
                         if isinstance(record[key], types.UnicodeType):
                            try:
                                record[key] = record[key].encode(self.oUser.userEncoding)
                            except Exception, param:
                                print Exception
                                print param
                                
                     liRecords[r] = record


                firstRecord = liRecords[0]
                #print ('nach user-encoding')
                #print 'firstRecord by SingleData', firstRecord
                ID = firstRecord['id']
                self.sStatus = ''
                #print 'statusfields = ', self.statusfields 
                try:
                    if self.statusfields:
                        for i in range (len(self.statusfields)):
                            self.sStatus = self.sStatus + firstRecord[self.statusfields[i]] + ', '
                            
                except:
                    pass
                    #self.printOut( 'no statusfield')
            #print 'Single Data Status = ',  self.sStatus        
            self.firstRecord = firstRecord
            #print 'firstRecord by SingleData2', firstRecord
        except AssertionError:
            #self.printOut( 'assert error')
            liRecords = None
        except Exception, param:
            print 'Error by load Data'
            print Exception,  param
            
            
        ##self.printOut( liRecords    )
            
            
            
        ##self.printOut( liRecords    )  
        return liRecords,  self.firstRecord,  ID 
       
    def xmlrpc_is_running(self):
        print 'is running 42'
        return 42
    def xmlrpc_is_running2(self):
        print 'is running Dictionary'
        return [1, "Hallo", {"t1":55}]
        
    def xmlrpc_test_s1(self, dic1):
        print dic1
        s = 'string = '
        for i in dic1.keys():
            s += i + " : " + dic1[i] + ",  "
            
        return s
    
    def xmlrpc_test_s2(self, dic1,  dic2):
        print dic1
        s = 'string = '
        for i in dic1.keys():
            s += i + " : " + dic1[i]  + ",  "
            
        for i in dic2.keys():
            s += i + " : " + dic2[i]  + ",  "
            
        return s  
        

    def xmlrpc_getGladeKey(self,sGlade,sKey):
        
        s = "NONE"
        sUUID = self.getKeyUUID(sGlade)
        print "load glade =", sGlade
        print sKey, sUUID
        if (sUUID != sKey):
            print "load new Glade Data"
            s = self.xmlrpc_getGlade(sGlade)
            
        return [sUUID,s]
        

    def xmlrpc_getGlade(self, sGlade):

        s1 = eval(self.doDecode64(self.getValue(sGlade)))
        s = cPickle.loads(s1)
        #print s
        return s

    def xmlrpc_getEntryKey(self,liEntry):

        sEntryPrefix = liEntry[0]
        sEntry = liEntry[1]
        sKey = liEntry[2]
        
        s = "NONE"
        sUUID = self.getKeyUUID(sEntryPrefix)
        print "load entry =", sEntry
        print sKey, sUUID
        if sUUID != sKey:
            print "load new Entry Data"
            s = self.xmlrpc_getEntry(sEntryPrefix)
            if s:
                s.append({"___UUID":sUUID})
                s.append({"___EntryName":sEntryPrefix})
                print "s = ", s
                return s
            else:
                sUUID = self.getKeyUUID(sEntry)
                s = self.xmlrpc_getEntry(sEntry)
                s.append({"___UUID":sUUID})
                s.append({"___EntryName":sEntry})
                print "s2 = ",s
                return s
        else:
            s = []
            s.append({"___UUID":sUUID})
            s.append({"___EntryName":sEntryPrefix})
            print "s = ", s
            return s

        
    def xmlrpc_getEntry(self, sEntry):
        print "sEntry = " ,  sEntry

        try:
            s1 = eval(self.doDecode64(self.getValue(sEntry)))
       
        
        #print s1
            cData0 = cPickle.loads(s1)
            #print cData0
            i = cData0.getCountOfEntries()
            #print "len = ",  i
            l1 = []
            for j in range(i):

            #print "Entry-Set" ,  cData0.EntrySet 

                cData = cData0.EntrySet[j]
              #  print "cData " ,  cData
                s = {}
                s["Name"] = cData.getName()
                s["Type"] = cData.getType()
                s["VerifyType"] = cData.getVerifyType()
                s["SqlField"] = cData.getSqlField()
                l1.append(s)        
            #print "return = ", l1
        except:
            l1 = []
        return l1
        
    def xmlrpc_sl_getEntry(self, sEntry):
        lEntry0 = self.xmlrpc_getEntry("entry_" + sEntry + ".xml")
        dicEntry = {}
        for i in lEntry0:
            dicEntry[ i["SqlField"]] = i["Name"]
        print "sl_getEntry = ",dicEntry
        return dicEntry

        

    def getValue(self, sKey):
        v = 'NONE'
        sSql = "select svalue from cuon where skey = '" + sKey + "'"
        result = self.xmlrpc_executeNormalQuery(sSql)
        #print 'get-Value = ', result
        if result not in ['NONE','ERROR']:
           try:
              v = result[0]['svalue']
           except:
              v = 'NONE'
        if not v:
            v = 'NONE'
            
        return v

    def getKeyUUID(self, sName):
        v = 'NONE'
        sSql = "select uuid from cuon where skey = '" + sName + "'"
        result = self.xmlrpc_executeNormalQuery(sSql)
        #print 'get-Value = ', result
        if result not in ['NONE','ERROR']:
           try:
              v = result[0]['uuid']
           except:
              v = 'NONE'
        if not v:
            v = 'NONE'
            
        return v   

    
    def saveValue(self, sKey, cKey):
        #self.out('py_saveValue cKey = ' + cKey)
        sSql = "select skey from cuon where skey = '" + sKey + "'"
        #self.out('py_saveValue sSql = ' + `sKey`)
        result = self.xmlrpc_executeNormalQuery(sSql)
        #self.out('py_saveValue result = ' + `result`)
        if result not in ['NONE','ERROR']:
           sSql = "update cuon set svalue = '" + cKey +"' where skey = '" + sKey + "'"
        else:
           sSql = "insert into cuon (skey, svalue) values ('" + sKey + "','" + cKey +"')"
        #self.out('py_saveValue sSql = ' + `sSql`)
        result = self.xmlrpc_executeNormalQuery(sSql)
        return result

    
    def xmlrpc_createPsql(self, sDatabase, sHost, sPort, sUser,  sSql):

        # os.system('pysql ' + '-h ' + sHost + '-p ' + sPort + ' -U ' + sUser + ' ' + sDatabase + ' < ' + sSql) 
        oFile = open('/etc/cuon/run.sql', 'w')
        oFile.write(sSql)
        oFile.close()
        
        sysCommand =  'psql  ' + '-h ' + sHost + ' -p ' + sPort + ' -U ' + sUser +   ' '  + sDatabase + ' < /etc/cuon/run.sql'
        
        print sysCommand
        self.writeLog(sysCommand)
        
        sReturn = os.system(sysCommand)
        self.writeLog('return from sysCommand ' + `sReturn`)
        
        return sReturn

#context.exSaveInfoOfTable(sKey, oKey )
    
    def checkUser(self, sUser, sID, userType = None):
        ok = None
        dSession = {}
        print sUser, userType, sID
        if userType:
           if userType == 'cuon':
                try:
                    #dSession['SessionID'] = self.dicVerifyUser[sUser]['SessionID']
                    #dSession['endTime'] = self.dicVerifyUser[sUser]['endTime']
                    #--dSession['SessionID'] = self.getValue('user_' + sUser + '_Session_ID')
                    #--dSession['endTime'] = float(self.getValue('user_' + sUser + '_Session_endTime'))
                    
                    liSession = self.loadObject('user_' + sUser + '_Session_ID')
                    
                        
                    for dSession in liSession:
                       
                #dSession['SessionID'] = self.loadObject('user_' + sUser + '_Session_ID')
                #dSession['endTime'] = float(self.loadObject('user_' + sUser + '_Session_endTime'))
                
                #print 'dSession', dSession,  sID
                
                        if sID == dSession['SessionID'] and self.checkEndTime(dSession['endTime']):
                            ok = sUser
                            break
                            
                except:
                    #dSession['SessionID'] = self.getValue('user_' + sUser + '_Session_ID')
                    #dSession['endTime'] = float(self.getValue('user_' + sUser + '_Session_endTime'))
                    #self.out('py_checkUser dSession is found')
                    ok = 'zope'
                    
        #self.out('Session = ' + `dSession`)
##        if dSession:
##            if sID == dSession['SessionID'] and self.checkEndTime(dSession['endTime']):
##                ok = sUser
##        
##        self.out('end checkUser ok = ' + `ok`)
        return ok
        
    def authenticate(self, name, password, request):
        name = name.strip()
        password = password.strip()
        
        ok = False
        cParser = ConfigParser()
        cParser.read(self.CUON_FS + '/user.cfg')
        try:
            if len(password.strip())<4:
                return ok
            sP = cParser.get('password',name)
            m = hashlib.sha512(sP.strip())
            print 'Password = ',  password,  m.hexdigest().upper() 

            if password.strip().upper() == m.hexdigest().upper()[0:len(password.strip())]:
                print "User hexdigest is ok"
                ok = True
            elif password.strip() == sP.strip():
                ok = True
                
        except:
            pass
        return ok
        
        
    def xmlrpc_getLogo(self):
        dicUser['Name'] == 'zope'
        sSql = "select logo from clients"
        return 'Hallo'
        
        
    def xmlrpc_createSessionID(self, sUser, sPassword):
        print '1 -- createSessionID start'
        print "User, password = ", sUser, sPassword

        s = ''
        newS = {}
        
        if self.authenticate(name=sUser,password=sPassword,request=None):
            #self.out('2 -- createSessionID User found ')
            newS = self.createNewSessionID()
            #self.out('3 -- createSessionID id created for ' + sUser + ' = '  + `s`)
            liSessionID = self.loadObject('user_'+ sUser + '_Session_ID')
            liNewSession = []
            liNewSession.append(newS)  
            try:
                if isinstance(liSessionID, list):
                    # check the time
                    for s in liSessionID:
                        if self.checkEndTime(s['endTime']):
                            liNewSession.append(s)
                        
            except:
                pass
            
            
            
            self.saveObject('user_'+ sUser + '_Session_ID' , liNewSession)
            #self.saveObject('user_'+ sUser + '_Session_endTime' , `s['endTime']`)
            dicUserACL = self.getUserAcl(sUser) 
            self.saveObject('user_'+ sUser + '_dicUserACL' , dicUserACL)
            try:
                f = open('/var/log/cuonlogin.log','a')
                f.write( `time.strftime('%Y-%m-%d %H:%M:%s')` +' LOGIN: ' + sUser +' Sessionid = ' + `liNewSession`  + '\n')
                f.close()
            except Exception,  params:
                print Exception,  params
                
            #--self.saveValue('user_'+ sUser + '_Session_ID' , s['SessionID'])
            #--self.saveValue('user_'+ sUser + '_Session_endTime' , `s['endTime']`)
            #context.exSaveInfoOfTable('user_' + sUser , s)
            #self.out('4 -- createSessionID User is write ')
            
            #self.dicVerifyUser[sUser] = {}
            #self.dicVerifyUser[sUser]['SessionID'] = s['SessionID']
            #self.dicVerifyUser[sUser]['endTime'] = `s['endTime']`
            
        else:
            print 'createSessionID User not found'
        try:
            if not newS or not newS.has_key('SessionID'):
                newS['SessionID'] = 'TEST'
            print 'createSessionID ID = ' + `s`
        except:
            newS = {}
            newS['SessionID'] = 'TEST'
        print "return id = ", newS['SessionID']
        return newS['SessionID']
        
        
    def getUserAcl(self, sUser):
        dicUserACL = {}
        liClients = []
        cParser = ConfigParser()
        cParser.read(self.CUON_FS + '/clients.ini')
        try:
            sClient = cParser.get(sUser, 'clientsIDs')
            liClients1 = sClient.split(',')
            for i in liClients1:
                liClients.append(int(i))
            
            
                
        except:
            liClients = [1]
        dicUserACL['clients'] = liClients
        print 'clients for ', sUser, dicUserACL
        
        return dicUserACL
        
    def xmlrpc_checkVersion(self, VersionClient, version):
        ok = 'O.K.'
        print version
        print VersionClient
        
        if version['Major'] != VersionClient['Major'] or version['Minor'] != VersionClient['Minor'] or version['Rev'] != VersionClient['Rev']:
            ok = 'Wrong'

        return ok

    def testMulti(self, value):
        return   value[0] * value[1]

    def xmlrpc_checkVersions(self,clientversion,OS="Linux64"):
        dicUser={'Name':'zope'}
        ok = "ok"
        try:
            if clientversion[:8] == "cuonlive":
                if OS != "android":
                    OS = clientversion[9:clientversion.find("-")]
                    lVersionClient =  clientversion[clientversion.find("-")+1:].split(".")
                    print "OS= ", OS, lVersionClient
                    id, version =  self.xmlrpc_getLastVersion("cuonlive",OS)
            elif  clientversion[:9] == "clientCPP":
                id, version =  self.xmlrpc_getLastVersion("clientCPP",OS)
        except:
            pass
        VersionClient = {}
        VersionClient['Major'] =  lVersionClient[0]
        VersionClient['Minor'] = lVersionClient[1]
        VersionClient['Rev']  = lVersionClient[2]
        print "Version = ", clientversion,VersionClient,version,id
        if version['Major'] != int(VersionClient['Major']) or version['Minor'] != int(VersionClient['Minor']) or version['Rev'] != int(VersionClient['Rev']):
            print "load data from id = ", id
            sSql = "select clientdata from cuon_clients where id = " + `id`
            print "sql = ", sSql 
            result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            
            ok = result[0]['clientdata']
            print "length", len(ok)
            
        return ok

            
        

    def xmlrpc_getLastVersion(self,platform="python",OS="Linux64"):
        id = 0
        version = '0.0.0'
        print "Platform,OS = ",platform,OS
        self.writeLog('Start check version')
        self.writeLog('os AT CHECKvERSION =' + OS)
           
        dicUser={'Name':'zope'}
        if platform == "python":
           sSql = "select max(id) as maxid  from cuon_clients where clientid = 'Python' "   
        elif  platform == "cuonlive":
           sSql = "select max(id) as maxid  from cuon_clients where clientid =  '" +( platform + "_" + OS).lower() + "'"
        #self.writeLog('Start check version sql= ' + `sSql`)
        print "sSql = ", sSql
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        self.writeLog('LastVersion = ' + `result`)

        if result not in ['NONE','ERROR']:
            try:
                id = int(result[0]['maxid'])
            except:
                id = -1
        else:
           id = -1
        if id == -1:
           version = '0.0.0'
        else:
           sSql = 'select version from cuon_clients where id = ' + `id`
           result = self.xmlrpc_executeNormalQuery(sSql, dicUser)

           if result not in ['NONE','ERROR']:
               version = result[0]['version']
        liVersion = version.split('.')
        print liVersion
        dicVersion = {'Major':int(liVersion[0]),'Minor':int(liVersion[1]), 'Rev':int(liVersion[2])}
        self.writeLog('check version id, version = ' + `id` + ', ' + `dicVersion` ) 


            

        return id, dicVersion
    def xmlrpc_getInfo(self, sSearch):
        return self.getValue(sSearch)
    def xmlrpc_saveInfo(self, sKey, cKey):
        return self.saveValue(sKey, cKey)
    
    def xmlrpc_getListOfClients(self, dicUser):
        self.writeLog('Start List Of Clients')
        sSql = "select id from clients"
        dicClients = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        liClients = []
        self.writeLog('Clients = ' + `dicClients`)

        if dicClients not in ['NONE','ERROR']:
           for i in dicClients:
              self.writeLog('i = ' + `i`)
              cli = i['id']
              self.writeLog('cli = ' + `cli`)
              
              liClients.append(cli)
        
        self.writeLog('liClients = ' + `liClients`)
        
        return liClients
    
    def xmlrpc_checkExistSequence(self, sName, dicUser):

        sSql = "select relname from pg_class where relname = '" + sName + "'"
        dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        ok = 0

        if dicName not in ['NONE','ERROR']:
            ok = len(dicName)

        return  ok
        
    
    def xmlrpc_checkExistTable(self, sName, dicUser):
        sSql = "select tablename from pg_tables where tablename = '" + sName + "'"
        dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        ok = 0

        if dicName not in ['NONE','ERROR']:
            try:
                ok = len(dicName)
            except:
                ok = 0

        return  ok
        
    def xmlrpc_checkExistColumn(self, sTableName, sColumnName, dicUser):
        sSql =  "select attname from pg_attribute where attrelid = ( select relfilenode from pg_class where relname = '" + sTableName +"')" 
        
        # and atttypmod != -1 )"
        
        dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        ok = 0
        try:
            
            for i in range(len(dicName)):
                #print dicName[i]['attname']
                if sColumnName == dicName[i]['attname']:
                   ok = 1
        except:
            ok = 0

        return ok
        
    def xmlrpc_checkTypeOfColumn(self, sTable, sColumn, sType, iSize, dicUser ):
        
        bCheck = 0
        sSql = "select typname from pg_type where pg_type.oid = (select atttypid from pg_attribute where attrelid = (select relfilenode from pg_class where relname = \'"
        
        sSql = sSql + sTable + "\') and attname = \'" + sColumn + "\' ) "
        
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser )
        #print result
        try: 
            self.writeLog('types by ColumnCheck: ' + `result[0]['typname']` + ' ### ' + `sType`)
            
            if string.find(result[0]['typname'],sType) > -1 :
               self.writeLog( 'type is equal')
               if string.find(sType,'char') > -1:
                  result2 = self.xmlrpc_getSizeOfColumn(sTable, sColumn, dicUser)
                  self.writeLog( 'Size =' + `result2[0]['atttypmod'] -4` + ', ' +  `int(iSize)`)
                  if (result2[0]['atttypmod'] -4) == int(iSize):
                     bCheck = 1
               else:
                  bCheck = 1
        except:
            bCheck = 0
        return bCheck
    
    def xmlrpc_getSizeOfColumn(self, sTable, sColumn, dicUser):
        sSql = "select pg_attribute.atttypmod from pg_attribute where  attrelid = (select relfilenode from pg_class where relname = \'" + sTable + "\') and attname = \'" + sColumn + "\'"
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        return result


    def xmlrpc_logout(self, sUser):
        ok = True
        self.openDB()
        self.saveObject('user_'+ sUser + '_Session_ID' , 'TEST')
        self.saveObject('user_'+ sUser + '_Session_endTime' , '0')
        self.closeDB()    
        #self.saveValue('user_' + sUser,{'SessionID':'0', 'endTime': 0})
        try:
            f = open('/var/log/cuonlogin.log','a')
            f.write ( `time.strftime('%Y-%m-%d %H:%M:%s')` +' LOGOUT: ' + sUser +'\n')
            f.close()
        except:
            pass
        return ok 
    def xmlrpc_createGroup(self, sGroup, dicUser):
        # check the group
        
        sSql = "select groname from pg_group where groname = '" + sGroup + "'"
        dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        
        if dicName not in ['NONE','ERROR']:
           ok = 'Group exists'
        else:
           sSql = 'CREATE GROUP ' + sGroup
           ok = self.xmlrpc_executeNormalQuery(sSql,dicUser )
        
        return ok
        
    def xmlrpc_createUser(self, sUser, sPassword,  dicUser, createDBUser=1):
        ok = 'No action'
        if createDBUser:
            # check the user
            
            sSql = "select usename from pg_user where usename = '" + sUser + "'"
            dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            
            if dicName not in ['NONE','ERROR']:
               ok = 'User exists'
            else:
               sSql = 'CREATE USER ' + sUser
               ok = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            
        # context.Cuon.src.Databases.py_saveValue('user_'+ sUser, sPassword)
        
        return ok

    def xmlrpc_addGrantToGroup(self, sGrants, sGroup, sTable, dicUser):
        # check the group
        ok = 'ERROR'
        
        sSql = "select groname from pg_group where groname = '" + sGroup + "'"
        dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        if dicName not in ['NONE','ERROR']:
           ok = 'GROUP'
           sSql = 'Grant ' +sGrants + ' ON '  + sTable + ' TO GROUP ' + sGroup
           ok = self.xmlrpc_executeNormalQuery(sSql, dicUser)
         
        
        else:
           ok = 'Group does not exist'
        
        return ok
    def xmlrpc_addUserToGroup(self, sUser, sGroup, dicUser):
        
        # check the user
        self.writeLog(dicUser['Name'])
        self.writeLog(dicUser['SessionID'])
        
        sSql = "select usename from pg_user where usename = '" + sUser + "'"
        dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        ok = 'ERROR'
        if dicName not in ['NONE','ERROR']:
           # check the group
           sSql = "select groname from pg_group where groname = '" + sGroup + "'"
           dicName = self.xmlrpc_executeNormalQuery(sSql, dicUser)
           ok = 'USER'
           if dicName not in ['NONE','ERROR']:
              ok = 'GROUP'
              sSql = 'ALTER GROUP ' +sGroup + ' ADD USER ' + sUser
              ok = self.xmlrpc_executeNormalQuery(sSql, dicUser)
         
        
        else:
           ok = 'Group or User does not exist'
        
        return ok
        
    def xmlrpc_createCuon(self, dicUser):
        
        self.writeLog('start py_createCuon')
        retValue = True
        ok = self.xmlrpc_checkExistTable('cuon', dicUser)
        self.writeLog('py_createCuon1 ' + `ok`)
        if ok == 0:
           retValue = False
           sSql = 'create table cuon ( skey varchar(255) NOT NULL UNIQUE , svalue text NOT NULL, PRIMARY KEY (skey) )'
           ok = self.xmlrpc_executeNormalQuery(sSql, dicUser)
           sSql = 'GRANT ALL ON  cuon TO PUBLIC'
           ok = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        # try to add column uuid
        sSql = "alter table cuon add column uuid varchar(36)"
        ok = self.xmlrpc_executeNormalQuery(sSql, dicUser)

           
        return retValue
        
        
    def saveWebshopRecord(self, sNameOfTable='EMPTY', id=0, id_field='id', dicValues ={}, dicUser={}):
        import string
        import types
        
        context.logging.writeLog('begin RECORD2')
        dicUser['Database'] = 'osCommerce'
        
        if id > -1:
            # update
            sSql = 'update ' + sNameOfTable + ' set  '
            
            for i in dicValues.keys():
                sSql = sSql + i
                liValue = dicValues[i]
                if liValue[1] == 'string' or liValue[1] == 'varchar':
                    sSql = sSql  + " = \'" + liValue[0]+ "\', "
        
                elif liValue[1] == 'int':
                    sSql = sSql  + " =  " + `int(liValue[0])` + ", "
        
                elif liValue[1] == 'float':
                    sSql = sSql  + " = " + `float(liValue[0])` + ", "
        
                elif liValue[1] == 'date':
                    if len(liValue[0]) < 10:
                        sSql = sSql  + " = NULL, "
                    else:
                        sSql = sSql  + " = \'" + liValue[0]+ "\', "
        
                elif liValue[1] ==  'bool':
                    context.logging.writeLog('REC2-bool ')
                    if liValue[0] == 1:
                        liValue[0] = 'True'
                    if liValue[0] == 0:
                        liValue[0] = 'False'
                    sSql = sSql + " = " + liValue[0] + ", "
                else:
                    sSql = sSql  + " = \'" + liValue[0]+ "\', "
            
            sSql = sSql[0:string.rfind(sSql,',')]
        
            sSql = sSql + ' where ' + id_field + ' = ' + `id`
            #print sSql
        else:
            context.logging.writeLog('new RECORD2')
            sSql = 'insert into  ' + sNameOfTable + ' (  '
            sSql2 = 'values ('
            context.logging.writeLog('REC2-1 ' + `sSql` + `sSql2`)
            for i in dicValues.keys():
                sSql = sSql + i + ', '
                context.logging.writeLog('REC2-1.1 ' + `sSql`)
                liValue = dicValues[i]
                context.logging.writeLog('REC2-1.2 ' + `liValue`)
                if liValue == None :
                    sSql2 = sSql2 + "\'\', " 
                else:
                    if liValue[1] ==  'string' or liValue[1] == 'varchar':
                        if len(liValue[0]) == 0:
                            sSql2 = sSql2 + "\'\', " 
                        else:
                            sSql2 = sSql2  + "\'" + liValue[0] + "\', "
                        context.logging.writeLog('REC2-2 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'int':
                        sSql2 = sSql2  + `int(liValue[0])` + ", "
                        context.logging.writeLog('REC2-3 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'float':
                        sSql2 = sSql2  + `float(liValue[0])` + ", "
                        context.logging.writeLog('REC2-4 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'date':
                        if len(liValue[0]) < 10:
                            sSql2 = sSql2  +  " NULL, "
                        else:
                            sSql2 = sSql2  + " \'" + liValue[0] + "\', "
                            context.logging.writeLog('REC2-5 ' + `sSql` + `sSql2`)
                    elif liValue[1] ==  'bool':
                        context.logging.writeLog('REC2-bool ')
                        if liValue[0] == 1:
                           liValue[0] = 'True'
                        if liValue[0] == 0:
                           liValue[0] = 'False'
        
                        sSql2 = sSql2  +"\'" + liValue[0] + "\', "
                        context.logging.writeLog('REC2-6 ' + `sSql` + `sSql2`)
                    else:
                        sSql2 = sSql2  +  " \'" + liValue[0] + "\', "
                        context.logging.writeLog('REC2-6 ' + `sSql` + `sSql2`) 
             
                        
            context.logging.writeLog('REC2-10 ' + `sSql` + '__' + `sSql2`) 
            sSql = sSql[0:sSql.rfind(',')]
            sSql2 = sSql2[0:sSql2.rfind(',')]
            #sSql2 = sSql2 + 'nextval(\'' + sNameOfTable + '_id\'), current_user, \'create\''  
            
            # set brackets and add
            sSql = sSql + ') ' + sSql2 + ')'
        
            # execute insert
            context.logging.writeLog('SQL by RECORD2 = ' + `sSql`)
            #print sSql
            context.py_executeNormalQuery(sSql, dicUser)
        
            # find last id 
        
            sSql = 'select max(' + id_field + ') as last_value from ' + sNameOfTable 
            # sSql = sSql[0:string.rfind(sSql,',')]
        
            
        
        #print sSql
        #return printed
                   
        return context.py_executeNormalQuery(sSql,dicUser)
    def xmlrpc_saveZipPlanet(self, dicPlanet, dicUser):   
        print 'dicPlanet', dicPlanet
        sSql = "select id from planet where name = '" + dicPlanet['name'][0] + "'"
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        print 'zipPlanet 1', result
        if result == 'NONE':
            print 'zipPlanet 2', result
            result = self.xmlrpc_saveRecord('planet', -1, dicPlanet, dicUser, liBigEntries='NO') 
            print 'zipPlanet 3', result
        else:
            result = self.xmlrpc_saveRecord('planet', result[0]['id'], dicPlanet, dicUser, liBigEntries='NO')
            print 'zipPlanet 4', result
        return result
    def xmlrpc_saveZipCountry(self, dicCountry, dicUser):   
        print 'dicCountry', dicCountry
        sSql = "select id from country where name = '" + dicCountry['name'][0] + "'"
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        print 'zipCountry 1', result
        if result == 'NONE':
            print 'zipCountry 2', result
            result = self.xmlrpc_saveRecord('country', -1, dicCountry, dicUser, liBigEntries='NO') 
            print 'zipCountry 3', result
        else:
            result = self.xmlrpc_saveRecord('country', result[0]['id'], dicCountry, dicUser, liBigEntries='NO')
            print 'zipCountry 4', result
        return result
        
        
    def xmlrpc_saveZipState(self, dicState, dicUser):   
        print 'dicState', dicState
        sSql = "select id from country where short_name = '"+ dicState['country_id'][0] +"'"
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        dicState['country_id'][0] = result[0]['id']
        if int(dicState['country_id'][0]) > 0:
            sSql = "select id from states where name = '" + dicState['name'][0] +"'"
            result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            print 'zipState 1', result
            if result == 'NONE':
                print 'zipState 2', result
                result = self.xmlrpc_saveRecord('states', -1, dicState, dicUser, liBigEntries='NO') 
                print 'zipState 3', result
            else:
                result = self.xmlrpc_saveRecord('states', result[0]['id'], dicState, dicUser, liBigEntries='NO')
                print 'zipState 4', result
            return result
        else:
            print 'No Country found'
        return result 
                
                
                
    def xmlrpc_saveZipCity(self, dicCity,dicZip, dicUser):  
         
        country_id = 0
        print 'dicCity', dicCity
        sSql = "select id from country where short_name = '"+ dicCity['country_id'][0] +"'"
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        if result not in ['NONE','ERROR'] and int(result[0]['id']) > 0:
            country_id = int(result[0]['id'])
        dicCity['country_id'][0] = country_id
        
        state_id = 0    
        sSql = "select id from states where state_short = '"+ dicCity['state_id'][0] +"' and country_id = " + `country_id` 
        result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        if result not in ['NONE','ERROR'] and int(result[0]['id']) > 0:
            state_id = int(result[0]['id'])
        dicCity['state_id'][0] = state_id
        ad_id = 0
        district_id = 0
        if country_id and state_id:
            if dicCity['ad_id'] != '-':
            
                sSql = "select id from administrative_district where state_id = " + `state_id` + " and name = '" + dicCity['ad_id'][0] + "'"
                result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
                if result not in ['NONE','ERROR'] and int(result[0]['id']) > 0:
                    ad_id = int(result[0]['id'])
                else:
                    dicAD = {}
                    dicAD['name'] = [dicCity['ad_id'][0],'string']
                    dicAD['state_id'] = [state_id,'int']
                    if result == 'NONE':
                        result = self.xmlrpc_saveRecord('administrative_district', -1, dicAD, dicUser, liBigEntries='NO') 
                    else:
                        result = self.xmlrpc_saveRecord('administrative_district', result[0]['id'], dicAD, dicUser, liBigEntries='NO')
                    
##                    sSql = "select id from district where state_id = ' + `state_id`
##                    result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
##                    if result != 'NONE' and int(result[0]['id']) > 0:
##                        ad_id = int(result[0]['id'])
##            if int(dicCity['country_id'][0]) > 0:
##                sSql = "select id from states where name = '" + dicCity['name'][0] +"'"
##                result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
##                print 'zipCity 1', result
##                if result == 'NONE':
##                    print 'zipCity 2', result
##                    result = self.xmlrpc_saveRecord('states', -1, dicCity, dicUser, liBigEntries='NO') 
##                    print 'zipCity 3', result
##                else:
##                    result = self.xmlrpc_saveRecord('states', result[0]['id'], dicCity, dicUser, liBigEntries='NO')
##                    print 'zipCity 4', result
##                return result
        else:
            print 'No Country or state  found'
            print '___________________________'
        return result 
                        
    def bindSql(self, liFields ):
        #print 'bindSql start'
        sSql = ''
        for liValues in liFields:
            sSql += liValues[0] + ' as ' + liValues[1] + ', '
        
        
        sSql = sSql[0:sSql.rfind(',')]
        #print 'bindSql = ', sSql
        
        return sSql
        
            
    def xmlrpc_getInternInformation(self, dicUser, Pers1=None, Pers2=None):
        #print 'Pers1 = ', Pers1
        
        dicRet = {}
        sSql = "select *  from staff where id = " + self.getStaffID(dicUser)
        liResult = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        if liResult and liResult not in ['NONE','ERROR']:
            dicPers = liResult[0]
            for key in dicPers.keys():
                
                dicRet['userinfo_' + key] = dicPers[key]
                    
            
            
        if Pers1:
            sSql = "select * from staff where id = " + `Pers1`
            liResult = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            if liResult and liResult not in ['NONE','ERROR']:
                dicPers = liResult[0]
                for key in dicPers.keys():
                    dicRet['person1_' + key] = dicPers[key]
                    
        if Pers2:
            sSql = "select * from staff where id = " + `Pers2`
            liResult = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            if liResult and liResult not in ['NONE','ERROR']:
                dicPers = liResult[0]
                for key in dicPers.keys():
                    dicRet['person2_' + key] = dicPers[key]
        
        if not dicRet:
            dicRet = 'NONE'
        #print dicRet
        return dicRet
    def xmlrpc_checkUpdateID(self, sTable, sField, liValue, dicUser):
        updateID = 0
        sSql = 'select id from ' + sTable + ' where ' + sField + ' = '
        if  liValue[1] == 'string':
            sSql += "'" + liValue[0].strip() + "'"
        else:
            sSql += `liValue[0]` 
        
        sSql += self.getWhere('',dicUser,2)
        liResult = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        print liResult
        if liResult and liResult not in ['NONE','ERROR']:
            updateID = liResult[0]['id']
            
        return updateID
        
    def xmlrpc_updateBank(self, dicUser):
        updateID = 0    
        sSql = "update bank set address_id = (select address.id from address where bank.bcn = address.status_info "
        sSql += self.getWhere('',dicUser,2,'bank.') + ')'
        liResult = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        print liResult
        return True

    def xmlrpc_getDMSRights(self,sName):
        value = None
        rights = 'NONE'
        groups = 'NONE'
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/sql.ini')
            #print cpServer
            #print cpServer.sections()
            if sName == 'INVOICE':
                value = self.getConfigOption('DEFAULT_MODUL_RIGHTS_DMS','INVOICE', cpServer)
            elif sName == 'EMAIL':
                value = self.getConfigOption('DEFAULT_MODUL_RIGHTS_DMS','EMAIL', cpServer)
        
        except:
            pass
        if value:
            liS = value.split(',')
            if liS:
                try:
                    rights = liS[0]
                    groups = liS[1]
                    
                except:
                    pass
                    
        return rights,groups
        
    def xmlrpc_getDIC_USER(self):
        return self.DIC_USER
        
    def writeConfigFiles2Database(self):
        print '-------------------------------------------------------------------------- start write config2SQL'
        
        cParser = ConfigParser()
        cParser.read(self.CUON_FS + '/user.cfg')
        try:
            sP = cParser.get('password',self.POSTGRES_USER)
            print sP
            sID = self.xmlrpc_createSessionID(self.POSTGRES_USER, sP)
                
        
            dicUser = {'Name': self.POSTGRES_USER, 'SessionID':sID, 'userType':'cuon' }
            print '2-------------------------------------------------------------------------- start write config2SQL ',  dicUser
            
            
                        
                        
           #clients
            sSql = "select id from clients where status != 'delete'"
            result = self.xmlrpc_executeNormalQuery(sSql, dicUser )
            print '3-------------------------------------------------------------------------- start write config2SQL ',  sSql,  result

            if result and result not in self.liSQL_ERRORS:
                for row  in result:
                    try:
                        client_id = row["id"]
                        cpSection = "CLIENT_" + `client_id`
                        cParser = ConfigParser()
                        cParser.read(self.CUON_FS + '/clients.ini')
                        print '4-------------------------------------------------------------------------- start write config2SQL'
    
                        if cParser. has_section(cpSection):
                            print cParser.options(cpSection)
                            for sOptions in self.liClientsOptions:
                                try:
                                    print '5-------------------------------------------------------------------------- start write config2SQL '
        
        
                                    sValue = self.getConfigOption(cpSection, sOptions,  cParser)
                                    
                                    if sValue:
                                        try:
                                            sValue = sValue.decode('UTF-8')
                                        except:
                                            print 'decoding not work'
                                            #oValue = sValue
                                        #sValue = oValue
                                    if sValue:
                                        print '6-------------------------------------------------------------------------- start write config2SQL ', cpSection, sOptions
                                        sSql = "select * from fct_write_config_value(" + `client_id` + ", '" + 'clients.ini' + "', '" + cpSection + "', '" + sOptions + "', '" + sValue + "' ) "
                                        #print sSql
                                        self.xmlrpc_executeNormalQuery(sSql, dicUser)
                                except Exception,  params:
                                    print Exception,  params
                                    
                    except Exception,  params:
                        print Exception,  params
                        
            #sSql =  "select * from fct_get_config_option(1,'clients.ini', 'client_1', 'order_main_headline_articles_id') "
            #print sSql
            #result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            #print result 
                           
                          
                         
        except Exception,  params:
            print Exception,  params
            

    def xmlrpc_checkUserClient(self,  UserName,  clientID):
#        if clientID == 1:
#            return True
            
        print "############### Check User/client for ", UserName, clientID
        cParser = ConfigParser()
        cParser.read(self.CUON_FS + '/clients.ini')
        if cParser. has_section(UserName):
           print "section found " 
           sValue = self.getConfigOption(UserName, 'clientsIDs',  cParser) 
           liClients = sValue.split(',')
           print "allowed clients = ", liClients
           for i in liClients:
               if clientID == int(i.strip()):
                   print "found client id = ",clientID, i
                   return True
        return False
               
        
    def getDatabase(self, type,  iBegin=0,  iEnd=0):
        sDB = "orderbook"
        sDBPositions = "orderposition"
        sDBInvoice = "orderinvoice"
        
        if type:
            
                
            if type == "Proposal":
                sDB = "proposal"
                sDBPositions = "proposalposition"
                sDBInvoice = "proposalinvoice"
            elif type == "Enquiry":
                sDB = "enquiry"
                sDBPositions = "enquiryposition"
                sDBInvoice = "enquiryinvoice"
            elif type == "Project":
                 
                sDB = "projects"
                sDBPositions = ""
                sDBInvoice = ""   
                
            else :
                sDB = "orderbook"
                sDBPositions = "orderposition"
                sDBInvoice = "orderinvoice"
                
        else:
            if iBegin >= self.OrderStatus['OrderStart'] and iEnd <= self.OrderStatus['OrderEnd']:
                sDB = "orderbook"
                sDBPositions = "orderposition"
                sDBInvoice = "orderinvoice"
            elif iBegin >= self.OrderStatus['ProposalStart'] and iEnd <= self.OrderStatus['ProposalEnd']:
                sDB = "proposal"
                sDBPositions = "proposalposition"
                sDBInvoice = "proposalinvoice"
                
            elif iBegin >= self.OrderStatus['EnquiryStart'] and iEnd <= self.OrderStatus['EnquiryEnd']:
                sDB = "Enquiry"
                sDBPositions = "enquiryposition"
                sDBInvoice = "enquiryinvoice"
            else:
                sDB = "orderbook"
                sDBPositions = "orderposition"
                sDBInvoice = "orderinvoice"
                
        return sDB, sDBPositions,  sDBInvoice



    def xmlrpc_import(self, sCtrl,sData, dicUser):
        bOK = False
        imp1 = ex_im_import_generic1()
        sFilename = self.getNewUUID()
        ctrlFile = open(sFilename,"wa")
        ctrlFile.write(sCtrl)
        ctrlFile.close()
        
        imp1.readCtrlFile(sFilename)
        imp1.standardImport(sData,dicUser) 
        os.remove(sFilename)

        return bOK
        

    def xmlrpc_export(self, sCtrl,sData, dicUser):
        sData = None
        ex1 = ex_im_import_generic1()
        sFilename = self.getNewUUID()
        ctrlFile = open(sFilename,"wa")
        ctrlFile.write(sCtrl)
        ctrlFile.close()
        
        ex1.readCtrlFile(sFilename)
        sData = ex1.standardExport(sData,dicUser) 
        os.remove(sFilename)

        return sData
        

class ex_im_port_generic1(basics):
    
    def __init__(self):
        
        
        self.dicFileAttributes = {}
        self.dicFileAttributes['iFile'] = None
        self.dicFileAttributes['splitValue'] = ';'
        self.dicFileAttributes['fromChangedValue'] = None
        self.dicFileAttributes['toChangedValue'] = ''
        self.dicFileAttributes['exportFile'] = None
        self.dicFileAttributes['exportTablse'] = None
        self.dicFileAttributes['exportHeader'] = None
        self.dicFileAttributes['inputType'] = 'Standard'
        self.dicFileAttributes['liColumns'] = []
        self.dicFileAttributes['decodeData'] = None
        self.dicFileAttributes['Decoding'] = None
        self.dicFileAttributes['Encoding'] = None
        
        self.dicFileAttributes['updateData'] = 'NO'
        self.dicFileAttributes['checkUpdateField'] = None
        self.dicFileAttributes['fixFields'] = None
        self.dicFileAttributes['mergeData'] = 'NO'
        self.dicFileAttributes['extraFunction'] = None
        self.dicFileAttributes['stockID'] = None

 
        self.iColumns = 0
        self.oDatabase = Database()
          
         
                               

    def readCtrlFile(self, sFilename):
        ctrlFile = open(sFilename)
      
        if ctrlFile:

            try:
                self.cpParser = ConfigParser.ConfigParser()
            
                f = open(iFileName)
                print 'File = ',  iFileName,  f
                self.cpParser.readfp(f)
                f.close()
            except:
                pass
            print 'List of sections',  self.getListOfParserSections()
            self.dicFileAttributes['exportFile'] = self.getConfigOption('Export', 'Filename')

            self.dicFileAttributes['exportTables'] = self.getConfigOption('Export', 'Tables')
            self.dicFileAttributes['Columns'] =self.getConfigOption('Export', 'Columns')
            self.dicFileAttributes['Headers'] =self.getConfigOption('Export', 'Headers')
            
            self.dicFileAttributes['Where'] =self.getConfigOption('SQL', 'Where')
            self.dicFileAttributes['Order'] =self.getConfigOption('SQL', 'Order')
            self.dicFileAttributes['Group'] =self.getConfigOption('SQL', 'Group')
            
            self.dicFileAttributes['inputFile'] = self.getConfigOption('Import', 'Filename')
            self.dicFileAttributes['inputType'] = self.getConfigOption('Import', 'inputType')
            self.dicFileAttributes['importTable'] = self.getConfigOption('Import', 'inputTable')




            self.dicFileAttributes['splitValue'] = self.getConfigOption('Values', 'Split')
            self.dicFileAttributes['Encoding'] = self.getConfigOption('Values', 'Encoding')
            self.dicFileAttributes['Decoding'] = self.getConfigOption('Values', 'Decoding')
           
            self.dicFileAttributes['printHeader'] = self.getConfigOption('Values', 'print_header')
            self.dicFileAttributes['stringDelimit'] = self.getConfigOption('Values', 'string_delimit')
            self.dicFileAttributes['changeFloat'] = self.getConfigOption('Values', 'change_float')
            self.dicFileAttributes['updateData'] =  self.getConfigOption('Values', 'update_data')
            self.dicFileAttributes['checkUpdateField'] = self.getConfigOption('Values', 'check_update_field')
            self.dicFileAttributes['fixFields'] =  self.getConfigOption('Values', 'fix_fields')
            self.dicFileAttributes['mergeData']  = self.getConfigOption('Values', 'merge_data')
            self.dicFileAttributes['stockID'] =  int(self.getConfigOption('Values', 'stockID'))


            bOK = True
            iZ = 0

            
            while (bOK):
                s2 =  (self.getConfigOption('Columns', 'column'+`iZ`))
                if s2:
                    bOK = True
                    liS2 = s2.split(',')
                    dicColumn = {}
                    
                    dicColumn['name'] = liS2[0].strip()
                    dicColumn['field'] = liS2[1].strip()


                    self.dicFileAttributes['liColumns'].append(dicColumn)
                    iZ += 1
                else:
                    bOK = False
                    

            
               


            self.dicFileAttributes['ExecPostProcess'] =  self.getConfigOption('Exec', 'PostProcess')
            self.dicFileAttributes['extraFunction'] =  self.getConfigOption('Exec', 'extra_function')

            
            print 'file attribs = ',  self.dicFileAttributes
              
            
                 
    def standardImport(self, sData, dicUser):
        #print 'dicfileAttributes', self.dicFileAttributes
        #print 'input-file = ', self.dicFileAttributes['inputFile']
        #importFile = open(self.dicFileAttributes['inputFile'])
        # generate UpdateField
##        if self.dicFileAttributes['updateData'] == 'YES':
##            liUF = self.dicFileAttributes['checkUpdateField'].split(',')
##            self.dicFileAttributes['checkUpdateField'] =[]
##            self.dicFileAttributes['checkUpdateField'][0] = liUF[0].strip()
##            self.dicFileAttributes['checkUpdateField'][1] = liUF[1].strip()
##            
                    
##        s1 = importFile.readline()
##        lS2 = s1.split(self.dicFileAttributes['splitValue'])
##        print 'icolumns = ', self.iColumns
##        notEnd = True
##        sNext = None
##        while (len(lS2) < self.iColumns) or notEnd:
##            sNext += importFile.readline()
##            
##            if sNext and  sNext.split(self.dicFileAttributes['splitValue']):
##                notEnd = False
##            else:
##                s1 += sNext
##                
####            
            
##        # Headlines
##        # for exmple 
##        #['ADRNR;ANREDE;LAND;NAME1;NAME2;ORT;PLZ;STRASSE;ANSPRANREDE;ANSPRTITEL;ANSPRNACHNAME;ANSPRVORNAME;BRIEFANREDE;ABTEILUNGKLAR;ABTEILUNG;FUNKTION;KRITERIUM;EINORDNUNG\r\n']
##        #
        #print 'len lS2 = ', len(lS2)
        

        print 'Type : ', self.dicFileAttributes['inputType'][0:8]
      
        #self.executeSomeStuff()
        #exit()
        if self.dicFileAttributes["stockID"]:
            stockID =  self.dicFileAttributes["stockID"]
        else:
            stockID = 1;

        importFile = StringIO.StringIO(sData)
        s1 = importFile.readline()
        if self.dicFileAttributes['importHeader'].upper() == 'YES':
            s1 = importFile.readline()
        
        se = 1
        
        #lS2 = s1.split(self.dicFileAttributes['splitValue'])
        print 'icolumns = ', self.iColumns
                
        print 's1 begin = ', s1
        while s1:
            #print s1
##            print '----'
##            if self.dicFileAttributes['decodeData']:
##                s1 = s1.decode(self.dicFileAttributes['decodeData']).encode('utf-8')
##            if self.dicFileAttributes['fromChangedValue']:
##                s1 = s1.replace(self.dicFileAttributes['fromChangedValue'],self.dicFileAttributes['toChangedValue'])
##            lS1 = s1.split(self.dicFileAttributes['splitValue'])
##            
            if self.dicFileAttributes['mergeData'] == 'YES':
                goAhead = True    
                #while (len(lS1) < self.iColumns):
                while(goAhead):
                    sNext = importFile.readline()
                    if sNext:
                        if len(sNext.split(self.dicFileAttributes['splitValue'])) > 1:
                            print 'snext = ', sNext
                            goAhead = False
                        else:
                            s1 += '\n' + sNext
                    else:
                        goAhead = False
            print 's1 01 = ', s1    
            if self.dicFileAttributes['decodeData']:
                s1 = s1.decode(self.dicFileAttributes['decodeData']).encode('utf-8')
            if self.dicFileAttributes['fromChangedValue']:
                s1 = s1.replace(self.dicFileAttributes['fromChangedValue'],self.dicFileAttributes['toChangedValue'])
                
            lS1 = s1.split(self.dicFileAttributes['splitValue'])
            
            #exportFile.write(s1)
            print lS1
            # now set the values
            dicValues = {}
            #print 'self.dicFileAttributes = ', self.dicFileAttributes
            
            for i in range(len(self.dicFileAttributes['liColumns'])):
                if self.dicFileAttributes['liColumns'][i]['field'].upper() != 'NONE':
                    print '###--> ',i,  self.dicFileAttributes['liColumns'][i]['field']
                    print self.dicFileAttributes['liColumns'][i]['name'] ,  self.dicFileAttributes['liColumns'][i]['field']
                    
                    dicValues[self.dicFileAttributes['liColumns'][i]['name']] = [lS1[i].strip(),self.dicFileAttributes['liColumns'][i]['field']]


            #print `self.dicUser`    

            self.importID = -1
            if self.dicFileAttributes['inputType'] == 'Standard':
                
                # verify Fields
                dicValues = oSingleImport.verifyValues(dicValues)
                # save to Database
                if self.dicFileAttributes['updateData'] in ['YES', 'IGNORE'] :
                    
                    updateID = self.xmlrpc_checkUpdateID(self.dicFileAttributes['importTable'], self.dicFileAttributes['checkUpdateField'], dicValues[self.dicFileAttributes['checkUpdateField']], dicUser)
                    print 'update-id = ', updateID
                    if updateID > 0:
                        self.importID = updateID
                        if self.dicFileAttributes['updateData'] == 'IGNORE':
                            print 'Ignore new data'
                            self.importID = -77
                    else:
                        self.importID = -1
                    
                    
                    
                else:
                    print 'New Data found'
                    self.importID = -1
                    
                if  self.importID != -77 :
                    print 'save Data'
                    oSingleImport.saveExternalData(dicValues)

            elif self.dicFileAttributes['inputType'] == 'stock_goods':
                    print "dicValues = ", dicValues['article'][0], dicValues['st'][0]
                    self.insertGoods(stockID,dicValues['article'][0],float(dicValues['st'][0]), dicUser)

            elif self.dicFileAttributes['inputType'] == 'webshop_article':
                    dicValues['products_model'][0] = dicValues['products_model'][0].decode('latin-1').encode('utf-8')
                    dicValues['remark_w'][0] = dicValues['remark_w'][0].decode('latin-1').encode('utf-8')
                    dicValues['s9'][0] = dicValues['s9'][0].decode('latin-1').encode('utf-8')
                    if dicValues['s8'][0]:
                        dicValues['s8'][0] = dicValues['s8'][0].decode('latin-1').encode('utf-8')
                    
                    s9 = dicValues['products_model'][0][0:3]
                    if  s9 == '913' or s9 == '311' or  s9 == '301' or s9 =='302' or s9 =='303'  :
                        #print `dicValues`
                        result = self.insertWebshopArticle( dicValues, dicUser)
                    #print ' webshop-data for article', `result`
                    
                    
            if self.dicFileAttributes['mergeData'] == 'YES':
                print 'Next Data', sNext        
                s1 = sNext
            else:
                s1 = importFile.readline().strip()
                
            
            se += 1
            #print se
            #s1 = None
        importFile.close()
        self.executeSomeStuff(dicUser)
            
    
    def executeSomeStuff(self, dicUser):
        # after close make some stuff 
        try:
            liExtraFunction =  self.dicFileAttributes['extraFunction'].split(',')
            
            for eF in liExtraFunction:
                if eF.strip().upper() == 'UPDATEBANK':
                    ok = self.updateBank(dicUser) 
                
        except Exception, param:
            print 'EX 9'
            print Exception,param
            
            

    def insertGoods(self, stock, article_number, move, dicUser):
        
        article_id = None
        sSql = "select id from articles where number = '" + article_number + "' "
        sSql += self.getWhere("", dicUser, 2)
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        if result not in ['NONE','ERROR']:
           article_id = result[0]['id']
        
        if not article_id:
           dicValues = {}
           dicValues['number']= [article_number,'string']
           article_id = self.oDatabase.xmlrpc_saveRecord('articles',-1,dicValues, dicUser)
        try:   
            if article_id > 0:
                dicValues = {}
                dicValues['stock_id'] = [stock,'int']
                dicValues['article_id'] = [article_id,'int']
                if move > 0:
                   dicValues['to_embed'] = [move,'float']
                else:
                   dicValues['roll_out'] = [move,'float']
            
                result = self.oDatabase.xmlrpc_saveRecord('stock_goods',-1,dicValues, dicUser)
                if result == None:
                    result = 'NONE'
        except:
            result = 'NONE'
        return result
                     
    def verifyValues(self, dicValues):
        try:
            assert dicValues != None
            ##self.printOut( dicValues)
            for i in dicValues.keys():
                oValue = dicValues[i][0]
                sVerify = dicValues[i][1]
                
                if sVerify  == 'string':
                    # self.out( oValue)
                    if oValue:
                        pass
##                        try:
##                            oValue = oValue.encode('utf-8')
##                            
##                        except:
##                            self.out('No encoding')
                    # self.out( oValue)
                    # self.out( '++++++++++++++++++++++++++++++++++')

                elif sVerify  in ['int', 'integer']:
                    # self.out( oValue,self.INFO)
                    oValue = self.getCheckedValue(oValue,'int')
##                    try:
##                        if oValue == '':
##                            oValue = 0
##                    except:
##                        oValue = 0
##                    # self.out( oValue, self.INFO)
##                    # self.out( '++++++++++++++++++++++++++++++++++',self.INFO)
##                    #self.printOut( oValue)
##                    if (not isinstance(oValue, types.IntType)) and isinstance(oValue, types.StringType):
##                        if oValue.isdigit():
##                            oValue = int(oValue)
##                        else:
##                            oValue = string.strip(oValue)
##                            oValue = long(oValue[0:len(oValue) -1])
##
##                    elif isinstance(oValue, types.IntType):
##                        pass
##                    elif isinstance(oValue, types.LongType):
##                        pass
##
##                    else:
##                        oValue = 0
##                    if not ( isinstance(oValue, types.IntType) or isinstance(oValue, types.longType) ):
##                        oValue = 0

                elif sVerify  == 'float':
                    print 'verify float = ',   oValue
                    oValue = self.getCheckedValue(oValue, 'float')
                    if oValue == None:
                        oValue = 0.0
                    # self.out( oValue)
                    # self.out( '++++++++++++++++++++++++++++++++++')
                    ##self.printOut( oValue)
#                    elif (not isinstance(oValue, types.FloatType)) and isinstance(oValue, types.StringType) :
#                        oValue = string.replace(oValue,',','.')
#                        oValue = float(oValue)
#                    elif isinstance(oValue, types.FloatType):
#                        pass
#                    elif isinstance(oValue, types.IntType):
#                        oValue = float(oValue)
#
#                    else:
#                        oValue = 0.0
                    print 'verify float 2= ',   oValue
                elif sVerify == 'date' :
                    if oValue == '':
                        oValue = '01.01.1900'
                    else:
                        #self.printOut('Date by Verify:', oValue)
                        try:
                            oDate =  time.strptime(oValue, self.sqlDicUser['DateformatString'])
                        except:
                            try:
                                if oValue.find("-") == 4: 
                                    oDate =  time.strptime(oValue, 'YYYY-MM-DD')
                            except:
                                oValue = '1900/01/01'
                        oValue = time.strftime("%Y/%m/%d",oDate)
                        #self.printOut('Date by Verify 2:', oValue)
                #print i, dicValues[i]
                #print oValue, sVerify
                dicValues[i][0] = oValue
                dicValues[i][1] = sVerify

        except AssertionError:
            #self.printOut( 'assert error')
            dicValues = None
     
        #print "DicValues by readEntries = " ,  dicValues
        
        return dicValues
