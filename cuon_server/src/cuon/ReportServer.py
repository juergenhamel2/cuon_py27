import random
import xmlrpclib
from twisted.web import xmlrpc
 
from basics import basics
import Database
import base64
import Reports.report
import os,  sys, commands

import locale
from locale import gettext as _  
#import Reports.report2

class ReportServer(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        self.oDatabase = Database.Database()
        self.debugFinances = 1
        self.ReportDefs = {}
        
        self.report = Reports.report.report()
        #self.report2 = Reports.report2.report2()
##    def xmlrpc_server_hibernation_incoming_document(self, dicOrder, dicUser):
##        import Reports.report_hibernation_incoming_document
##        import Garden
##        
##        print "startReport"
##        oGarden = Garden.Garden()
##        oReports = Reports.report_hibernation_incoming_document.report_hibernation_incoming_document()
##        rep = oReports.ServerStartReport(dicOrder, dicUser, oGarden, self.ReportDefs)
##        print "ok Report"
##        print len(rep)
##        en =  base64.encodestring(rep)
##        return en
##        
    def xmlrpc_createReport(self, *reportdata):
        print 'start ReportServer --> createReport : reportdata', reportdata
        
        rep = self.report.start(reportdata)
        #rep = oReports.ServerStartReport(dicOrder, dicUser, oGarden, self.ReportDefs)
        print "ok Report"
        print len(rep)
        en =  base64.encodestring(rep)
        return en

#    def xmlrpc_createReport2(self, *reportdata):
#        print 'start ReportServer --> createReport : reportdata', reportdata
#        
#        rep = self.report2.start(reportdata)
#        #rep = oReports.ServerStartReport(dicOrder, dicUser, oGarden, self.ReportDefs)
#        print "ok Report"
#        print len(rep)
#        en =  base64.encodestring(rep)
#        return en
    def findReportFile(self, sFile, dicUser):
        print 'load = ',  sFile
        if sFile:
            dirNorm = os.path.dirname(sFile)
            sDirFile = os.path.basename(sFile)
            dirClient = dirNorm[0:len(dirNorm) -4] + '/client_' + `dicUser['client']`.strip("'") + '/' + sDirFile
            dirUser = dirNorm[0:len(dirNorm) -4] + '/user_' + dicUser['Name'].strip("'") + '/' + sDirFile
                
            #print 'Pathes for dir'
            #print dirNorm
            #print dirClient
            #print dirUser
            
            
            if os.path.exists(dirUser):
                sFileName = dirUser
            elif os.path.exists(dirClient):
                sFileName = dirClient
            else:
                sFileName = sFile
                
            sFileName.replace("'","")
            return sFileName

        
    def xmlrpc_createJReport(self,dicUser, reportPath, liSearchfields,dicReportData, reportDefs ):
       #.createJReport(dicUser, reportPath, liSearchfields,dicReportData, reportDefs ) 
       # values in this order:
       # 1 reportname
       # 2 dicUser
       # 3 liSearchlist
       # 4 dicReportData
       # 5 reportDefs
       #print "reportata = ", reportdata
       
      
       
       pathToJasperstarter = "/usr/share/cuon/jasperstarter/bin/jasperstarter"
       reportFile = self.findReportFile(reportPath, dicUser)
       pdfFile = reportDefs['pdfFile']
       pwdPath = os.getcwd()
       pdfFile = pdfFile.replace("\'","")
       pdfFile1 =  pdfFile[:-4]
       newPath = os.path.dirname(reportFile)
       output_file = self.getRandomFilename(".pdf")
       
       shellCommand = "cd " + newPath + " ; " + pathToJasperstarter + " pr " + reportFile + " -t postgres  -u " + dicUser["Name"].strip("'") +" -f pdf -H " +self.POSTGRES_HOST +" --db-port "+ `self.POSTGRES_PORT` + " -n cuon -o " + pdfFile1 +" -P "

       for param in liSearchfields:
           
           # Parameter
           shellCommand += " " + param

       shellCommand += "  CUON_TITLE=" +'"'+ _("Address Phonelist1")+'"'
       
       # CUON_TITLE="Cashdesk"  cash_date_from=01.05.2015  cash_date_to=15.05.2015 cash_sign_from= cash_sign_to= cash_nr_from= cash_nr_to=  cash_id_from= cash_id_to= sType= 
       print shellCommand
       
       liStatus = commands.getstatusoutput(shellCommand)

       print "generated pdffile = ", pdfFile
       
       return pdfFile
       
