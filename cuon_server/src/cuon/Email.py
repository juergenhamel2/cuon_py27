# -*- coding: utf-8 -*-

##Copyright (C) [2003, 2004, 2005, 2006, 2007]  [Juergen Hamel, D-32584 Loehne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 


# Import smtplib for the actual sending function
#import smtplib, sys
import xmlrpclib
from twisted.web import xmlrpc
from twisted.internet.threads import deferToThread
from twisted.internet import threads
from basics import basics
from Email2 import Email 
import Database
import time, os
import email.mime.text
from copy import deepcopy


class cuonemail(xmlrpc.XMLRPC, basics):

    def __init__(self):
        
        
        basics.__init__(self)
        self.oDatabase = Database.Database()
        
    def xmlrpc_getCryptCombobox(self):
        
        return self.EMAILLOGINCRYPT
      
        
    def xmlrpc_sendTheEmail(self, dicValues, liAttachments,dicUser ):
        ok = ''
            
        if dicValues.has_key('To'):
            if dicValues['To'][0:11] == 'Newsletter:':
                cNL = dicValues['To'][12 :]
                print cNL
                liNL = cNL.split(',')
                for oneNL in liNL:
                    oneNL = oneNL.strip()
                    if oneNL:
                        result = self.getNewsletterEmail(oneNL,dicUser)
                        print 'result = ', result 
                        for sm in result:
                            #print 'sm = ',  sm
                            if sm.has_key('email'):
                                #print 'sm[email] = ',  sm['email']
                                dicValues2 = deepcopy(dicValues)
                                dicValues2['To'] = deepcopy(sm['email'])
                                dicValues2['sm'] = deepcopy(sm)
                                ok += self.sendEmail(dicValues2, liAttachments,dicUser) + '\n'
                            
                       
                        
            else:
                #ok += deferToThread(self.sendEmail, dicValues, liAttachments,dicUser) +'\n'
                ok += self.sendEmail(dicValues, liAttachments,dicUser) +'\n'
            
        return ok
                
    def sendEmail(self, dicValues, liAttachments,dicUser ):
        cuonmail = Email(smtp_server = "localhost")
        print ' send mail'
        if liAttachments:
            cuonmail.attachments = liAttachments
        else:
            cuonmail.attachments = []
            

        ok = ''
        try:
            dicValues = self.replaceValues(dicValues)
              
            if dicUser.has_key('Email'):
                dicEmail = dicUser['Email']
##                self.Email['From']='MyAddress@mail_anywhere.com'
##                self.Email['Host']='mail_anywhere.com'
##                self.Email['Port']='25'
##                self.Email['LoginUser']='login'
##                self.Email['Password']='secret'
##                self.Email['Signatur']='NONE'

                print '1'
                if dicEmail['LoginUser'] != 'login':
                    self.EMAILUSER = dicEmail['LoginUser']
                print '2'   
                if dicEmail['Password'] != 'secret':
                    self.EMAILPASSWORD = dicEmail['Password']
                print '3'    
                if dicEmail['Host'] != 'mail_anywhere.com':
                    self.EMAILSERVER = dicEmail['Host']
                      
            
            if dicValues.has_key('From'):
                cuonmail.from_address = dicValues['From']
            print '4'
            print dicValues
            if dicValues.has_key('To'):
                #print 'send mail to ',  dicValues['To']
                liTo = dicValues['To'].split(',')
                for s in liTo:
                    cuonmail.recipients.add(s) 
            if dicValues.has_key('CC') and dicValues['CC'] is not None:
                #print 'send mail to ',  dicValues['To']
                liCc = dicValues['CC'].split(',')
                for s in liCc:
                    cuonmail.cc_recipients.add(s)    
            if dicValues.has_key('BCC') and dicValues['BCC'] is not None:
                #print 'send mail to ',  dicValues['To']
                liBcc = dicValues['BCC'].split(',')
                for s in liBcc:
                    cuonmail.bcc_recipients.add(s)   
            print '6'   
            if dicValues.has_key('Subject'):
                cuonmail.subject = dicValues['Subject']
            else:
                cuonmail.subject = 'No Subject'
            print '7'
            if dicValues.has_key('Body'):
                #print 'dicValues = ',  dicValues.keys()
                
                
                # workaround for python 2.6
                try:
                    dicValues['Body'] = dicValues['Body'].encode('UTF-8')
                except:
                    pass
             
                dicValues['Body']  = self.normalizeHtml(dicValues['Body'] )
                cuonmail.message  = dicValues['Body'] 
            print '8'
            cuonmail.smtp_server = self.EMAILSERVER
            print '9'
            cuonmail.smtp_user = self.EMAILUSER
            print '10'
            cuonmail.smtp_password = self.EMAILPASSWORD
            print '11'
            cuonmail.smtp_crypt = dicEmail['Crypt']
            print '12'
            
      
        
            s = None
            try:
                s = cuonmail.send()
            except Exception, params:
                print Exception
                print ' -----------------'
                print  params
                s = params
                
            try:
                print 'return Value form Email2 ', s
                print 'Status = ', cuonmail.statusdict
                print 's = ', s
                if not s:
                    s = 'Email '
                    try:
                        s += 'send : ' +  dicValues['To'] + ', ' + `dicValues['Subject']`
                    except:
                        s += ' wrong To or subject'
                else:
                    s = `s`
                ok = s
                if  dicValues['sm'].has_key('addressid'):
                    ok += ';p='
                else:
                    ok += ';a='
                ok += `dicValues['sm']['id']`  
                
                f = open('/var/log/cuonmail.log','a')
                f.write(time.ctime(time.time() ))
                f.write('     ')
                f.write(ok)
                f.write('\n')
                f.close()
            except:
                pass
        except Exception, params:
            print 'Error in Email'
            print Exception, params
        return ok
        
    def getNewsletterEmail(self, NewsletterShortcut, dicUser):
        print NewsletterShortcut
        sSql = "select * from address where newsletter ~'.*" + NewsletterShortcut +".*'"
        sSql += self.getWhere("",dicUser,2)
        print sSql
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        if result in ['NONE','ERROR']:
            result = []
        #print 'result 1 ', result
        sSql = "select * from partner where newsletter ~'.*" + NewsletterShortcut +".*'"
        sSql += self.getWhere("",dicUser,2)
        print sSql
        result2 = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        print 'result2 = ', result2
        if result2 not in ['NONE','ERROR']:
            for res in result2:
                result.append(res)
        print 'result3', result
        return result
        
    def replaceValues(self, dicValues) :
        try:
            dicVars = dicValues['sm']
            for v in ['Body','Subject']:
                s = dicValues[v]
                for key in dicVars.keys():
                    try:
                        try:
                            if self.checkType(dicVars[key], 'unicode'): 
                                
                                dicVars[key] = dicVars[key].encode('utf-8')
                        except Exception, params:
                            print Exception, params
                            
                        #print 'try to replace this ', key,  dicVars[key]
                        if dicVars[key] == 'NONE' or dicVars[key] == None:
                            s = s.replace('##'+ key + ';;','')
                        elif self.checkType(dicVars[key], 'string') or self.checkType(dicVars[key], 'unicode'):
                            
                            #print dicVars[key],  dicVars[key].decode('utf-8'),  dicVars[key].decode('utf-8').decode('utf-8')
                            #print dicVars[key].decode('latin-1'),  dicVars[key].decode('utf-8').encode('latin-1')
                            s = s.replace('##'+ key + ';;',self.normalizeHtml(dicVars[key]))
                        
                        else:
                            s = s.replace('##'+ key + ';;',`dicVars[key]` )
                    except Exception,  params:
                        print Exception, params
                        s = s.replace('##'+ key + ';;','')
                dicValues[v] = s 
        except Exception, params:
            print Exception, params
        
        
        
        return dicValues


    def xmlrpc_checkImap(self, dicUser):
        d = threads.deferToThread(self.start_imap_dms,dicUser)
        d.addCallback(self.handleResult)
      

        return 1

    def start_imap_dms(self,dicUser):
        imapD = imap_dms(dicUser )
        imapD.run()
        return 1

    
    def handleResult(self,i):
        print "handle Result"
        return i

    
##        Email(
##557           from_address = "server@gp-server.gp",
##558           smtp_server = "gp-server.gp",
##559           to_address = "gerold@gp-server.gp",
##560           subject = "Einfaches Beispiel (öäüß)",
##561           message = "Das ist der Nachrichtentext mit Umlauten (öäüß)"
##562       ).send()


        
##        COMMASPACE = ', '
## 
##        # Create the container (outer) email message.
##        self.msg = MIMEMultipart('related')
##        
##        # Guarantees the message ends in a newline
##        self.msg.epilogue = ''
##
##        self.mFiles = []
##        
##        
##
##    def setSubject(self, s):
##        if s:
##            self.msg['Subject'] = s
##        else:
##            self.msg['Subject'] = 'No Subject'
##            
##
##    def setFrom(self,s):
##        self.msg['From'] = s
##        
##
##    def setTo(self,s):
##        self.msg['To'] = s
##        
## 
##    def addAttachments(self, *maFiles):
##        for j in range(0, len(maFiles)):
##            self.mFiles.append( maFiles[j])
##
##    def setBody(self,body=None ):
##        if body:
##            self.msg.preamble =  body 
##        else:
##            self.msg.preamble = 'No Text' 
##            
##            
##                                                                                
##    def xmlrpc_sendTheEmail(self, dicValues, liAttachments,dicUser ):
##        ok = False
##        
##        if dicValues:
##            
##            try:
##                if dicValues.has_key('Username'):
##                    self.EMAILUSER = dicValues['Username']
##                if dicValues.has_key('Password'):
##                    self.EMAILPASSWORD = dicValues['Password']
##                if dicValues.has_key('To'):
##                    self.setTo(dicValues['To'])
####                if dicValues.has_key('From'):
####                    self.setFrom(dicValues['From'])
##                if dicValues.has_key('Subject'):
##                    self.setSubject(dicValues['Subject'])
##                if dicValues.has_key('Body'):
##                    self.setBody(dicValues['Body'])
##            except:
##                pass
##                    
##        # Assume we know that the txt files are all in ascii format
##        if liAttachments:
##            for file in self.mFiles:
##                # Open the files in binary mode.  Let the MIMEText class automatically
##                # guess the specific image type.
##                fp = open(file, 'rb')
##                txt = MIMEText(fp.read())
##                fp.close()
##                self.msg.attach(txt)
##                 # Send the email via our own SMTP server.
##            try:
##                
##                print ' start send email'
##         replaceValues(dicValues)       server = smtplib.SMTP(self.EMAILSERVER)
##                #print 'Email-Server = ', server
##                #print self.EMAILUSER, self.EMAILPASSWORD
##                
##                server.login(self.EMAILUSER, self.EMAILPASSWORD)
##                #print 'login'
##                #print self.msg.as_string()
##                
##                server.sendmail(dicValues['From'], dicValues['To'], 'To: ' + dicValues['To'] + '\nSubject: cuon 7 \n\n ' + dicValues['Body'] + '\n')
##                print 'send'
##                server.quit()
##                ok = True
##            except Exception, param:
##                print Exception
##                print param
##            
##        # Normal Email without Attachment    
##        else:
##                # Send the email via our own SMTP server.
##            try:
##                
##                print ' start send email'
##                server = smtplib.SMTP(self.EMAILSERVER)
##                #print 'Email-Server = ', server
##                #print self.EMAILUSER, self.EMAILPASSWORD
##                msgText = MIMEText(dicValues['Body'])
##                server.login(self.EMAILUSER, self.EMAILPASSWORD)
##                #print 'login'
##                #print self.msg.as_string()
##                print dicValues
##                s = server.sendmail(dicValues['From'], dicValues['To'], 'To: ' + dicValues['To'] + '\nSubject: ' + dicValues['Subject'] +' \n\n ' + msgText + '\n')
##                print 'send', s
##                server.quit()
##                ok = True
##            except Exception, param:
##                print Exception
##                print param
##            
##        return ok
##            
##            
##        


# -*- coding: utf-8 -*-
##Copyright (C) [2009]  [Juergen Hamel, D-32584 Loehne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import imaplib, string, email, getpass, sys, time
import os,  time
import locale
from locale import gettext as _

from email import message_from_string
from email.header import decode_header
from email import parser
from threading import Thread
import base64
import Database
import DMS
import Address
import Misc

class imap_dms( basics):
    def __init__(self, dicUser):
        #Thread.__init__(self)
        #constants.__init__(self)
        #dumps.__init__(self)
        basics.__init__(self)

        self.oDatabase = Database.Database()
        self.oAddress = Address.Address()
        self.oDMS = DMS.DMS()
        self.oMisc = Misc.Misc()
        

        sSql = "select * from preferences where user_id = '" + dicUser["Name"] + "' " +  self.getWhere("",dicUser,2)
        self.dicUser = dicUser
        
        #try:
        self.Prefs =  self.oDatabase.xmlrpc_executeNormalQuery(sSql, self.dicUser )[0]
        print "self.prefs = ", self.Prefs
        print "user_check_imap = ", self.Prefs["user_check_imap"]
        if self.Prefs["user_check_imap"] == 't':
           
            self.imap_server =  self.Prefs["email_user_imap_host"]
            self.imap_port =  self.Prefs["email_user_imap_port"]
            
            self.imap_user =  self.Prefs["email_user_imap_loginname"]

            
            self.imap_password =  self.Prefs["email_user_imap_password"]

            print "User + passwd = ",  self.imap_user,  self.imap_password
            #seconds to sleep between each check
            

            # if 1 use SSL, otherwise don't
            if self.Prefs["user_imap_email_ssl"] == "t":
                self.use_ssl = True
            else:
                self.use_ssl = False
                
            self.use_crypt= self.Prefs["email_user_imap_crypt"]
            # number of seconds to display message
            self.display_timeout=5

            # if 1, print some debug info; otherwise don't
            self.debug=0
         

            # color of message
         

            #self.singleDMS = cuon.DMS.SingleDMS.SingleDMS(allTables)
            #self.documentTools = cuon.DMS.documentTools.documentTools()
            #self.misc = cuon.Misc.misc.misc()
        #except Exceptions, params:g
           # print "imap error", Exceptions, params
            
            
    def decodeMailHeader(self, sSubject1):
        noDecode = False
        sSubject = ''
        for iSub in sSubject1:
            try:
                if iSub[1]:
                    sSubject += iSub[0].decode(iSub[1]) + ' '
                else:
                    sSubject += iSub[0]+ ' '
                                               
            except:
                noDecode = True
        if noDecode:
            sSubject = sSubject1
        return sSubject
        
    
    def run(self):
        M = None
        ok = False
        liPartnerID = []
        liAddressID = []
        liPartnerID2 = []
        liAddressID2 = []
        
        if self.Prefs["user_check_imap"] == 't':
            #print 'Imap check is True'
            try:
                
                if self.use_ssl:
                    if  self.imap_port == 0:
                        M = imaplib.IMAP4_SSL(self.imap_server)
                    else: 
                        M = imaplib.IMAP4_SSL(self.imap_server, self.imap_port)
                        
                else:
                    if  self.imap_port == 0:
                        M = imaplib.IMAP4(self.imap_server)
                    else:
                        M = imaplib.IMAP4(self.imap_server,  self.imap_port)

                    
            except Exception, e:
                #print sys.exc_info()[0]
                print 'IMAP Server error: ', Exception, e
                M = None
                
                    
            
        try:
            if M:
                if  self.use_crypt < 1 or self.use_crypt == 2 or self.use_crypt == 3:
                    try:
                        res = M.login(self.imap_user, self.imap_password)
                    except Exception, e:
                        print 'IMAP login error 0: ', Exception, e   
                elif self.use_crypt == 4:
                    try:
                        res = M.login_cram_md5(self.imap_user, self.imap_password)
                    except Exception, e:
                        print 'IMAP login error 1: ', Exception, e   
                elif self.use_crypt == 1:   
                    try:
                        res = M.login(self.imap_user, self.imap_password)
                    except:
                        try:
                            res = M.login_cram_md5(self.imap_user, self.imap_password)
                        except Exception, e:
                            print 'IMAP login error 2: ', Exception, e   
                else:
                    try:
                        res = M.login(self.imap_user, self.imap_password)
                    except Exception, e:
                            print 'IMAP login error 2: ', Exception, e 
                    
                # Get inbox status
                #mboxes = M.list()
                #imap.SelectMailbox("Inbox")
                #print mboxes
                #status = M.status('INBOX.AUTOMATIC_CUON', '(UNDELETED)')
    
                # Parse status string
                # ie. ('OK', ['"INBOX" (UNSEEN 0)'])
                #print status
                
                #print 'msgCount = ',  status[1][0].split()[2].split(')')[0]
    
                M.select('INBOX.AUTOMATIC_CUON')
                r, data = M.search(None, '(ALL)')
                print r,  data
                if r == 'OK':
                    for singleMail in data:
                        #print 'singlemail = ',  singleMail
                        liNumbers = singleMail.split(' ')
                        for oneNumber in liNumbers:
                            self.message1 = None
                            oneNumber = oneNumber.strip()
                            print 'oneNumber = ',  oneNumber
                           
                            mail = M.fetch(int(oneNumber),  '(RFC822)')
                            print 'mail found',  mail
                            self.message1 = mail[1][0][1]

                            message = message_from_string(self.message1)
                           
                            #self.message1 = mail[1][0][1]

                            #message = message_from_string(self.message1)
                            #message = parser.Parser().parsestr(self.message1)
                            #sFrom = email.header.decode_header(message['From'])
                            sFrom = self.decodeMailHeader( email.header.decode_header(message['From']))
                            #print 'From = ',  sFrom
                            #sTo = email.header.decode_header(message['To'])
                            sTo = self.decodeMailHeader( email.header.decode_header(message['To']))
                            #print 'sTo = ',  sTo
                            
                            sDate = self.decodeMailHeader( email.header.decode_header(message['Date']))
                            #print 'sDate = ',  sDate
                            
#                            sTo = self.decodeMailHeader( email.header.decode_header(message['To']))
#                            print 'sTo = ',  sTo
# 
 
                            sSubject = self.decodeMailHeader(email.header.decode_header(message['Subject']))
                            #print 'sSubject = ',  sSubject
                            #sSubject = self.decodeMailHeader(sSubject1)
                                    
                            #print 'sSubject = ',  sSubject
                            #print 'keys : ',  message.keys()
                            sMessageID = message['Message-ID'].strip('<').strip('>')
                            #print sMessageID
                                    # check if an address has this email address
                            try:
                                try:
                                    sFrom = base64.encodestring(sFrom)
                                    
                                except:
                                    sFrom = ""
                                try:
                                    sTo = base64.encodestring(sTo)
                                except:
                                    sTo = ""


                                liAddressID = self.oAddress.xmlrpc_getAddressEmailID('address',  sFrom,self.dicUser)
                                print "liAddressID = ", liAddressID
                                if sFrom:
                                    if  liAddressID == None or liAddressID == [0]:
                                        liAddressID = []

                                        liPartnerID = self.oAddress.xmlrpc_getAddressEmailID('partner',sFrom,self.dicUser)
                                        if liPartnerID == None or liPartnerID == [0]:
                                            liPartnerID = []

                                if sTo:
                                    liAddressID2 = self.oAddress.xmlrpc_getAddressEmailID('address',sTo,self.dicUser)
                                    if  liAddressID2 == None or liAddressID2 == [0]:
                                        liAddressID2 = []
                                
                               
                                    liPartnerID2 = self.oAddress.xmlrpc_getAddressEmailID('partner',  sTo,self.dicUser)
                                    if  liPartnerID2 == None or liPartnerID2 == [0]:
                                        liPartnerID2 = []
                                
                                if liAddressID2:
                                    liAddressID.extend(liAddressID2)
                                if liPartnerID2:
                                    liPartnerID.extend(liPartnerID2)
                                    
                            except Exception, params:
                                print Exception, params
                                


                            print 'Address ID`s: ',  liAddressID, liPartnerID
                            iSearch = sSubject.find('AUTOMATIC_CUON ADDRESS ID:')
                            if iSearch != -1:
                                iSearch += 26
                                liStr = sSubject[iSearch:sSubject.find(';') -1].split(',')
                                print 'liStr = ',  liStr
                                for iID in liStr:
                                    try:
                                        nID = int(iID.strip())
                                        print 'nID = ',  nID
                                        liAddressID.append(nID)
                                    except:
                                        pass
                                sSubject = sSubject[sSubject.find(';')+1:]      
                                print 'new sSubject = ',  sSubject
                            iSearch = sSubject.find('AUTOMATIC_CUON PARTNER ID:')
                            if iSearch != -1:
                                iSearch += 26
                                liStr = sSubject[iSearch:sSubject.find(';')].split(',')
                                for iID in liStr:
                                    try:
                                        nID = int(iID.strip())
                                        liPartnerID.append(nID)       
                                    except:
                                        pass
                                sSubject = sSubject[sSubject.find(';')+1:]        
                            for part in message.walk():
                                print 'email part = ',  part.get_content_type() 
                            # each part is a either non-multipart, or another multipart message
                            # that contains further parts... Message is organized like a tree
                                sType = None
                                sExtension = None
                                if part.get_content_type() == 'multipart/mixed':
                                    print 'mixed'
                                    # solve later, try to find all 
                                elif part.get_content_type() == 'application/octet-stream':
                                    sType = 'Stream'    
                                else:
                                    try:
                                        sType = self.MimeType[part.get_content_type()][0]
                                    except:
                                        print 'error to find part type = ',  part.get_content_type()
                                        sType = None
                                print 'sType = ',  sType        
                                if sType:
                                    self.save(part,sType,  liAddressID,  liPartnerID,sSubject,   sFrom,  sTo ,  sDate )

                            # create a new Email for Save
                            fname = self.getRandomFilename(".eml")
                            fm = open(fname,"wb")
                            fm.write(self.message1)
                            fm.close()
                            sFrom = base64.decodestring(sFrom)
                            sTo = base64.decodestring(sTo)

                            # Action, save mail to DMS
                            if liAddressID and liAddressID not in ['NONE', 'ERROR']:
                                for id in liAddressID:
                                    if id > 0:
                                        print ' ID = ',  id
                                        dms_id = self.oDMS.save2DMS( fname,"email" , 'Address',  id, _("Email: ")+ sSubject,  sFrom,  sTo,  sDate, self.dicUser)    
                            if liPartnerID and liPartnerID not in ['NONE', 'ERROR']:
                                for id in liPartnerID:
                                    if id > 0:
                                        print ' ID Partner = ',  id
                                        dms_id =self.oDMS.save2DMS(fname, "email", 'Partner',  id,  _("Email: ")+ sSubject,  sFrom,  sTo, sDate, self.dicUser)         
                            print 'dms_id = ', dms_id
                            # remove file

                            os.remove(fname)

                            
                            # Delete mail and next mail
                            print "Delete mail and next mail",sType,"--",liAddressID,"---",liPartnerID,"!!"
                            if (liAddressID or liPartnerID):
                                print "Now set the Delete Flag "
                                success = M.store(oneNumber, '+FLAGS', '\\Deleted')
                                print 'Success 1 : ',  success
                                success = M.expunge()
                                print 'success 2 : ',  success
              
            success = M.expunge()
            print 'success 20 = ',  success 
        except Exception, e:
            #print sys.exc_info()
            print 'IMAP read error: ', Exception,  e
            success = M.expunge()
            print 'success 30 = ',  success
            
        try:
            if M:
                success = M.expunge() 
                M.logout()
        except e,  params: 
            print e,  params
        
        
        return ok
            
    def save(self, part , sType,  liAddressID,  liPartnerID, sSubject,   sFrom,  sTo,  sDate):
        print 'save it',  sType
        doSave = True
        if sType == 'Stream':
            sType = 'pdf'
            sFile = part.get_filename()
            #print 'sFile = ',  sFile
            if sFile:
                iFileExt = sFile.rfind('.')
                if iFileExt > -1:
                    sType = sFile[iFileExt +1:]
            else:
                sType = 'bin'
            #print 'new sType = ',  sType
        
        sExtension = '___dms.' + sType
        fname = self.getRandomFilename(sExtension)
        #print fname
        cwd = os.getcwd() 
        #print cwd
        fname = cwd + '/' +fname
        f1 = open(fname , 'w')
        try:
            f1.write(part.get_payload(decode=True))
        except:
             f1.write(part.get_payload())
        f1.close()
        #print 'fname = ',  fname
        #print 'sType = ',  sType
        #print 'sTo = ',  sTo
        #print 'sFrom = ',  sFrom
        #print 'sSubject = ',  sSubject
        dms_id = 0
        #print part.get_payload() # prints the raw text

        sFrom = base64.decodestring(sFrom)
        sTo = base64.decodestring(sTo)
        #sSubject = base64.decode(sSubject)
        #sDate = base64.decode(sDate)
        #print 'sTo = ',  sTo
        #print 'sFrom = ',  sFrom
        #print 'sSubject = ',  sSubject
        #print "sDate = ", sDate
        fMailname = self.getRandomFilename(".eml")
        print "10 fMailname", fMailname
        cwd = os.getcwd() 
        #print cwd
        fMailname = cwd + '/' +fMailname
        f2 = open(fMailname , 'wb')
        f2.write(self.message1)
        f2.close()
        
        if liAddressID and liAddressID not in ['NONE', 'ERROR']:
            for id in liAddressID:
                if id > 0:
                    print ' ID = ',  id
                    dms_id = self.oDMS.save2DMS( fname, sType, 'Address',  id,  sSubject,  sFrom,  sTo,  sDate, self.dicUser)
                    dms_id2 = self.oDMS.save2DMS( fMailname, "eml", 'Address',  id,  sSubject,  sFrom,  sTo,  sDate, self.dicUser)
        if liPartnerID and liPartnerID not in ['NONE', 'ERROR']:
            for id in liPartnerID:
                if id > 0:
                    print ' ID Partner = ',  id
                    dms_id =self.oDMS.save2DMS(fname, sType, 'Partner',  id,  sSubject,  sFrom,  sTo, sDate, self.dicUser)
                    dms_id2 =self.oDMS.save2DMS(fMailname, "eml", 'Partner',  id,  sSubject,  sFrom,  sTo, sDate, self.dicUser)         
        print 'dms_id = ', dms_id
            
        if dms_id:
            print 'dms stuff = ', dms_id,  sType
            
            s = self.oMisc.xmlrpc_getTextExtract( dms_id, sType,  self.dicUser)
            
            #print 'extract = ',  s
            
        
            
   
        
        os.remove(fname)
        os.remove(fMailname)
      
