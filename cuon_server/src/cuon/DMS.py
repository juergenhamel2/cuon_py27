import time
from datetime import datetime
import random
import xmlrpclib
from twisted.web import xmlrpc
from basics import basics
import Database,User
import hashlib
import bz2
import base64
import re,string
import binascii
import os.path
import shlex, subprocess
import zipfile

# localisation
#import locale, gettext

import locale
from locale import gettext as _


class DMS(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        self.oDatabase = Database.Database()
        self.oUser = User.User()

       
        
   

      
        

    def convert2PDF(self, data, file_suffix):
        postfix = None
        file_suffix = file_suffix.lower()
        #libreoffice --invisible --norestore -pt Virtual_PDF_Printer Dokumente/cuon_seq_liste.pdf
        sPDF = self.getNewUUID()
        sOutFile = "/tmp/" + sPDF + "." + file_suffix
        sOutdir = "/root/PDF/"
        
        f = open(sOutFile, "wb")
        f.write(data)
        f.close()
        if file_suffix in  self.FileTypes['Office']:
            sCommand = '"' + self.PDF_CREATOR['OFFICE'] + '"'
        elif file_suffix in  self.FileTypes['Image']:
            sCommand = '"' + self.PDF_CREATOR['IMAGE'] + '"'
            postfix = sOutdir + sPDF + ".pdf"
        elif file_suffix in  self.FileTypes['ebook']:
            sCommand = '"' + self.PDF_CREATOR['EBOOK'] + '"'
            postfix = sOutdir + sPDF + ".pdf"
        else:
            sCommand = None
        newData = 'NONE'
       
        if sCommand:
       
            try:
                sDoit = "/usr/share/cuon/cuon_server/bin/printToPDF.sh " + sCommand + " " +  sOutFile
                if postfix:
                    sDoit += " " + postfix 
                shellcommand = shlex.split(sDoit  )
                print 'shellcommand = ',  shellcommand
                liStatus = subprocess.Popen(shellcommand)
                liStatus.wait()
            except Exception,  params:
                print Exception, params
       
            try:
                sFile = sOutdir + sPDF+ ".PDF"
                n1 = 0
                while n1 < 5:
                    time.sleep(2)
                    ok = False
                    if os.path.isfile(sFile):
                        ok = True
                    else:
                        sFile = sOutdir + sPDF+ ".pdf"
                        if os.path.isfile(sFile):
                            ok = True
                        else:
                            print 'no pdf file exist'
                    if ok:
                        f = open(sFile, "rb")
                        newData = f.read()
                        f.close()
                        break
                    n1 += 1
                    
            except Exception,  params:
                print "DMS 01 = ", Exception,  params
            print 'len of newData = ',  len(newData)
        return newData
            
          
          
          
    def xmlrpc_sl_showDMS(self, dicUser, ID,  iModul, sTitle=None, sCategory=None,  sSub1=None):
        sSql = "select id, title, category, sub1 from dms where insert_from_module = "
        sSql += `iModul` + " and sep_info_1 = " + `ID`  
        if sTitle:
            sSql += " and title ~*'" + sTitle +"' "
        if sCategory:
            sSql += " and category ~*'" + sCategory +"' "
        if sSub1:
            sSql += " and sub1 ~*'" + sSub1 +"' "
        
        sSql += self.getWhere("", dicUser,  2 ) + " ORDER BY title LIMIT 20"
        
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        
        retValue = {}
        if result and result not in self.liSQL_ERRORS:
            
            for row in result:
                if not row['title']:
                    row['title'] = 'NONE'
                if not row['category']:
                    row['category'] = 'NONE'
                if not row['sub1']:
                    row['sub1'] = 'NONE'   
                retValue[`row['id']`] = row['title'] + ", " + row['category'] +", " + row['sub1']
                
        else:
            retValue = {'0':'NONE'}     
        
        return retValue


    def xmlrpc_sl_loadPDF(self, dmsID, dicUser):
        
        sSql = 'select file_suffix, document_image from dms where id = ' + `dmsID`
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
        if liResult and liResult not in self.liSQL_ERRORS:
            file_suffix = liResult[0]['file_suffix']
            s = liResult[0]['document_image']
           
                
            try:
                b = base64.decodestring(s)
                c = bz2.decompress(b)
                if file_suffix.upper() not in [ 'PDF']:
                     c = self.convert2PDF(c, file_suffix)
                imageData = base64.encodestring(c)
                
            except Exception, param:
                print Exception, param
                imageData = "NONE"
        
        print 'len of newData 2 = ',  len(imageData)    
        return imageData

    def xmlrpc_getExtractText(self, dms_id,  dicUser):
        sSql = "select fct_ExtractTextFromDMS(" + `dms_id` + ") as etext "
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
        return liResult[0]['etext']

    def xmlrpc_getNormalDMSFields(self, id, dicUser):
        sSql = "select title,  category ,  sub1, sub2,sub3,sub4,sub5 from dms where id = " + `id` 
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
        if liResult not in self.liSQL_ERRORS:
            return liResult[0]
        else:
            return 'NONE'
        
    def xmlrpc_getImage(self, id, dicUser):

        print "get Image for id = ", id  
        sSql = 'select fct_get_dms_document_image('+ `id` +') as document_image'
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
       

        if liResult not in self.liSQL_ERRORS:
            #print "Result", len(liResult[0]), liResult[0]["document_image"]
            return liResult[0]["document_image"]

        else:
            return "NONE"


    def xmlrpc_getData(self,id,liVars,dicUser):

        d = self.xmlrpc_getImage(id, dicUser)
        if d not in self.liSQL_ERRORS:
            b = base64.decodestring(d)
            s = bz2.decompress(b)
        else:
            s = d

        
        
        dicVars = self.list2Dic(liVars)

        tmpFile = self.getRandomFilename()

        f1 =  open(tmpFile ,'a')
        
        
        f1.write(s)
        f1.close()


        
        try:
            if zipfile.is_zipfile(tmpFile):
                print 'zipfile found'
                z1 = zipfile.ZipFile(tmpFile,'a')
                print z1.namelist()
                for f1 in ['content.xml', 'styles.xml']:
                    f_in = str(z1.read(f1))
                    #print 'content.xml', f_in

                    f_in = self.replaceValues(dicVars,f_in, dicUser)
                    #print 'replaced Content', f_in
                    z1.writestr(f1,f_in)

                z1.close()

            else:

                f_out = open(tmpFile + '_w1','a')

                f_in = open(tmpFile,'r')
                if f_in and f_out:
                    s = f_in.readline()
                    while s:
                        s = self.replaceValues(dicVars,s, dicUser)                                 

                        f_out.write(s)
                        s = f_in.readline()
                    f_in.close()
                    f_out.close()

                    tmpFile = tmpFile + '_w1'
                else:
                    'error read/create tmp-file'
        except Exception, param:
            print Exception
            print param


        f1 = open(tmpFile,"r")
        c = f1.read()
        f1.close()

        s = base64.encodestring(c)
        
            
        

        return s
    
    def xmlrpc_sl_getImage(self, id, dicUser):
          
        s = self.xmlrpc_getImage(id, dicUser)
        if s not in self.liSQL_ERRORS:
            b = base64.decodestring(s)
            c = bz2.decompress(b)
        else:
            c = s
        c= base64.encodestring(c)
        return c


    def xmlrpc_getSuffix(self, dmsID, dicUser):
        print "get suffix for id = ", id  
        file_suffix = "NONE"
        sSql = 'select file_suffix from dms where id = ' + `dmsID`
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )

        if liResult and liResult not in self.liSQL_ERRORS:
            file_suffix = liResult[0]['file_suffix']

        return file_suffix

    def xmlrpc_getViewExe(self, dmsID, dicUser):
        print "get exe to view"
        file_exe = "NONE"
        file_suffix = self.xmlrpc_getSuffix(dmsID, dicUser)

        sSql = "select * from fct_getUserExe(" + `dmsID` + ") as sexe "
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
        print "Exe = ", liResult
        if liResult and liResult not in self.liSQL_ERRORS:
            file_exe = liResult[0]['sexe']

        return file_exe
         
    def xmlrpc_sl_search1(self, title,dicUser):
        print 'sl search 1',  dicUser, title 
       
        dicUser['client'] = int(dicUser['client'])
        sSql = "select id, title from dms where "
        
        if title:
            sSql += " title ~*'" + title + "' "
       # if firstname:
       #     if lastname:
      #          sSql + " and "
       #     sSql += " firstname ~*'" +firstname + "' "
       
            
        sSql += self.getWhere("", dicUser, 2) 
        
        sSql += " order by title LIMIT 20"
        
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        ret = {}
        if result and result not in self.liSQL_ERRORS:
            for row in result:
                ret[`row['id']`] = row['title']+ '\n' 
                #ret[`row['id']`] = "Hallo" + '\n'
                
        else:
            ret = {'0':'NONE'}
        print "ret from search1 = ",  ret
        return ret


    def xmlrpc_sl_searchBiblio1(self, title,author,dicUser):
        print 'sl search 1',  dicUser, title 
       
        dicUser['client'] = int(dicUser['client'])
        sSql = "select id, title from biblio where "
        
        if title:
            sSql += " title ~*'" + title + "' "
        if author:
            if title:
                sSql + " and "
            sSql += " author ~*'" +author + "' "
       
            
        sSql += self.getWhere("", dicUser, 2) 
        
        sSql += " order by title LIMIT 20"
        
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        ret = {}
        if result and result not in self.liSQL_ERRORS:
            for row in result:
                ret[`row['id']`] = row['title']+ '\n' 
                #ret[`row['id']`] = "Hallo" + '\n'
                
        else:
            ret = {'0':'NONE'}
        print "ret from search1 = ",  ret
        return ret


    def xmlrpc_getFileExePrg(self,sSuffix,dicUser={}):
        self.prefDMS = self.oUser.getDMSPrefs(dicUser)
        sSuffix =sSuffix.lower()
        exe = "libreoffice"
        sEXT = 'txt'
        print "sSuffix = ", sSuffix
        # insert her the db routines to check preferences

        if sSuffix:
            #print 'Format = ', singleDMS.fileFormat
            if sSuffix == self.prefDMS['fileformat']['scanImage']['format']:
                print 'show'
                exe = "SCAN"
            elif sSuffix == self.prefDMS['fileformat']['LINK']['format']:
                print 'Link'
                exe = "LINK"
            else:
                for key in  self.prefDMS['fileformat'].keys():
                    print 'file-format', sSuffix, key
                    print "format ", self.prefDMS['fileformat'][key]['format']
                    #print 'User-fileformat', dicUser['prefDMS']['fileformat'][key]['format']
                    #if sSuffix ==  self.prefDMS['fileformat'][key]['format'].lower():
                    if sSuffix ==  key.lower():
                        #print 'dicUser-prefDMS', dicUser['prefDMS']['fileformat'][key]
                        
                        exe =  self.prefDMS['fileformat'][key]['executable']
                        print '-------------------------------------------------------------------'
                        print 'exe = ', exe
                        print '-------------------------------------------------------------------'
                        #sys.exit(0)
                        
     
        print "return exe = ", exe
        return exe


    def viewDocument(self, singleDMS,dicUser, dicVars,Action=None, liEmailAddresses = None):
        #print 'dicVars1 ', dicVars
        #print 'Action = ', Action
        #print singleDMS.ID, singleDMS.fileFormat
        singleDMS.loadDocument()
        #print singleDMS.ID, singleDMS.fileFormat
        #print 'len Image = ', len(singleDMS.imageData)
        exe = None
        sEXT = 'txt'
        if singleDMS.fileFormat:
            #print 'Format = ', singleDMS.fileFormat
            if singleDMS.fileFormat == dicUser['prefDMS']['fileformat']['scanImage']['format']:
                print 'show'
                s = bz2.decompress( singleDMS.imageData)
                #sys.exit(0)
                newIm = Image.fromstring('RGB',[singleDMS.size_x, singleDMS.size_y], s)
                newIm.show()
            elif singleDMS.fileFormat == dicUser['prefDMS']['fileformat']['LINK']['format']:
                print 'Link'
                s = singleDMS.imageData
                #print 's = ', s
                
                os.system(dicUser['prefDMS']['exe']['internet'] + ' ' + `s` )
                
            else:
                for key in  dicUser['prefDMS']['fileformat'].keys():
                    #print 'file-format', singleDMS.fileFormat
                    #print 'User-fileformat', dicUser['prefDMS']['fileformat'][key]['format']
                    if singleDMS.fileFormat ==  dicUser['prefDMS']['fileformat'][key]['format']:
                        #print 'dicUser-prefDMS', dicUser['prefDMS']['fileformat'][key]
                        exe =  dicUser['prefDMS']['fileformat'][key]['executable']
                        #print '-------------------------------------------------------------------'
                        #print 'exe = ', exe
                        #print '-------------------------------------------------------------------'
                        #sys.exit(0)
                        
                        if singleDMS.fileSuffix and singleDMS.fileSuffix not in ['NONE','ERROR']:
                            sEXT = singleDMS.fileSuffix
                        else:   
                            sEXT =  dicUser['prefDMS']['fileformat'][key]['suffix'][0]
        else:
            exe = None
        
        print 'exe 1 = ', exe
        if exe or Action != None:
            singleDMS.createTmpFile(sEXT)
            if dicVars:
                #print ' '
                #print 'dicVars = ', dicVars
                try:
                    if zipfile.is_zipfile(singleDMS.tmpFile):
                        print 'zipfile found'
                        z1 = zipfile.ZipFile(singleDMS.tmpFile,'a')
                        print z1.namelist()
                        for f1 in ['content.xml', 'styles.xml']:
                            f_in = str(z1.read(f1))
                            #print 'content.xml', f_in
                            
                            f_in = self.replaceValues(dicVars,f_in, dicUser)
                            #print 'replaced Content', f_in
                            z1.writestr(f1,f_in)
                            
                        z1.close()

                    else:
                        
                        f_out = open(singleDMS.tmpFile + '_w1','a')
                                                    
                        f_in = open(singleDMS.tmpFile,'r')
                        if f_in and f_out:
                            s = f_in.readline()
                            while s:
                                s = self.replaceValues(dicVars,s, dicUser)                                 
                                
                                f_out.write(s)
                                s = f_in.readline()
                            f_in.close()
                            f_out.close()
                            
                            singleDMS.tmpFile = singleDMS.tmpFile + '_w1'
                        else:
                            'error read/create tmp-file'
                except Exception, param:
                    print Exception
                    print param
                    
        print 'exe2 = ', exe
        if Action == 'PrintNewsletter':
            sExe = dicUser['prefApps']['printNewsletter']
            print 'sExe', sExe, singleDMS.tmpFile 
            os.system(sExe + ' ' + singleDMS.tmpFile)
        elif Action == 'sentAutomaticEmail':
            print 'sentAutomaticEmail'
            print dicUser
            if dicUser.has_key('Email'):
                liAttachments = []
                filename = singleDMS.tmpFile
                print 'filename = ',  filename
                f = open(filename,'rb')
                if f:
                    s = f.read()
                    #print 's = ',  s
                    s = bz2.compress(s)
                    s = base64.encodestring(s)
                    dicAtt = {}
                    dicAtt['filename'] = filename[:filename.find('_w1')]
                    dicAtt['data'] = s
                    print 'len data = ', dicAtt['filename'] ,  len(dicAtt['data'])
                    liAttachments.append(dicAtt)
                f.close()
                
                for emailTo in liEmailAddresses:
                    dicV = {}
                    dicV['From'] = dicUser['Email']['From']
                    dicV['To'] = emailTo
                    dicV['Subject'] = dicVars['email_subject']
                    dicV['Body'] = dicVars['Body']
                    if dicVars.has_key('sm'):
                        dicV['sm'] = dicVars['sm']
                    print 'dicV = ', dicV.keys()
                    print dicUser['Email']
                    
                    em = self.rpc.callRP('Email.sendTheEmail', dicV, liAttachments, dicUser)
                    self.writeEmailLog(em)
                    
        else:
            print 'else execute ', exe 
            #os.system(exe + ' ' + singleDMS.tmpFile )
            self.startExternalPrg(exe,singleDMS.tmpFile)



    def importDocument(self, dicUser, sFile):

        dicData = {}
        
        if sFile:
            #print sFile
            f = open(sFile,'rb')
            #print f
            #f.seek(0)
            #b = f.readline()
            b = f.read()
            print 'len of b', len(b)
            #print b
            
            dicData["imageData"] =  base64.encodestring( bz2.compress(b))
            sSuffix =  string.lower(sFile[string.rfind(sFile,'.')+1:len(sFile)])
            for key in  self.prefDMS['fileformat'].keys():
                    #print 'file-format', sSuffix, key, self.prefDMS['fileformat'][key]['format']
                    #print 'User-fileformat', dicUser['prefDMS']['fileformat'][key]['format']
                    if sSuffix ==  self.prefDMS['fileformat'][key]['format'].lower():
                        dicData["suffix"] = sSuffix
                        dicData["format"] =  self.prefDMS['fileformat'][key]['format']
                        break
                    else:
                         dicData["suffix"] = "txt"
                         dicData["format"] = "Text"

                         
            #for key in  dicUser['prefDMS']['fileformat'].keys():
            #    for i in dicUser['prefDMS']['fileformat'][key]['suffix']:
            #        #print i
            #        #print suffix
            #        if i == suffix:
            #            print 'suffix found'
            #            singleDMS.fileFormat = singleDMS.fileFormat = dicUser['prefDMS']['fileformat'][key]['format']
            #            singleDMS.fileSuffix = suffix
            #            print 'singleDMS -f-format', `singleDMS.fileFormat`
            #            print 'singleDMS -f-suffix', `singleDMS.fileSuffix`
                       
            f.close()

            #print "this are the found data by importDocument ",dicData
        return dicData
        
    def load_mainwindow_logo(self,  allTables):        
        self.singleDMS = SingleDMS.SingleDMS(allTables)
        self.singleDMS.loadMainLogo()
        return  self.singleDMS.createTmpFile(self.singleDMS.firstRecord['file_suffix'])
        
    def load_article_catalogue_pic(self,  allTables,  picID):        
        self.singleDMS = SingleDMS.SingleDMS(allTables)
        self.singleDMS.loadDocumentForID(picID)
        return  self.singleDMS.createTmpFile(self.singleDMS.firstRecord['file_suffix'])
            
    def replaceValues(self, dicVars, s, dicUser):
        #print 'replace this in document: ',  dicVars
        for key in dicVars.keys():
            try:
                if self.checkType( dicVars[key],"unicode"): 
#                    if dicUser['Locales'] == 'de':
                    dicVars[key] = dicVars[key].encode('utf-8')
#                        #print 'de and unicode'
#                        #print dicVars[key]
                    
                if self.checkType( dicVars[key], 'string'):
                    dicVars[key]  = dicVars[key].replace('&','&amp;' )
                    dicVars[key] = dicVars[key].encode('utf-8')
                    
                print key, dicVars[key]
                #print '\n'
                
            except Exception, params:
                print Exception, params
            
                
            try:
                #print 'try to replace this ', key,  dicVars[key]
                if dicVars[key] == 'NONE' or dicVars[key] ==None:
                    s = s.replace('##'+ key + ';;','')
                elif self.checkType(dicVars[key], 'string') :
                    s = s.replace('##'+ key + ';;',dicVars[key] )
                
                else:
                    s = s.replace('##'+ key + ';;',`dicVars[key]` )
            except:
                pass
        return s

    
    def save2DMS(self,  fname, sType, Modul , id,  sSubject,  sFrom,  sTo,  sDate, dicUser ):
        #print'load = ',   fname
        dicValues = {}
        dicValues['paired_id'] = [0, "int"]
        dicData = self.importDocument(dicUser, fname)
        dicValues['file_format'] = [dicData["format"], 'string']
        dicValues['file_suffix'] = [dicData["suffix"], 'string']
        dicValues['document_image'] = [dicData["imageData"],'text']

        print "1", dicValues
        dicValues["insert_from_module"] = [self.MN[Modul], "int"]
        dicValues["sep_info_1"] =[ id, "int"]
        ID = -1
        #print "2"
        dicValues["insert_at_date"] =  [ datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') , "datetime"]
        dicValues["title"] = [sSubject, "string"]
        #print "3"
        dicValues["category"] = [_('email'), "string"]
        dicValues["document_rights_groups"] =  ['EMAIL', "string"]
        print "4"
        dicValues["sub1"] = [_('From: ') + sFrom, "string"]
        dicValues["sub2"] = [_('To: ') + sTo, "string"]
        #print "5"
        if sType == 'txt':
            dicValues["sub3"] = [_('Plain Text') , "string"]
        elif sType == 'eml':
            dicValues["sub3"] = [_('Standard-Email') , "string"]
        else:
            dicValues["sub3"] = [_('Attachment Type: ') + sType, "string"]
        dicValues["sub4"] = [_('Email Date: ') + sDate , "string"]
        #print "dicUser = ", dicUser
        newID =  self.oDatabase.xmlrpc_saveRecord( 'dms', ID, dicValues , dicUser)
        print "newID after save (DMS) ", newID
      

        return newID



  
