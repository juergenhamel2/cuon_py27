import time
from datetime import datetime
import random
import xmlrpclib
from twisted.web import xmlrpc
from basics import basics
import Database
import hashlib
import types
import Order
import base64


class Address(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        self.oDatabase = Database.Database()
        
    def xmlrpc_getContactComboBoxEntries(self, dicUser):
        liStatus = []
        liPriority = []
        
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        liStatus0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbStatus', cpServer)
        liPriority0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbPriority', cpServer)
        if liStatus0:
            liStatus = liStatus0.split(',')
        if liPriority0:
            liPriority = liPriority0.split(',')
            
        return self.liTime,  liStatus,  liPriority
        
        
    def xmlrpc_getComboBoxEntries(self, dicUser):
  
        print 'get comboBox Entries'
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        liTrade0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbTrade', cpServer)
        liTurnover0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbTurnover', cpServer)
        liLegalform0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbLegalform', cpServer)
        liFashion0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbFashion', cpServer)
        liSchedulTime0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbSchedulTime', cpServer)
        liTrade = ['NONE']
        liTurnover = ['NONE']
        liLegalform = ['NONE']
        liFashion = ['NONE']
        print self.liTime
        
        liSchedulTime = self.liTime
        
        if liTrade0:
            liTrade = liTrade0.split(',')
        if liTurnover0:
            liTurnover = liTurnover0.split(',')
        if liLegalform0:
            liLegalform = liLegalform0.split(',')
        if liFashion0:
            liFashion = liFashion0.split(',')
        if liSchedulTime0:
            print liSchedulTime0
            liSchedulTime.extend(liSchedulTime0.split(',') )
        print 'short before return = ',  liSchedulTime
        
        return liFashion, liTrade,liTurnover,liLegalform,  liSchedulTime
        
    def xmlrpc_getComboBoxEntriesFashion(self, dicUser):
   
        print 'get comboBox Entries'
        dicUser = self.checkIt(dicUser)
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        liFashion0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbFashion', cpServer)
        liFashion = ['NONE']
        if liFashion0:
            liFashion = liFashion0.split(',')

        return liFashion

    def xmlrpc_getComboBoxEntriesTrade(self, dicUser):
        print 'get comboBox Entries'
        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        liTrade0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbTrade', cpServer)
        liTrade = ['NONE']
        if liTrade0:
            liTrade = liTrade0.split(',')
        return liTrade

     
    def xmlrpc_getComboBoxEntriesTurnover(self, dicUser):
        print 'get comboBox Entries'
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])
        liTurnover0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbTurnover', cpServer)
        liTurnover = ['NONE']
        if liTurnover0:
            liTurnover = liTurnover0.split(',')

        return liTurnover

    def xmlrpc_getComboBoxEntriesLegalform(self, dicUser):
        print 'get comboBox Entries'
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])
        liLegalform0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbLegalform', cpServer)
        print  'CLIENT_' +  `dicUser['client']`,'cbLegalform'
        liLegalform = ['NONE']
        if liLegalform0:
            liLegalform = liLegalform0.split(',')

        return liLegalform


   
    def xmlrpc_getComboBoxEntriesSchedultime(self, dicUser):
        print 'get comboBox Entries'
        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        liSchedulTime0 = self.getConfigOption('CLIENT_' + `dicUser['client']`,'cbSchedulTime', cpServer)
        
        liSchedulTime = self.liTime
        if liSchedulTime0:
            liSchedulTime.extend(liSchedulTime0.split(',') )

        return liSchedulTime



    def xmlrpc_getAddressField(self,id, dicUser ):
        sAddress = ""
        liRow =  self.xmlrpc_getAddress(id, dicUser )
        print "liRow = ", liRow
        if liRow and liRow not in self.liSQL_ERRORS :
            dicRecord = liRow[0]
            try:
                sAddress += dicRecord['lastname'] + "\n"
                sAddress += dicRecord['lastname2'] + "\n"
                sAddress += dicRecord['firstname'] + "\n"
                sAddress += dicRecord['street'] + "\n"
                sAddress += dicRecord['country'] + '-' +dicRecord['zip']+ ' ' + dicRecord['city'] + "\n"
            except:
                pass
                    
        return sAddress 
    def getReportAddressfield(self,id,dicUser ):
        sSql = "select *  from fct_getAddressField(" +`id` + ") as ( address_id int, address text , firstname text , lastname text,lastname2 text ,street text , city text , city_country text , zip text , country text , city_alone text ,cityfield text, first_last text , last_first text)"
        return self.xmlrpc_executeNormalQuery(sSql, dicUser)
    
    def xmlrpc_getAddress(self,id, dicUser ):
    
        sSql = 'select address, lastname, lastname2,firstname, street, zip, city, ' 
        sSql = sSql + ' trim(country) || \'-\' || trim(zip) || \' \' || trim(city) as cityfield, state, country from address '
        sWhere = 'where id = ' + `id`
        sWhere = self.getWhere(sWhere, dicUser)
        sSql = sSql + sWhere
        
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)

##    def xmlrpc_getAllActiveSchedul(self, dicUser):
##        
##        sSql = "select to_char(partner_schedul.schedul_date, \'" + dicUser['SQLDateFormat'] + "\') as date, "
##        sSql = sSql + "to_char(partner_schedul.schedul_time, \'" + dicUser['SQLTimeFormat'] + "\') as time, "
##        sSql = sSql + "address.city, partner_schedul.short_remark, partner_schedul.notes , "
##        sSql = sSql + "partner.lastname as partner_lastname, address.lastname as address_lastname, "
##        sSql = sSql + "address.lastname2 as address_lastname2, partner.firstname as partner_firstname "
##        sSql = sSql + " from partner, address, partner_schedul "
##        sW = " where partner.id = partnerid and address.id = partner.addressid and "
##        sW = sW + " process_status != 999 "
##        sSql = sSql + self.getWhere(sW, dicUser)
##        
##        sSql = sSql + " order by partner_schedul.schedul_date, partner_schedul.schedul_time " 
##        
##        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
##

    
    def xmlrpc_getAllActiveSchedulByNames(self, dicUser, OrderType='Name', SelectStaff='All'):
        self.xmlrpc_getAllActiveSchedul(dicUser)
        

    def xmlrpc_sl_getSchedulesForPartner(self, dicUser,pID):
              
        sSql = "select partner_schedul.schedul_date as date, partner_schedul.dschedul_date as date_norm, "
        sSql += "partner_schedul.id as id,  "
        sSql +=  "partner_schedul.schedul_time_begin as time_begin,partner_schedul.schedul_time_end as time_end, "
        sSql += " address.zip as a_zip, "
        sSql +=  "address.city as a_city, partner_schedul.short_remark as s_remark, partner_schedul.notes as s_notes, "
        sSql +=  "partner.lastname as p_lastname, address.lastname as a_lastname "

        sSql += " from partner, address, partner_schedul "
        sSql += " where partner.id = " + `pID` + " and address.id = partner.addressid and "
        sSql += " process_status != 999 "

        liResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
        liSchedul = []
        if liResult not in self.liSQL_ERRORS:
        
            for row in liResult:
                dicRow = {}
                dicRow[`row['id']`]=row["date"] +", "+ self.liTime[row["time_begin"]] + ", " + self.liTime[row["time_end"]] + ", " + row["a_lastname"] + "," + row["a_city"]
                liSchedul.append(dicRow)
        if not liSchedul:
            liSchedul = [{"0":"NONE"}]
        return liSchedul


    def xmlrpc_sl_getAllActiveSchedul(self, dicUser, OrderType='Name', SelectStaff='All', sChoice = 'New'):
        liResult,hash = xmlrpc_getAllActiveSchedul(dicUser, OrderType, SelectStaff, sChoice )
        liSchedul = []
        if liResult not in self.liSQL_ERRORS:
        
            for row in liResult:
                dicRow = {}
                dicRow[`row[id]`]=row["date"] +", "+ row["time_begin"] + ", " +row["time_end"] + ", " + row["a_lastname"] + "," + row["a_city"]
                liSchedul.append(dicRow)
        if not liSchedul:
            liSchedul = [{"0":"NONE"}]
        return liSchedul

                         

                         

    def xmlrpc_getAllActiveSchedulMeta(self,liValues,dicUser):
        
        liDates,hash =  self.xmlrpc_getAllActiveSchedul( dicUser, OrderType=liValues[0], SelectStaff=liValues[1], sChoice =liValues[2], sHash = "NONE" )

        liReturn = []
        dicReturn = {}
        dicNames = {}
        dicDates = {}
        dicSalesMan = {}
        
        #print 'lidates = ',  liDates
        #print 'newHash = ',  newHash
        try:
            if liDates in ['NO_NEW_DATA',"ERROR"]:
                print 'liDates = no Data'
                if not dicNames:
                    dicNames["NONE"]=["NONE"]

                if not dicDates:
                    dicDates["NONE"]=["NONE"]
                if not dicSalesMan:
                    dicSalesMan["NONE"]=["NONE"]

                return [dicNames,dicDates,dicSalesMan] 



            # new data arrived, go on    
               #liststore = gtk.ListStore(str)


            print 'Schedul by names: ', liDates
            if liDates:
                lastRep = None
                lastSalesman = None
                Schedulname = None
                lastSchedulname = None




                for oneDate in liDates:
                    Schedulname = oneDate['schedul_name']
                    if lastSchedulname != Schedulname:
                        lastSchedulname = Schedulname
                        dicNames[Schedulname] = []



                    sTime  = self.getTimeString(oneDate['time_begin'] )
                    sTime2  = self.getTimeString(oneDate['time_end'] )

                    dicNames[Schedulname].append(oneDate['date'] +'--' + sTime + '-' +sTime2 +', ' + oneDate['a_lastname'] + ', ' + oneDate['a_city'] + ' ###' +  `oneDate['id']`)


    ##        try:
    ##            iter = treestore.append(None,['Names'])
    ##            iter2 = treestore.insert_after(iter,None,['jhamel'])
    ##            iter3 = treestore.insert_after(iter2,None,['termin1'])
    ##            iter = treestore.append(None,['Scheduls'])
    ##            iter2 = treestore.insert_after(iter,None,['date'])
    ##            iter3 = treestore.insert_after(iter2,None,['termin1'])
    ##        except Exception,params:
    ##            print Exception,params
    ##            

            #liDates,  self.schedulHash2 = self.rpc.callRP('Address.getAllActiveSchedul', oUser.getSqlDicUser(),'Schedul','All',sChoice)
            #liTest.sort(key=(lambda x: (x['test1'], lambda x: x['testA']) ))

            liDates.sort(key=(lambda x: (x['date_norm'],  x['schedul_name'], x['time_begin'] )),   reverse = True)
            #print 'Schedul by schedul_date 2 : ', liDates
            if liDates:
                lastRep = None
                lastSalesman = None
                Schedulname = None
                lastSchedulname = None


                for oneDate in liDates:
                    Schedulname = oneDate['date']
                    if lastSchedulname != Schedulname:
                        lastSchedulname = Schedulname
                        dicDates[Schedulname] = []
                    sTime  = self.getTimeString(oneDate['time_begin'] )
                    sTime2  = self.getTimeString(oneDate['time_end'] )

                    dicDates[Schedulname].append(oneDate['schedul_name'] +'--' + sTime + '-' +sTime2  +', ' + oneDate['a_lastname'] + ', ' + oneDate['a_city'] +' ###' +  `oneDate['id']`)


            # reps and Saleman

    #        #liDates,  self.schedulHash3 = self.rpc.callRP('Address.getAllActiveSchedul', oUser.getSqlDicUser(),'rep_salesman','All', sChoice)
            #liDates.sort(key=(lambda x: (x['date_norm'], lambda x: x['rep_lastname'],  lambda x: x['salesman_lastname'], lambda x: x['date_norm'])),   reverse = True)
            liDates.sort(key=(lambda x:  (x['salesman_lastname'], x['rep_lastname'], x['date_norm']) ),   reverse = True)
            #print 'Schedul by names: 3', liDates
            if liDates and liDates not in ['NONE']:
                lastRep = None
                lastSalesman = None
                Schedulname = None
                lastSchedulname = None


                for oneDate in liDates:
                    Schedulname = oneDate['schedul_name']
                    if lastSchedulname != Schedulname:
                        lastSchedulname = Schedulname
                        dicSalesMan[Schedulname] = []
                    sTime  = self.getTimeString(oneDate['time_begin'] )
                    sTime2  = self.getTimeString(oneDate['time_end'] )

                    dicSalesMan[Schedulname].append(oneDate['date'] +'--' + sTime + '-' +sTime2 +', ' + oneDate['a_lastname'] + ', ' + oneDate['a_city'] + ' ###' +  `oneDate['id']`)   
        except:
            pass
    
        if not dicNames:
            dicNames["NONE"]=["NONE"]

        if not dicDates:
            dicDates["NONE"]=["NONE"]
        if not dicSalesMan:
            dicSalesMan["NONE"]=["NONE"]
        
        return [dicNames,dicDates,dicSalesMan] 
                
    def xmlrpc_getAllActiveSchedul(self, dicUser, OrderType='Name', SelectStaff='All', sChoice = 'New', sHash = 'NONE'):
        value = None
        rep_salesman = None
        result = 'NONE'
        sw = None
        SQL = True
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/user.cfg')
            #print cpServer
            #print cpServer.sections()
            
            value = self.getConfigOption('SHOW_SCHEDUL',dicUser['Name'], cpServer)
                       
            #print cpServer
            #print cpServer.sections()
            
            rep_salesman = self.getConfigOption('SHOW_REP_SALESMAN',dicUser['Name'], cpServer)
            
        except Exception, params:
            print 'Error by Schedul Read user.cfg'
            print Exception, params
        print dicUser['Name']    
        print 'value = ', value
        print 'rep_salesman = ', rep_salesman
        
            
        if value and value == 'NO':
            pass
        
        elif value:
                
                
            sSql = "select partner_schedul.schedul_date as date, partner_schedul.dschedul_date as date_norm, "
            sSql += "partner_schedul.id as id,  "
            sSql +=  "partner_schedul.schedul_time_begin as time_begin,partner_schedul.schedul_time_end as time_end, "
            sSql += " address.zip as a_zip, "
            sSql +=  "address.city as a_city, partner_schedul.short_remark as s_remark, partner_schedul.notes as s_notes, "
            sSql +=  "partner.lastname as p_lastname, address.lastname as a_lastname, "
        
            #
            sSql += " case  schedul_staff_id "
            sSql += " when 0 then  'NONE' else ( select staff.lastname || ', ' || staff.firstname from staff where staff.id = schedul_staff_id) END as schedul_name,  "
            
    
            sSql += "( select staff.lastname from staff where staff.id = (select rep_id from address where address.id = partner.addressid)) as rep_lastname, " 
            sSql += "( select staff.lastname from staff where staff.id = (select salesman_id from address where address.id = partner.addressid)) as salesman_lastname, " 
            sSql +=  "address.lastname2 as a_lastname2, partner.firstname as p_firstname "
            # from 
            sSql += " from partner, address, partner_schedul "
            
            # where
            if SelectStaff == 'All':
                sW = " where partner.id = partner_schedul.partnerid and address.id = partner.addressid and "
                sW += " process_status != 999 "
                
            else:
                sW = " where partner.id = partner_schedul.partnerid and address.id = partner.addressid and "
                sW +=  " process_status != 999 "
                sW += " and schedul_staff_id = " + SelectStaff + " "
            
            #print 'sChoice = ', sChoice
            if sChoice == 'New':
                sW += " and ( ( date_part('doy', to_date(partner_schedul.schedul_date, '" + dicUser['SQLDateFormat'] +"'))  >=  date_part('doy', now())"
                sW += " and date_part('year', to_date(partner_schedul.schedul_date, '" + self.DIC_USER['SQLDateFormat'] +"'))  >=  date_part('year', now()) ) or (date_part('year', to_date(partner_schedul.schedul_date, '" + self.DIC_USER['SQLDateFormat'] +"'))  >  date_part('year', now()) ))"
            elif sChoice == 'actualWeek':
                sW += " and  date_part('week', to_date(partner_schedul.schedul_date, '" + dicUser['SQLDateFormat'] +"'))  =  date_part('week', now())"
                sW += " and date_part('year', to_date(partner_schedul.schedul_date, '" + self.DIC_USER['SQLDateFormat'] +"'))  >=  date_part('year', now())"
            elif sChoice == 'Cancel':
                sW += ' and partner_schedul.process_status between 801 and 998 '
            elif sChoice == 'All':
                pass
#            if OrderType == 'rep_salesman':
#                if rep_salesman == 'ALL':
#                    #print 'dicUser = ',  dicUser['Name']
#                    sW += " and address.rep_id = (select id from staff where cuon_username = '" + dicUser['Name'] + "') "
#                    sW += ' and address.salesman_id > 0'
                
            if value != 'ALL':
                liValue = value.split(',')
                sW += ' and ( '
                for sOptValue in liValue:
                    sW += ' schedul_staff_id = ' + sOptValue + ' or '
                
                sW = sW[:len(sW)-4]
                sW += ')'
            sW += " and char_length(partner_schedul.schedul_date) = 10 "
            sSql = sSql + self.getWhere(sW, dicUser,Prefix='partner_schedul.')
                
                
            if OrderType == 'Name' or OrderType == 'rep_salesman':
                sSql = sSql + " order by schedul_name DESC, to_date(partner_schedul.schedul_date, '" + dicUser['SQLDateFormat'] +"') DESC , partner_schedul.schedul_time_begin DESC" 
            elif OrderType == 'Schedul' :
                sSql = sSql + " order by to_date(partner_schedul.schedul_date , '" + dicUser['SQLDateFormat'] +"') DESC  , schedul_name DESC,  partner_schedul.schedul_time_begin DESC " 
            #print sSql    
            if OrderType == 'rep_salesman':
                if not rep_salesman or rep_salesman == 'NO':
                    SQL = False
            if SQL:
                result = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
                
        elif value == None:
            pass
        if OrderType == 'rep_salesman':
                if not rep_salesman or rep_salesman == 'NO':
                    result = 'NONE'
                    
        m = hashlib.md5()
        m.update(`result`)
        sNewHash =  m.hexdigest()
        print sHash
        print sNewHash
        if sNewHash == sHash:
            result = ['NO_NEW_DATA']
            
         
        return result,  sNewHash
        
        
    def xmlrpc_getAllActiveContacts(self, dicUser):
        #caller_id = self.getConfigOption('caller_id',dicUser['name'],self.getParser(self.CUON_FS +'/user.cfg'))
        #print caller_id
        sSql = "select contact.schedul_date as date, contact.id as id, "
        
        sSql +=  "contact.schedul_time_begin as time, "
        sSql += "alarm_days, alarm_hours, alarm_minutes, "
        sSql +=  "address.city, contact.partnerid as partner_id, contact.address_id as address_id, "
        sSql +=  " (select lastname as partner_lastname from partner where contact.partnerid = id), address.lastname as address_lastname, "
        sSql +=  "address.lastname2 as address_lastname2 "
        sSql +=  " from  address, contact "
        sW = " where address.id = contact.address_id and "
        sW +=  " process_status = 0 and contacter_id = " + self.getStaffID(dicUser) + " "  
        sW += " and char_length(contact.schedul_date) = 10 "
        sSql = sSql + self.getWhere(sW, dicUser, Prefix='contact.')
        
        sSql = sSql + " order by contact.schedul_date, contact.schedul_time_begin " 
        
        liResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
        liResult2 = []
        if liResult not in ['NONE','ERROR']:
            for dicResult in liResult:
                try:
                    #print 'Len1 = ', len(liResult)
                    Hour, Minute = self.getTime(dicResult['time'])
                    
                    sDate = dicResult['date'] + ' ' + `Hour` + ':' + `Minute` +':00'
                    #print 'Date =',  sDate
                    if sDate.find('.') > 0:
                        sFormat = '%d.%m.%Y %H:%M:%S'
                    else:
                        sFormat = '%Y/%m/%d %H:%M:%S'
                    wakeDate = time.strptime(sDate,sFormat)
                    #print wakeDate

                    nD1 = datetime(wakeDate[0], wakeDate[1],wakeDate[2],wakeDate[3],wakeDate[4], wakeDate[5])
                    #print 'nD1', nD1
                    currentDate = time.localtime()
                    #print currentDate
                    nD2 = datetime(currentDate[0], currentDate[1],currentDate[2],currentDate[3],currentDate[4], currentDate[5])
                    #print nD2
                    nD3 = nD1 -nD2
                    #print nD3
                    #print '---------------------------------------------------'
                    #print 'Name = ', dicResult['address_lastname']
                    #print 'Differenz Tage', nD3.days
                    #print 'Differenz Sekunden', nD3.seconds
                    #print 'DB-alarm','Differenz', 'Datum', 'Current'
                   
                    #print dicResult['alarm_hours'] * 3600 + dicResult['alarm_minutes'] * 60, nD3.seconds, nD1, nD2 

                    ok = False
                    
                    if nD3.days < 0:
                        ok = True
                    
                    elif nD3.days >= 0:
                    
                        warningDays = nD3.days - dicResult['alarm_days'] 
                        
                        if warningDays < 0 : 
                            ok = True
                        elif warningDays > 0 : 
                            ok = False
                        elif warningDays == 0 :
                            AlarmSeconds = dicResult['alarm_hours'] * 3600 + dicResult['alarm_minutes'] * 60
                            #print 'AlarmSeconds = ', AlarmSeconds
                            #print 'nD3-seconds', nD3.seconds
                            #print 'Differenz' , nD3.seconds - AlarmSeconds
                            if nD3.seconds - AlarmSeconds < 0 :
                               ok = True
                    if  ok:
                        #print 'copy entry to  list'
                        liResult2.append(dicResult)
                        #print 'Len2 = ', len(liResult2)
                        
                    
                except Exception, params:
                    print 'xmlrpc_getAllActiveContacts'
                    print Exception, params
        
        
        #print liResult2
        #print 'Len3 = ', len(liResult2)

        return liResult2
        
        
    def xmlrpc_getNotes(self, addressid, dicUser):
        dicReturn = 'NONE'
        sSql = 'select * from address_notes where address_id = ' +  `addressid`
        liResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
        if liResult not in ['NONE','ERROR']:
            dicReturn = liResult[0]
            #print dicReturn
            for key in dicReturn.keys():
                #print "key = ",  key
                #rint key[0:5]
                if key[0:6] == 'notes_':
                   #print 'found notes'
                    #or i in range(len(dicReturn[key])):
                    #  print ord(dicReturn[key][i])
                    dicReturn[key] = dicReturn[key].replace(chr(10), '</text:p><text:p text:style-name="Standard">')
                    #print dicReturn[key]
        return dicReturn
        
    def xmlrpc_getMisc(self, addressid, dicUser):
        dicReturn = 'NONE'
        sSql = 'select * from addresses_misc where address_id = ' +  `addressid`
        liResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
        if liResult not in ['NONE','ERROR']:
            dicReturn = liResult[0]
            liFashion, liTrade,liTurnover,liLegalform,  liSchedulTime = self.xmlrpc_getComboBoxEntries(dicUser)
            try:
                dicReturn['cb_fashion'] = liFashion[dicReturn['cb_fashion']]
                dicReturn['turnover'] = liTurnover[dicReturn['turnover']]
                dicReturn['trade'] = liTrade[dicReturn['trade']]
                dicReturn['legal_form'] = liLegalform[dicReturn['legal_form']]
            except:
                pass
                    
        return dicReturn

            
    def sentChangesPerMail(self, sModul, addressID, dicUser):
        if sModul == 'address_notes':
            
            value = None
            try:
                           
                cpServer, f = self.getParser(self.CUON_FS + '/user.cfg')
                #print cpServer
                #print cpServer.sections()
                
                value = self.getConfigOption('SENT_MAIL','representant', cpServer)
                
            except Exception, params:
                print 'Error by Schedul Read user.cfg'
                print Exception, params
            if value and addressID:
                dicRecord = self.xmlrpc_getNotes(addressID, dicUser)
                if dicRecord not in ['NONE','ERROR']:
                    pass
                
                
            
            
    def xmlrpc_getPartnerAddress(self, id, dicUser):
        sSql = 'select address, lastname, lastname2,firstname, street, zip, city, state, country, phone from partner where id = ' + `id`
        
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
     
    def xmlrpc_getPartnerToAddress(self, id, dicUser):
        sSql = 'select * from partner where addressid = ' + `id`
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)



    def xmlrpc_viewAddressList01(self,dicSearchlist,dicUser):
        sReturn = ""
        sSearch = getSqlArrayFromSearchlist(dicSearchlist)
        sSql = "select * from fct_createViewAddressList01(array [" + sSearch + "]"
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser )
        
        return sReturn
        
    def getPhonelist1(self, dicSearchlist, dicUser):
        
        
        return dicSearchlist
                
            
        # import string 
        
        # sSql = 'select lastname, lastname2, firstname, street, zip, city, phone, id from address'
        # sSql3 = ''
        # sSql4 = ''
        # sSql5 = ''
        # sSql6 = ''
        # sSql7 = ''
        # sSql8 = ''
        
        # if dicSearchlist:
        #     sSql2 = sSql + ' where '
        #     if dicSearchlist['eLastnameFrom'] and dicSearchlist['eLastnameTo'] :
        #        lastnameFrom =  string.upper(dicSearchlist['eLastnameFrom']) 
        #        lastnameTo = string.lower(dicSearchlist['eLastnameTo']) 
                
        #        sSql3 = sSql2 + " lastname  between  '" +  lastnameFrom + "' and '" + lastnameTo +"'"  
        #        sSql = sSql3
        
        #     if dicSearchlist['eFirstnameFrom'] and dicSearchlist['eFirstnameTo'] :
        #        firstnameFrom =  string.upper(dicSearchlist['eFirstnameFrom']) 
        #        firstnameTo = string.lower(dicSearchlist['eFirstnameTo']) 
                
               
        #        sSql4 = " firstname  between  '" +  firstnameFrom + "' and '" + firstnameTo +"'"  
        #        if sSql3:
        #            sSql4 = sSql3 + ' and ' + sSql4
        #        else:
        #            sSql4 = sSql2 + sSql4
        
        #        sSql = sSql4
        
        
        #     if dicSearchlist['eCityFrom'] and dicSearchlist['eCityTo'] :
        #        cityFrom =  string.upper(dicSearchlist['eCityFrom']) 
        #        cityTo = string.lower(dicSearchlist['eCityTo']) 
                
               
        #        sSql5 = " city  between  '" +  cityFrom + "' and '" + cityTo +"'"  
        #        if sSql3 and not sSql4:
        #            sSql5 = sSql3 + ' and ' + sSql5
        #        elif sSql3 and sSql4:
        #            sSql5 = sSql4 + ' and ' + sSql5
        #        else:
        #            sSql5 = sSql2 + sSql5
        
        #        sSql = sSql5
        
        
        #     if dicSearchlist['eCountryFrom'] and dicSearchlist['eCountryTo'] :
        #        countryFrom =  string.upper(dicSearchlist['eCountryFrom']) 
        #        countryTo = string.lower(dicSearchlist['eCountryTo']) 
                
           
                
        #        sSql6 = " country  between  '" +  countryFrom + "' and '" + countryTo +"'"  
        #        if sSql3 and not sSql4 and not sSql5:
        #            sSql6 = sSql3 + ' and ' + sSql6
        #        elif sSql4 and not sSql5:
        #            sSql6 = sSql4 + ' and ' + sSql6
        #        elif sSql5:
        #            sSql6 = sSql5 + ' and ' + sSql6
        #        else:
        #            sSql6 = sSql2 + sSql6
        
        #        sSql = sSql6
        #     if dicSearchlist['eInfoContains'] :
        #         sSql7 = " status_info ~*'" + dicSearchlist['eInfoContains'] +"' " 
        #         if sSql3 or sSql4 or sSql5 or sSql6:
        #             sSql = sSql + " and " + sSql7
        #         else:
        #             sSql = sSql2 + sSql7
                    
        #     if dicSearchlist['eNewsletterContains'] :
        #         sSql8 = " newsletter ~*'" + dicSearchlist['eNewsletterContains'] +"' " 
        #         if sSql3 or sSql4 or sSql5 or sSql6 or sSql7:
        #             sSql = sSql + " and " + sSql8
        #         else:
        #             sSql = sSql2 + sSql8
                    
        # if dicSearchlist:
        #     sSql = sSql + self.getWhere("",dicUser,2)
        # else:
        #     sSql = sSql + self.getWhere("",dicUser,1)
            
        
        
        # sSql = sSql + ' order by lastname, lastname2, firstname, city'
        
        # return self.oDatabase.xmclrpc_executeNormalQuery(sSql, dicUser)

    
    def getPhonelist11(self, dicSearchlist, dicUser):
        import string 
        
        sSql = 'select lastname, lastname2, firstname, street, zip, city, phone, id from address'
        sSql3 = ''
        sSql4 = ''
        sSql5 = ''
        sSql6 = ''
        sSql7 = ''
        sSql8 = ''
        
        if dicSearchlist:
            sSql2 = sSql + ' where '
            if dicSearchlist['eLastnameFrom'] and dicSearchlist['eLastnameTo'] :
               lastnameFrom =  string.upper(dicSearchlist['eLastnameFrom']) 
               lastnameTo = string.lower(dicSearchlist['eLastnameTo']) 
                
               sSql3 = sSql2 + " lastname  between  '" +  lastnameFrom + "' and '" + lastnameTo +"'"  
               sSql = sSql3
        
            if dicSearchlist['eFirstnameFrom'] and dicSearchlist['eFirstnameTo'] :
               firstnameFrom =  string.upper(dicSearchlist['eFirstnameFrom']) 
               firstnameTo = string.lower(dicSearchlist['eFirstnameTo']) 
                
               
               sSql4 = " firstname  between  '" +  firstnameFrom + "' and '" + firstnameTo +"'"  
               if sSql3:
                   sSql4 = sSql3 + ' and ' + sSql4
               else:
                   sSql4 = sSql2 + sSql4
        
               sSql = sSql4
        
        
            if dicSearchlist['eCityFrom'] and dicSearchlist['eCityTo'] :
               cityFrom =  string.upper(dicSearchlist['eCityFrom']) 
               cityTo = string.lower(dicSearchlist['eCityTo']) 
                
               
               sSql5 = " city  between  '" +  cityFrom + "' and '" + cityTo +"'"  
               if sSql3 and not sSql4:
                   sSql5 = sSql3 + ' and ' + sSql5
               elif sSql3 and sSql4:
                   sSql5 = sSql4 + ' and ' + sSql5
               else:
                   sSql5 = sSql2 + sSql5
        
               sSql = sSql5
        
        
            if dicSearchlist['eCountryFrom'] and dicSearchlist['eCountryTo'] :
               countryFrom =  string.upper(dicSearchlist['eCountryFrom']) 
               countryTo = string.lower(dicSearchlist['eCountryTo']) 
                
           
                
               sSql6 = " country  between  '" +  countryFrom + "' and '" + countryTo +"'"  
               if sSql3 and not sSql4 and not sSql5:
                   sSql6 = sSql3 + ' and ' + sSql6
               elif sSql4 and not sSql5:
                   sSql6 = sSql4 + ' and ' + sSql6
               elif sSql5:
                   sSql6 = sSql5 + ' and ' + sSql6
               else:
                   sSql6 = sSql2 + sSql6
        
               sSql = sSql6
            if dicSearchlist['eInfoContains'] :
                sSql7 = " status_info ~*'" + dicSearchlist['eInfoContains'] +"' " 
                if sSql3 or sSql4 or sSql5 or sSql6:
                    sSql = sSql + " and " + sSql7
                else:
                    sSql = sSql2 + sSql7
                    
            if dicSearchlist['eNewsletterContains'] :
                sSql8 = " newsletter ~*'" + dicSearchlist['eNewsletterContains'] +"' " 
                if sSql3 or sSql4 or sSql5 or sSql6 or sSql7:
                    sSql = sSql + " and " + sSql8
                else:
                    sSql = sSql2 + sSql8

                    
        if dicSearchlist:
            sSql = sSql + self.getWhere("",dicUser,2)
        else:
            sSql = sSql + self.getWhere("",dicUser,1)
            
        
        
        sSql = sSql + ' order by lastname, lastname2, firstname, city'
        
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
    



    def getPhonelist12(self, dicSearchlist, dicUser):
        import string 
        sSql = 'select address.id as address_id, address.lastname as address_lastname, address.lastname2 as ddress_lastname2, address.firstname as address_firstname, address.street as address_street, address.zip as ddress_zip, address.city as address_city, address.phone as address_phone, '
        sSql = sSql + ' partner.lastname as partner_lastname, partner.lastname2 as partner_lastname2, partner.firstname as partner_firstname, partner.street as partner_street, partner.zip as partner_zip, partner.city as partner_city, partner.phone as partner_phone '
        sSql = sSql + ' from address, partner '
        sSqlWhere = 'where partner.addressid = address.id  '
        sSql3 = ''
        sSql4 = ''
        sSql5 = ''
        sSql6 = ''
        sSql7 = ''
        sSql8 = ''
        
        if dicSearchlist:
            sSql2 = sSql + sSqlWhere + ' and '
            if dicSearchlist['eLastnameFrom'] and dicSearchlist['eLastnameTo'] :
               lastnameFrom =  string.upper(dicSearchlist['eLastnameFrom']) 
               lastnameTo = string.lower(dicSearchlist['eLastnameTo']) 
                
               sSql3 = sSql2 + " address.lastname  between  '" +  lastnameFrom + "' and '" + lastnameTo +"'"  
               sSql = sSql3
        
            if dicSearchlist['eFirstnameFrom'] and dicSearchlist['eFirstnameTo'] :
               firstnameFrom =  string.upper(dicSearchlist['eFirstnameFrom']) 
               firstnameTo = string.lower(dicSearchlist['eFirstnameTo']) 
                
               
               sSql4 = " address.firstname  between  '" +  firstnameFrom + "' and '" + firstnameTo +"'"  
               if sSql3:
                   sSql4 = sSql3 + ' and ' + sSql4
               else:
                   sSql4 = sSql2 + sSql4
        
               sSql = sSql4
        
        
            if dicSearchlist['eCityFrom'] and dicSearchlist['eCityTo'] :
               cityFrom =  string.upper(dicSearchlist['eCityFrom']) 
               cityTo = string.lower(dicSearchlist['eCityTo']) 
                
               
               sSql5 = " address.city  between  '" +  cityFrom + "' and '" + cityTo +"'"  
               if sSql3 and not sSql4:
                   sSql5 = sSql3 + ' and ' + sSql5
               elif sSql3 and sSql4:
                   sSql5 = sSql4 + ' and ' + sSql5
               else:
                   sSql5 = sSql2 + sSql5
        
               sSql = sSql5
        
        
            if dicSearchlist['eCountryFrom'] and dicSearchlist['eCountryTo'] :
               countryFrom =  string.upper(dicSearchlist['eCountryFrom']) 
               countryTo = string.lower(dicSearchlist['eCountryTo']) 
                
           
                
               sSql6 = " address.country  between  '" +  countryFrom + "' and '" + countryTo +"'"  
               if sSql3 and not sSql4 and not sSql5:
                   sSql6 = sSql3 + ' and ' + sSql6
               elif sSql4 and not sSql5:
                   sSql6 = sSql4 + ' and ' + sSql6
               elif sSql5:
                   sSql6 = sSql5 + ' and ' + sSql6
               else:
                   sSql6 = sSql2 + sSql6
        
               sSql = sSql6
            if dicSearchlist['eInfoContains'] :
                sSql7 = " address.status_info ~*'" + dicSearchlist['eInfoContains'] +"' " 
                if sSql3 or sSql4 or sSql5 or sSql6:
                    sSql = sSql + " and " + sSql7
                else:
                    sSql = sSql2 + sSql7
                    
            if dicSearchlist['eNewsletterContains'] :
                sSql8 = " address.newsletter ~*'" + dicSearchlist['eNewsletterContains'] +"' " 
                if sSql3 or sSql4 or sSql5 or sSql6 or sSql7:
                    sSql = sSql + " and " + sSql8
                else:
                    sSql = sSql2 + sSql8
                    
        if dicSearchlist:
            sSql = sSql + self.getWhere("",dicUser,2, 'address.')
        else:
            sSql = sSql + self.getWhere("",dicUser,1, 'address.')
            
        
        
        sSql = sSql + ' order by address.lastname, address.lastname2, address.firstname, address.city,partner.lastname, partner.lastname2, partner.firstname, partner.city '
        
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
  


    def getBarcodeList(self, dicSearchlist, dicUser):
        import string 
        
        sSql = 'select lastname, lastname2, firstname, street, zip, city, phone, id from address'
        sSql3 = ''
        sSql4 = ''
        sSql5 = ''
        sSql6 = ''
        sSql7 = ''
        sSql8 = ''
        
        if dicSearchlist:
            sSql2 = sSql + ' where '
            if dicSearchlist['eLastnameFrom'] and dicSearchlist['eLastnameTo'] :
               lastnameFrom =  string.upper(dicSearchlist['eLastnameFrom']) 
               lastnameTo = string.lower(dicSearchlist['eLastnameTo']) 
                
               sSql3 = sSql2 + " lastname  between  '" +  lastnameFrom + "' and '" + lastnameTo +"'"  
               sSql = sSql3
        
            if dicSearchlist['eFirstnameFrom'] and dicSearchlist['eFirstnameTo'] :
               firstnameFrom =  string.upper(dicSearchlist['eFirstnameFrom']) 
               firstnameTo = string.lower(dicSearchlist['eFirstnameTo']) 
                
               
               sSql4 = " firstname  between  '" +  firstnameFrom + "' and '" + firstnameTo +"'"  
               if sSql3:
                   sSql4 = sSql3 + ' and ' + sSql4
               else:
                   sSql4 = sSql2 + sSql4
        
               sSql = sSql4
        
        
            if dicSearchlist['eCityFrom'] and dicSearchlist['eCityTo'] :
               cityFrom =  string.upper(dicSearchlist['eCityFrom']) 
               cityTo = string.lower(dicSearchlist['eCityTo']) 
                
               
               sSql5 = " city  between  '" +  cityFrom + "' and '" + cityTo +"'"  
               if sSql3 and not sSql4:
                   sSql5 = sSql3 + ' and ' + sSql5
               elif sSql3 and sSql4:
                   sSql5 = sSql4 + ' and ' + sSql5
               else:
                   sSql5 = sSql2 + sSql5
        
               sSql = sSql5
        
        
            if dicSearchlist['eCountryFrom'] and dicSearchlist['eCountryTo'] :
               countryFrom =  string.upper(dicSearchlist['eCountryFrom']) 
               countryTo = string.lower(dicSearchlist['eCountryTo']) 
                
           
                
               sSql6 = " country  between  '" +  countryFrom + "' and '" + countryTo +"'"  
               if sSql3 and not sSql4 and not sSql5:
                   sSql6 = sSql3 + ' and ' + sSql6
               elif sSql4 and not sSql5:
                   sSql6 = sSql4 + ' and ' + sSql6
               elif sSql5:
                   sSql6 = sSql5 + ' and ' + sSql6
               else:
                   sSql6 = sSql2 + sSql6
        
               sSql = sSql6
            if dicSearchlist['eInfoContains'] :
                sSql7 = " status_info ~*'" + dicSearchlist['eInfoContains'] +"' " 
                if sSql3 or sSql4 or sSql5 or sSql6:
                    sSql = sSql + " and " + sSql7
                else:
                    sSql = sSql2 + sSql7
                    
            if dicSearchlist['eNewsletterContains'] :
                sSql8 = " newsletter ~*'" + dicSearchlist['eNewsletterContains'] +"' " 
                if sSql3 or sSql4 or sSql5 or sSql6 or sSql7:
                    sSql = sSql + " and " + sSql8
                else:
                    sSql = sSql2 + sSql8
                    
        if dicSearchlist:
            sSql = sSql + self.getWhere("",dicUser,2)
        else:
            sSql = sSql + self.getWhere("",dicUser,1)
            
        
        
        sSql = sSql + ' order by zip'
        
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
    

    def xmlrpc_sendEmail2Address(self, dicEmail, liAttach, dicUser):
        print 'read Email-config'
        
    def xmlrpc_getNewsletterAddresses(self, NewsletterShortcut, dicUser):
        erp_print_newsletter_misc_higher_then = 0
        erp_email_newsletter_misc_higher_then = 0
        dicUser = self.checkIt(dicUser)
        
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()

            erp_print_newsletter_misc_higher_then = int(self.getConfigOption("CLIENT_" +  `dicUser["client"]`,"erp_print_newsletter_misc_higher_then", cpServer))
            erp_email_newsletter_misc_higher_then = int(self.getConfigOption("CLIENT_" +  `dicUser["client"]`,"erp_email_newsletter_misc_higher_then",cpServer))
        except Exception,params:
            print Exception,params
            erp_print_newsletter_misc_higher_then = 0
            erp_email_newsletter_misc_higher_then = 0
        if not   erp_print_newsletter_misc_higher_then:
            erp_print_newsletter_misc_higher_then = 0
            erp_email_newsletter_misc_higher_then = 0

            
        print NewsletterShortcut
        
        print "erp values = ", erp_print_newsletter_misc_higher_then, erp_email_newsletter_misc_higher_then
        result = []
        sSql = "select address.id, address,lastname,lastname2,firstname,street,country,zip,city,letter_address  from address, cuon_erp1 where newsletter ~'.*" + NewsletterShortcut +".*' "

        sSql += " and erp_newsletter >= " + `erp_print_newsletter_misc_higher_then` + "  and cuon_erp1.address_id = address.id "
        
        sSql += self.getWhere("",dicUser,2,"address.")
        sSql += " order by zip "
        #print sSql
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        if result in [ 'NONE','ERROR']:
            result = []
        #print 'result 1 ', result
        sSql = "select partner.id as partner_id, partner.address as address,partner.lastname as lastname, partner.lastname2 as lastname2, partner.firstname as firstname, partner.street as street, partner.country as country, partner.zip as zip , partner.city as city, partner.letter_address as letter_address, "
        sSql += " address.id as address_id, address.address as address_address, address.lastname as address_lastname, address.lastname2 as address_lastname2, address.firstname as address_firstname, address.zip as address_zip, address.city as address_city,address.street as address_street,address.country as address_country,address.letter_address as address_letter_address"
       
        sSql += " from partner, address,cuon_erp1 where addressid = address.id and " 
        sSql += " partner.newsletter ~'.*" + NewsletterShortcut +".*' "

        sSql += " and erp_newsletter >= " + `erp_print_newsletter_misc_higher_then` + "  and cuon_erp1.address_id = address.id "
            
        sSql += self.getWhere("",dicUser,2,'partner.')
        sSql += " order by address.zip"
        #print sSql
        result2 = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        #print 'result2 = ', result2
        if result2 not in ['NONE','ERROR']:
            for res in result2:
                result.append(res)
        #print 'result3', result
        result4 = []
        for record in result:
            result4.append(self.addDateTime(record))
        
        
        oOrder = Order.Order()

        

       
        result5 = []
        for record in result4:
            if record.has_key("id"):
                dicIncomming =  oOrder.xmlrpc_getLastYearIncomming(record["id"],dicUser)
                record["total_sum"] =  dicIncomming["total_sum"]
                record["incomming_sum"] =  dicIncomming["incomming_sum"]
                record["total_bonus"] =  dicIncomming["total_bonus"]
                record["incomming_bonus"] =  dicIncomming["incomming_bonus"]
                record["total_sum_string"] =  dicIncomming["total_sum_string"]
                record["incomming_sum_string"] =  dicIncomming["incomming_sum_string"]
                record["total_bonus_string"] =  dicIncomming["total_bonus_string"]
                record["incomming_bonus_string"] =  dicIncomming["incomming_bonus_string"]




            result5.append(record)
            
#        print result5
        return result5
        
        
    def xmlrpc_getStatCaller(self, dicUser):
        result = {}
        CALLER_ID = None
        WITHOUT_ID = None
        MIN_SCHEDUL_YEAR = '2005'
        SCHEDUL_PROCESS_STATUS = None
        liCaller = None
        liSchedulProcessStatus = None
        iCentury = 2
        iDecade = 5
        iYear = 3
        iQuarter = 6
        iMonth = 14
        iWeek = 5
        
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/user.cfg')
            #print cpServer
            #print cpServer.sections()
            
            CALLER_ID = self.getConfigOption('STATS','CALLER_ID', cpServer)
            WITHOUT_ID = self.getConfigOption('STATS','WITHOUT_ID', cpServer)
        
        except:
            pass
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            SCHEDUL_PROCESS_STATUS = self.getConfigOption('CLIENT_' + `dicUser['client']`,'SchedulProcessStatus', cpServer)
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerCentury', cpServer)
            if iValue:
                iCentury = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerDecade', cpServer)
            if iValue:
                iDecade = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerYear', cpServer)
            if iValue:
                iYear = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerQuarter', cpServer)
            if iValue:
                iQuarter = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerMonth', cpServer)
            if iValue:
                iMonth = int(iValue)
                
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerWeek', cpServer)
            if iValue:
                iWeek = int(iValue)
                
                    
                
        except:
            pass    
        print "SCHEDUL_PROCESS_STATUS",   SCHEDUL_PROCESS_STATUS
        
        if SCHEDUL_PROCESS_STATUS:
            liSPS = SCHEDUL_PROCESS_STATUS.split(',')
            print "liSPS",  liSPS
            liSchedulProcessStatus = []
            for st in liSPS:
                print st
                liSchedulProcessStatus.append(int(st.strip()))
            
       
            
        if CALLER_ID:
            liCaller = CALLER_ID.split(',')
            liSql = []
            liSql.append({'id':'day','sql':'doy','logic':'='})
            liSql.append({'id':'week','sql':'week','logic':'='})
            liSql.append({'id':'month','sql':'month','logic':'='})
            liSql.append({'id':'quarter','sql':'quarter','logic':'='})
            liSql.append({'id':'year','sql':'year','logic':'='})
            liSql.append({'id':'decade','sql':'decade','logic':'='})
            liSql.append({'id':'century','sql':'century','logic':'='})
        
            for caller in liCaller:
                caller_name = None
                sSql = 'select cuon_username from staff where staff.id = ' + caller 
                res1 = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                print 'dicUser' , dicUser
                if res1 and res1 not in ['NONE','ERROR']:
                    caller_name = res1[0]['cuon_username']
                if caller_name:    
                    sSql = "select '" + caller_name + "' as caller_name_" + caller + " ,"
                    for vSql in liSql:
                        for z1 in xrange(0,30):
                            if vSql['id'] == 'decade' and z1 > iDecade:
                                pass
                            elif vSql['id'] == 'century' and z1 > iCentury:
                                pass 
                            elif vSql['id'] == 'year' and z1 > iYear:
                                pass 
                            elif vSql['id'] == 'quarter' and z1 > iQuarter:
                                pass 
                            elif vSql['id'] == 'month' and z1 > iMonth:
                                pass     
                            elif vSql['id'] == 'week' and z1 > iWeek:
                                pass     
                            elif vSql['id'] == 'day' and z1 > 10:
                                pass   
                            else:
                                sSql += "(select  count(ps.id)  from partner_schedul as ps, address as a, partner as p where a.id = p.addressid and ps.user_id = '" + caller_name + "' and ps.partnerid = p.id and  ps.process_status != 999 "
                                #sSql +=  " and  date_part('" + vSql['sql'] +"', ps.insert_time) " + vSql['logic']+"  date_part('" + vSql['sql'] + "', now()) - " + `z1`
                                sSql +=  " and  date_part('" + vSql['sql'] +"', ps.insert_time) " + vSql['logic']+" " + self.getNow(vSql, z1)[0]
                                                                
                                sSql += " and date_part('year', ps.insert_time) =  " + `self.getBeforeYears(vSql['id'],z1)`
                                if WITHOUT_ID:
                                    liWithoutId = WITHOUT_ID.split(',')
                                    for no_id in liWithoutId:
                                        sSql += ' and a.id != ' + no_id
                                sSql += " and date_part('year',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"')) >= " + MIN_SCHEDUL_YEAR
                                sSql += self.getWhere('',dicUser,2,'ps.')
                                sSql += ") as " + 'caller_' + caller +'_'+ vSql['id'] + '_count_' + `z1` + " , "
                                
                                #sSql += " group by a.caller_id "
                                if liSchedulProcessStatus:
                                    for sps in liSchedulProcessStatus:
                                        sSql += "(select  count(process_status)  from partner_schedul as ps, address as a, partner as p where a.id = p.addressid and ps.user_id = '" + caller_name + "' and ps.partnerid = p.id and  ps.process_status != 999 "
                                        sSql +=  " and  date_part('" + vSql['sql'] +"', ps.insert_time) " + vSql['logic'] + " " + self.getNow(vSql,  z1)[0]
                                        sSql += " and date_part('year', ps.insert_time) = "  + `self.getBeforeYears(vSql['id'],z1)`
                                        if WITHOUT_ID:
                                            liWithoutId = WITHOUT_ID.split(',')
                                            for no_id in liWithoutId:
                                                sSql += ' and a.id != ' + no_id
                                        sSql += " and date_part('year',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"')) >= " + MIN_SCHEDUL_YEAR
                                        sSql += "and process_status = " + `sps` + " " 
                                        sSql += self.getWhere('',dicUser,2,'ps.')
                                        sSql += ") as " + 'caller_' + caller +'_'+ vSql['id'] + '_count_' + `z1` + "_status_" + `sps` +", "   

                    sSql = sSql[0:len(sSql)-2]
                    self.writeLog(sSql)
                    tmpResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                    if tmpResult and tmpResult not in ['NONE','ERROR']:
                        oneResult = tmpResult[0]
                        for key in oneResult.keys():
                            if oneResult[key]:
                                result[key] = oneResult[key]
                            else:
                                result[key] =0
                            #result[key] = oneResult[key]
                                      
                 

##                                tmpResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
##                                if tmpResult and tmpResult not in ['NONE','ERROR']:
##                                    oneResult = tmpResult[0]
##                                    for key in oneResult.keys():
##                                      result['caller_' + caller +'_'+ vSql['id'] + '_' + key + '_' + `z1` ] = oneResult[key]
##                                      
                    
                
                
            
        if not result:
            result = 'NONE'


        #self.writeLog('Caller-Result = ' + `result`)
        
        return result
        
    def xmlrpc_getStatRep(self, dicUser):
        result = {}
        REP_ID = None
        WITHOUT_ID = None
        MIN_SCHEDUL_YEAR = '2003'
        iCentury = 2
        iDecade = 5
        iYear = 3
        iQuarter = 6
        iMonth = 14
        iWeek = 5
        
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/user.cfg')
            #print cpServer
            #print cpServer.sections()
            
            REP_ID = self.getConfigOption('STATS','REP_ID', cpServer)
            WITHOUT_ID = self.getConfigOption('STATS','WITHOUT_ID', cpServer)
        
        except:
            pass
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            SCHEDUL_PROCESS_STATUS = self.getConfigOption('CLIENT_' + `dicUser['client']`,'SchedulProcessStatus', cpServer)
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerCentury', cpServer)
            if iValue:
                iCentury = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerDecade', cpServer)
            if iValue:
                iDecade = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerYear', cpServer)
            if iValue:
                iYear = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerQuarter', cpServer)
            if iValue:
                iQuarter = int(iValue)
            
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerMonth', cpServer)
            if iValue:
                iMonth = int(iValue)
                
            iValue = self.getConfigOption('CLIENT_' + `dicUser['client']`,'StatsCallerWeek', cpServer)
            if iValue:
                iWeek = int(iValue)
                
                    
                
        except:
            pass    
        print "SCHEDUL_PROCESS_STATUS",   SCHEDUL_PROCESS_STATUS
        
        if SCHEDUL_PROCESS_STATUS:
            liSPS = SCHEDUL_PROCESS_STATUS.split(',')
            print "liSPS",  liSPS
            liSchedulProcessStatus = []
            for st in liSPS:
                print st
                liSchedulProcessStatus.append(int(st.strip()))
            

        if REP_ID:
            lirep = REP_ID.split(',')
            liSql = []
            liSql.append({'id':'day','sql':'doy','logic':'='})
            liSql.append({'id':'week','sql':'week','logic':'='})
            liSql.append({'id':'month','sql':'month','logic':'='})
            liSql.append({'id':'quarter','sql':'quarter','logic':'='})
            liSql.append({'id':'year','sql':'year','logic':'='})
            liSql.append({'id':'decade','sql':'decade','logic':'='})
            liSql.append({'id':'century','sql':'century','logic':'='})

            for rep in lirep:
                rep_name = None
                sSql = 'select cuon_username from staff where staff.id = ' + rep 
                res1 = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                if res1 and res1 not in ['NONE','ERROR']:
                    rep_name = res1[0]['cuon_username']
                if rep_name:    
                    sSql = "select '" + rep_name + "' as rep_name_" + rep + " ,"
                    for vSql in liSql:
                        for z1 in xrange(-5,20):
                            if vSql['id'] == 'decade' and z1 > iDecade:
                                pass
                            elif vSql['id'] == 'century' and z1 > iCentury:
                                pass 
                            elif vSql['id'] == 'year' and z1 > iYear:
                                pass 
                            elif vSql['id'] == 'quarter' and z1 > iQuarter:
                                pass 
                            elif vSql['id'] == 'month' and z1 > iMonth:
                                pass     
                            elif vSql['id'] == 'week' and z1 > iWeek:
                                pass     
                            
                            else:
                                sSql += "(select  count(ps.id) from partner_schedul as ps, address as a, partner as p where a.id = p.addressid and ps.schedul_staff_id = '" + rep + "' and ps.partnerid = p.id and  ps.process_status != 999 "
                                sSql +=  " and  date_part('" + vSql['sql'] +"',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"') ) " + vSql['logic'] +" " + self.getNow(vSql, z1)[0]
                                sSql += " and date_part('year', to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"') ) = "  + `self.getBeforeYears(vSql['id'],z1)`
                                if WITHOUT_ID:
                                    liWithoutId = WITHOUT_ID.split(',')
                                    for no_id in liWithoutId:
                                        sSql += ' and a.id != ' + no_id
                                sSql += " and date_part('year',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"')) >= " + MIN_SCHEDUL_YEAR
                                sSql += self.getWhere('',dicUser,2,'ps.')
                                sSql += ") as " + 'rep_' + rep +'_'+ vSql['id'] + '_count_' + `z1`.replace('-','M') + " , "
                                if liSchedulProcessStatus:
                                    for sps in liSchedulProcessStatus:
                                        sSql += "(select  count(ps.id) from partner_schedul as ps, address as a, partner as p where a.id = p.addressid and ps.schedul_staff_id = '" + rep + "' and ps.partnerid = p.id and  ps.process_status != 999 "
                                        sSql +=  " and  date_part('" + vSql['sql'] +"',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"') ) " + vSql['logic'] +" " + self.getNow(vSql, z1)[0]
                                        sSql += " and date_part('year', to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"') ) = "  + `self.getBeforeYears(vSql['id'],z1)`
                                         
                                        if WITHOUT_ID:
                                            liWithoutId = WITHOUT_ID.split(',')
                                            for no_id in liWithoutId:
                                                sSql += ' and a.id != ' + no_id
                                        sSql += " and date_part('year',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"')) >= " + MIN_SCHEDUL_YEAR
                                        sSql += "and process_status = " + `sps` + " " 
                                        sSql += self.getWhere('',dicUser,2,'ps.')
                                        sSql += ") as " + 'rep_' + rep +'_'+ vSql['id'] + '_count_' + `z1`.replace('-','M') + "_status_" + `sps` + " , "       
                                                
                                        
                                        
                                #sSql += " group by a.rep_id "


                    sSql = sSql[0:len(sSql)-2]
                    self.writeLog(sSql)
                    tmpResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                    if tmpResult and tmpResult not in ['NONE','ERROR']:
                        oneResult = tmpResult[0]
                        for key in oneResult.keys():
                            if oneResult[key]:
                                result[key] = oneResult[key]
                            else:
                                result[key] =0
                                
                                      
                 

##                                tmpResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
##                                if tmpResult and tmpResult not in ['NONE','ERROR']:
##                                    oneResult = tmpResult[0]
##                                    for key in oneResult.keys():
##                                      result['rep_' + rep +'_'+ vSql['id'] + '_' + key + '_' + `z1` ] = oneResult[key]
##                                      
                    
                
                
            
        if not result:
            result = 'NONE'


        self.writeLog('rep-Result = ' + `result`)
        
        return result

    def xmlrpc_getStatSalesman(self, dicUser):
        result = {}
        SALESMAN_ID = None
        WITHOUT_ID = None
        MIN_SCHEDUL_YEAR = '2005'
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/user.cfg')
            #print cpServer
            #print cpServer.sections()
            
            SALESMAN_ID = self.getConfigOption('STATS','SALESMAN_ID', cpServer)
            WITHOUT_ID = self.getConfigOption('STATS','WITHOUT_ID', cpServer)
        
        except:
            pass
            

        if SALESMAN_ID:
            lisalesman = SALESMAN_ID.split(',')
            liSql = []
            liSql.append({'id':'day','sql':'doy','logic':'='})
            liSql.append({'id':'week','sql':'week','logic':'='})
            liSql.append({'id':'month','sql':'month','logic':'='})
            liSql.append({'id':'quarter','sql':'quarter','logic':'='})
            liSql.append({'id':'year','sql':'year','logic':'='})
            liSql.append({'id':'decade','sql':'decade','logic':'='})
            liSql.append({'id':'century','sql':'century','logic':'='})

            for salesman in lisalesman:
                salesman_name = None
                sSql = 'select cuon_username from staff where staff.id = ' + salesman 
                res1 = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                if res1 and res1 not in ['NONE','ERROR']:
                    salesman_name = res1[0]['cuon_username']
                if salesman_name:    
                    sSql = "select '" + salesman_name + "' as salesman_name_" + salesman + " ,"
                    for vSql in liSql:
                        for z1 in xrange(-5,20):
                            if vSql['id'] == 'decade' and z1 > 4:
                                pass
                            elif vSql['id'] == 'century' and z1 > 1:
                                pass 
                            elif vSql['id'] == 'year' and z1 > 5:
                                pass 
                            elif vSql['id'] == 'quarter' and z1 > 9:
                                pass 
                            elif vSql['id'] == 'month' and z1 > 14:
                                pass     
                            elif vSql['id'] == 'week' and z1 > 9:
                                pass     
                            
                            else:
                                sSql += "(select  count(ps.id) from partner_schedul as ps, address as a, partner as p where a.id = p.addressid and ps.schedul_staff_id = '" + salesman + "' and ps.partnerid = p.id and  ps.process_status != 999 "
                                sSql +=  " and  date_part('" + vSql['sql'] +"',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"') ) " + vSql['logic']+"  date_part('" + vSql['sql'] + "', now()) - " + `z1`
                                if WITHOUT_ID:
                                    liWithoutId = WITHOUT_ID.split(',')
                                    for no_id in liWithoutId:
                                        sSql += ' and a.id != ' + no_id
                                sSql += " and date_part('year',to_date(ps.schedul_date , '" + dicUser['SQLDateFormat'] +"')) >= " + MIN_SCHEDUL_YEAR
                                sSql += self.getWhere('',dicUser,2,'ps.')
                                sSql += ") as " + 'salesman_' + salesman +'_'+ vSql['id'] + '_count_' + `z1`.replace('-','M') + " , "
                                
                                #sSql += " group by a.salesman_id "


                    sSql = sSql[0:len(sSql)-2]
                    self.writeLog(sSql)
                    tmpResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                    if tmpResult and tmpResult not in ['NONE','ERROR']:
                        oneResult = tmpResult[0]
                        for key in oneResult.keys():
                            result[key] = oneResult[key]
                                      
                 

##                                tmpResult = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
##                                if tmpResult and tmpResult not in ['NONE','ERROR']:
##                                    oneResult = tmpResult[0]
##                                    for key in oneResult.keys():
##                                      result['salesman_' + salesman +'_'+ vSql['id'] + '_' + key + '_' + `z1` ] = oneResult[key]
##                                      
                    
                
                
            
        if not result:
            result = 'NONE'


        self.writeLog('salesman-Result = ' + `result`)
        
        return result

    def xmlrpc_getAllAddressForThisPartner(self,sWhere,dicUser):
        sAddressWhere = ' where id = 0'
        sSql = 'select addressid from partner ' + sWhere
        sSql += self.getWhere('',dicUser,2)
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        if result and result not in ['ERROR','NONE']:
            sAddressWhere = ' where '
            for i in result:
                sAddressWhere += ' id = ' + `i['addressid']` + ' or'
                #print sAddressWhere
            sAddressWhere = sAddressWhere[:len(sAddressWhere)-3]
            
        return sAddressWhere
        
    def xmlrpc_getButtonGrave(self, dicUser):
        print 'button grave'
        cpServer, f = self.getParser(self.CUON_FS + '/menus.cfg')
        setButton = self.getConfigOption(dicUser['Name'],'address_button_grave', cpServer)
        print 'setButton = ',  setButton
        buttonPosition = self.getConfigOption(dicUser['Name'],'address_button_grave_position', cpServer)
        if setButton and buttonPosition:
            return setButton.upper() ,  buttonPosition
        else:
            return 'NO',  '-1'
            
    def xmlrpc_getButtonHibernation(self, dicUser):
        print 'button Hibernation'
        cpServer, f = self.getParser(self.CUON_FS + '/menus.cfg')
        setButton = self.getConfigOption(dicUser['Name'],'address_button_hibernation', cpServer)
        print 'setButton = ',  setButton
        buttonPosition = self.getConfigOption(dicUser['Name'],'address_button_hibernation_position', cpServer)
        if setButton and buttonPosition:
            return setButton.upper() ,  buttonPosition
        else:
            return 'NO',  '-1'  
   
   
    def xmlrpc_getAddressEmailID(self, sTable, sEmail, dicUser):
        sEmail = base64.decodestring(sEmail)
        liEmail = sEmail.split(',')

        liID = [0]
        sSql = 'select ' +sTable + '.id from ' + sTable
        
        sSql +=  ' where ( '
        for email in liEmail:
            liSingleEmail = email.split(' ')
            
            for sEmail in liSingleEmail:
                print "sEmail = ", sEmail
#		import os
#        	sEmail = os.popen('/usr/local/bin/parse.sh "' + sEmail + '"').read()
		print "sEmail = ", sEmail
                if sEmail.find('@') > 0:
                    for sSt in ['<', '>', '"', "'"]:
                        sEmail = sEmail.strip(sSt)
                        sEmail = sEmail.strip(sSt)
                    sEmail=self.normalizeXML(sEmail)
                    sSql += "email  ~*  '" + sEmail+ "' or "
                    break
                
                    
                    
                  
        sSql = sSql[0:len(sSql) -3]
        sSql += ' ) '
        sSql += self.getWhere("",dicUser,2, sTable+'.')
        print sSql 
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        
        
        print result
        if result and result not in ['NONE', 'ERROR']:
            liID = []
            for oneRow in result:
                liID.append(oneRow['id'])
            print liID    
            
        if sTable == "address":
            sSql ='select address_id as id from addresses_misc where ('
        elif sTable == "partner":
            sSql ='select id as id from partner where ('
        for email in liEmail:
            liSingleEmail = email.split(' ')
            
            for sEmail in liSingleEmail:
		print "sEmail = ", sEmail
#                import os
#                sEmail = os.popen('/usr/local/bin/parse.sh "' + sEmail + '"').read()
                print "sEmail = ", sEmail
                if sEmail.find('@') > 0:
                    for sSt in ['<', '>', '"']:
                        sEmail = sEmail.strip(sSt)
                        sEmail = sEmail.strip(sSt)
                    if sTable == "address":
                        sSql += " addresses_misc.additional_emails ~* '" + sEmail+ "' or "        
                    elif sTable == "partner":
                        sSql += " partner.additional_emails ~* '" + sEmail+ "' or "     
        sSql = sSql[0:len(sSql) -3]
        sSql += ' ) '
        sSql += self.getWhere("",dicUser,2)
        print sSql 
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        
        
        print result
        if result and result not in ['NONE', 'ERROR']:
           
            for oneRow in result:
                liID.append(oneRow['id'])
            print liID               
                        
        return liID
        


    def xmlrpc_createMiscEntry(self, address_id, dicUser):
        dicEntries = {}
        dicEntries['address_id'] = [address_id, 'int']
        dicEntries['pricegroup1'] = [True, 'bool']
        dicEntries['pricegroup2'] = [False, 'bool']
        dicEntries['pricegroup3'] = [False, 'bool']
        dicEntries['pricegroup4'] = [False, 'bool']
        dicEntries['pricegroup_none'] = [False, 'bool']
        
        
        newID = self.oDatabase.xmlrpc_saveRecord('addresses_misc', -1, dicEntries, dicUser, 'NO')
        
        return newID
        

    def xmlrpc_getLastnameForID(self,  id,  dicUser):
        sSql = "select lastname from fct_getLastname(" + `id` + " )"
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)[0]


    #############################################################################################################
    #
    # LifeCode Functions
    #
    #############################################################################################################
    
    def xmlrpc_sl_search1(self, dicUser,  lastname,  firstname,  city,  info):
        print 'sl search 1',  dicUser,  lastname,  firstname,  city,  info 
        print 'dicUser = ',  dicUser 
        dicUser['client'] = int(dicUser['client'])
        sSql = "select id, address, lastname, lastname2, firstname, street, zip, city from address where "
        
        if lastname:
            sSql += " lastname ~*'" + lastname + "' "
        if firstname:
            if lastname:
                sSql + " and "
            sSql += " firstname ~*'" +firstname + "' "
        if city:
            if lastname or firstname:
                sSql + " and "
            sSql += " city ~*'" + city + "' "
            
        if info:
            if lastname or firstname or city:
                sSql + " and "
            sSql += " status_info ~*'" + info + "' " 
            
        sSql += self.getWhere("", dicUser, 2) 
        
        sSql += " order by lastname LIMIT 20"
        
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        ret = {}
        if result and result not in self.liSQL_ERRORS:
            for row in result:
                ret[`row['id']`] = row['lastname']+ '\n' + row['lastname2'] + '\n' + row['firstname']+  '\n' + row['street']+'\n' + row['zip'] + ' ' + row['city']
                #ret[`row['id']`] = "Hallo" + '\n'
                
        else:
            ret = {'0':'NONE'}
        print "ret from search1 = ",  ret
        return ret
         
    def xmlrpc_sl_getPhones(self, dicUser, id):
        sSql = "select phone,  phone_handy , email from address where id = " + `id` 
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        retValue = {'phone':'NONE', 'phone_handy':'NONE', 'email':'NONE'}
        if result and result not in self.liSQL_ERRORS:
            retValue = result[0]
        
        return retValue
        
    def xmlrpc_sl_getPartner(self, dicUser, addressID):
        print "search Partner for ",  addressID
        sSql = "select id, address, lastname,lastname2, firstname, street, zip, city from partner where addressid = " + `addressID` 
        sSql += self.getWhere("", dicUser,  2 )
        sSql += " order by lastname"
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        retValue = {}
        if result and result not in self.liSQL_ERRORS:
            for row in result:
                retValue[`row['id']`] = row['address']+ '\n' + row['lastname']+ '\n' + row['lastname2'] + '\n' + row['firstname']+  '\n' + row['street']+'\n' + row['zip'] + ' ' + row['city']
                #ret[`row['id']`] = "Hallo" + '\n'
                
        else:
            retValue = {'0':'NONE'}
        print 'retValue = ',  retValue 
        return retValue
        
    def xmlrpc_sl_getPartnerPhones(self, dicUser, id):
        sSql = "select phone1,  phone_handy, phone, email from partner where id = " + `id` 
        sSql += self.getWhere("", dicUser,  2 )
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        retValue = {'phone':'NONE', 'phone1':'NONE','phone_handy':'NONE', 'email':'NONE'}
        if result and result not in self.liSQL_ERRORS:
            retValue = result[0]
        
        return retValue

    
    def xmlrpc_sl_getNotes(self, dicUser, id):
        #self.writeLog('result Notes dicuser =' + `dicUser`)
        sSql = "select notes_misc as misc, notes_contacter as contacter,  notes_representant as representant, notes_salesman as salesman ,notes_organisation as organisation from address_notes where address_id = " + `id` 
        sSql += self.getWhere("", dicUser,  2 )
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,  dicUser)
        retValue = {'0':'NONE', '1':'NONE','2':'NONE','3':'NONE', '4':'NONE'}
        #self.writeLog('result Notes = ' +  `result`)
        if result and result not in self.liSQL_ERRORS:
            retValue = {}
            if not result[0]['misc']:
                result[0]['misc']='NONE'
            retValue['0']= result[0]['misc']
            if not result[0]['contacter']:
                result[0]['contacter']='NONE'
            retValue['1']= result[0]['contacter']
            if not result[0]['representant']:
                result[0]['representant']='NONE'
            retValue['2']= result[0]['representant']
            if not result[0]['salesman']:
                result[0]['salesman']='NONE'
            retValue['3']= result[0]['salesman']
            if not result[0]['organisation']:
                result[0]['organisation']='NONE'
            retValue['4']= result[0]['organisation']
        
        return retValue
