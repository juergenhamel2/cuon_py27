#include <gnome.h>


gboolean
on_Mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_chooseAddress_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_phase1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_phasenew1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_phaseedit1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_phasesave1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_phasedelete1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_task1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_tasknew1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_taskedit1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_task_save1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_task_delete1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_staff_resources_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_staff_resources_new1_activate       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_staff_resources_edit1_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_staff_resources_save1_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_staff_resources_delete1_activate    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_material_resources_new1_activate    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_on_material_resources_edit1_activate
                                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_material_resources_save1_activate   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_material_resources_delete1_activate (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_eFind_key_press_event               (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data);

void
on_eAddressNumber_changed              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bChoose_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_eProjectStartAt_changed             (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_eProjectEndsAt_changed              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_calendar1_day_selected_double_click (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_calendar2_day_selected_double_click (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_bProjectDMS_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_bGotoAddress_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_bLetter_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_ePhaseStartAt_changed               (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_ePhaseEndsAt_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_PhaseCalendar1_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_PhaseCalendar2_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_eTaskStartAt_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_eTaskEndsAt_changed                 (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_TaskCalendar1_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_TaskCalendar2_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_eSRStaffNumber_changed              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bChooseStaff_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_eSRPlanedDate_changed               (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_eSRPlanedBegin_changed              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_eSRPlanedEnd_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bRange1_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_staffCalendar1_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_eSRRealDate_changed                 (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_eSRRealBegin_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_eSRRealEnds_changed                 (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_staffCalendar2_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);

void
on_eMRArticleNumber_changed            (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bChooseArticle_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_eMRDate_changed                     (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_MRcalendar1_day_selected_double_click
                                        (GtkCalendar     *calendar,
                                        gpointer         user_data);
