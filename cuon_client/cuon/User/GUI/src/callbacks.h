#include <gtk/gtk.h>


void
on_print_setup1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_New1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_Edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_Save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_Delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_eFindTitle_editing_done             (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_eFindTitle_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_eFindDesignation_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_eFindDesignation_key_press_event    (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data);
