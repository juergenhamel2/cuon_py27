#include <gnome.h>


void
on_tBack_clicked                       (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tReload_clicked                     (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tForward_clicked                    (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_tQuit_clicked                       (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

gboolean
on_eUrl_key_press_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);
