#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_choose_profile1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{

}


void
on_colorFG_color_set                   (GtkColorButton  *colorbutton,
                                        gpointer         user_data)
{

}


void
on_colorDutyBG_color_set               (GtkColorButton  *colorbutton,
                                        gpointer         user_data)
{

}


void
on_colorDutyFG_color_set               (GtkColorButton  *colorbutton,
                                        gpointer         user_data)
{

}


void
on_colorBG_color_set                   (GtkColorButton  *colorbutton,
                                        gpointer         user_data)
{

}


void
on_bListScanDevices_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bOOWriter_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bOOCalc_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bOODraw_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bOOImpress_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bImage_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bMusic_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bOgg_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bWav_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bText_clicked                       (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bTex_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bLaTex_clicked                      (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bFlowchart_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bHtml_clicked                       (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bInternet_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bGoogleearth_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}

