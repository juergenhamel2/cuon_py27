// Java Client AI
import java.util.Vector;
import java.util.Hashtable;
import helma.xmlrpc.*;
import java.io.*;

public class JavaClient {

    // The location of our server.
    public final static String server_url =
        "http://localhost:7080";

    
    public  void speak()throws IOException{
        
        BufferedReader inType = new BufferedReader( new InputStreamReader(System.in));
        boolean ok = true ;
        while (ok){
            String sIn = inType.readLine();
            System.out.println ("This is type" + sIn);
            if (sIn.equals("ready")){
                ok = false;
            }
        }
    }
    
    public void readConfig()throws IOException{
//~         Properties configProperties = new Properties()
//~         FileInputStream f1 = new FileInputStream("cuon_mini_client.ini")
//~         configProperties.load(f1)
        BufferedReader inType = new BufferedReader( new InputStreamReader(System.in));
        System.out.println ("Input your Username");
        String sUser = inType.readLine();
        System.out.println ("Input your pasword");
    
        String sPassword = inType.readLine();
        System.out.println ("This is type" + sUser + ", " + sPassword);
        // Build our parameter list.
        Vector params = new Vector();
        params.addElement(new String(sUser));
        params.addElement(new String(sPassword));

        return params ;
    }
    
    public static void main (String [] args) {
        try {
            JavaClient jc = new JavaClient();
            // Create an object to represent our server.
            XmlRpcClient server = new XmlRpcClient(server_url);

            Vector params = jc.readConfig() ;
            // Call the server, and get our result.
            //sid = self.Server.src.Databases.py_createSessionID( Username, Password)
            String sessionID = server.execute("Databases.createSessionID", params);
            System.out.println (sessionID);
        
        //int sum = ((Integer) result.get("sum")).intValue();
          //  int difference = ((Integer) result.get("difference")).intValue();

            // Print out our result.

        } catch (XmlRpcException exception) {
            System.err.println("JavaClient: XML-RPC Fault #" +
                               Integer.toString(exception.code) + ": " +
                               exception.toString());
        } catch (Exception exception) {
            System.err.println("JavaClient: " + exception.toString());
        }
    }
}
