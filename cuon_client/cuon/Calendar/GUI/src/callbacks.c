#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_cut1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_copy1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_paste1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_rbVisualDay_toggled                 (GtkToggleToolButton *toggletoolbutton,
                                        gpointer         user_data)
{

}


void
on_rbvisualMonth_toggled               (GtkToggleToolButton *toggletoolbutton,
                                        gpointer         user_data)
{

}


void
on_calendar1_day_selected              (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}


void
on_calendar1_day_selected_double_click (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}


void
on_calendar1_month_changed             (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}


void
on_calendar1_next_month                (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}


void
on_calendar1_next_year                 (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}


void
on_calendar1_prev_month                (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}


void
on_calendar1_prev_year                 (GtkCalendar     *calendar,
                                        gpointer         user_data)
{

}

