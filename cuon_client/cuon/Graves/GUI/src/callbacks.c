#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


gboolean
on_Mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print_icoming_document1_activate    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print_pickup_document1_activate     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print_outgoing_document1_activate   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_InvoicesNew1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_InvoicesEdit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_InvoicesSave1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_InvoicesClear1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_MaintenanceNew1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_MaintenanceEdit1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_MaintenanceSave1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_MaintenanceClear1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SpringNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SpringEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SpringSave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SpringClear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SummerNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SummerEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SummerSave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SummerClear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AutumnNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AutumnEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AutumnSave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AutumnDelete1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_HolidaysNew1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_HolidaysEdit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_HolidaysSave1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_HolidaysClear1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_WinterNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_WinterEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_WinterSave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_WinterClear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AnnualNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AnnualEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AnnualSave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AnnualClear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_UniqueNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_UniqueEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_UniqueSave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_UniqueClear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_tbNew_clicked                       (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


void
on_tbEdit_clicked                      (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


void
on_tbSave_clicked                      (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


void
on_tbDelete_clicked                    (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


gboolean
on_eFindButton_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{

}


void
on_bChooseAddress_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eAddressNumber_changed              (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


gboolean
on_key_press_event                     (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_bGotoAddress_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eGraveyardID_changed                (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bSearchGraveyard_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eInvoiceAddressID_changed           (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bInvoiceChooseAddress_clicked       (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eInvoiceTOPID_changed               (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bInvoiceChooseTOP_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eArticleID_changed                  (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bChooseServiceArticle_clicked       (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bChooseServices_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bChooseArticle_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bSpringChooseArticle_clicked        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_treeMaterialgroup_row_activated     (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_treeArticles_row_activated          (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_bQuickAppend_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}

