-- -*- coding: utf-8 -*-

-- Copyright (C) [2003 -2012]  [Juergen Hamel, D-32584 Loehne, Germany]

-- This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.

-- You should have received a copy of the GNU General Public License along with this program; if not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
-- IMPORTANT
-- To use this without cuon (www.cuon.org) replace all $$ with a '


CREATE OR REPLACE FUNCTION fct_create_single_account(i_client int, i_number text, t_designation text, d_date date, account_id int, counter_account_id int, f_amount float ,  address_id int, cUUID varchar(36) ) returns int AS $$
 DECLARE
    
    
   
    sSql text ;
    sSql2 text ;
    rID int ;
    cNormalUUID varchar(36) ;
   
    
    
    BEGIN
        cNormalUUID = fct_new_uuid() ;
        
        sSql := $$' insert into account_system (id, uuid, user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,document_number,document_designation,account_id,contra_account_id,amount,address_id, actual_date, transaction_uuid ) values $$' ;
        -- raise notice $$'sSql1 = %$$', sSql ;
        sSql := sSql || $$'( nextval($$' || quote_literal($$'account_system_id$$') || $$'),$$' || quote_literal(cNormalUUID) || $$', current_user,$$' || quote_literal($$'insert$$') ||$$', $$' ||  quote_literal(now()) || $$',NULL,NULL,$$' || i_client || $$',0,0,0,nextval($$' || quote_literal($$'numerical_accounting_booknumber_client_$$'  || i_client ) || $$'), $$' ;
        
        -- raise notice $$'sSql2 = %$$', sSql ;
        sSql := sSql || quote_literal(t_designation) || $$', $$' ; 
        -- raise notice $$'sSql3 = %$$', sSql ;
        
        sSql := sSql || account_id  || $$', $$' || counter_account_id || $$', $$' || f_amount || $$', $$' ;
        
        -- raise notice $$'sSql4 = %$$', sSql ;
        sSql := sSql || address_id ||$$', $$' || quote_literal(d_date) || $$', $$' || quote_literal(cUUID )|| $$') $$' ;
        
        raise notice $$'sSql9 = % $$', sSql ;
        execute(sSql);
        sSql := $$'select id from account_system where uuid = $$' || quote_literal(cNormalUUID );
        execute(sSql) into rID ;
        RETURN rID ;

    END ;
    
     $$ LANGUAGE 'plpgsql'; 
      
