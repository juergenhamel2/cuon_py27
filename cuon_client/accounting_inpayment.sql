-- -*- coding: utf-8 -*-

-- Copyright (C) [2003 -2012]  [Juergen Hamel, D-32584 Loehne, Germany]

-- This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.

-- You should have received a copy of the GNU General Public License along with this program; if not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
-- IMPORTANT
-- To use this without cuon (www.cuon.org) replace all $$ with a '

DROP FUNCTION fct_incomming_bank(int) CASCADE ;


CREATE OR REPLACE FUNCTION fct_incomming_bank(cUUID varchar(36),iBankNumber int ) returns text AS $$
 DECLARE
  
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    iNewAccountingID int ;
    sSql text ;
    sSql1 text ;
    
    sSql2 text ;
    account_revenue_id int;
    account_receivable_id int ;
    account_taxvat_id int ;
    account_revenue_net_id int := 0;
    
    
    BEGIN
        iClient = fct_getUserDataClient(  ) ;
        sSql := $$'select id  from account_info where accounting_system_type = $$'$$'2400$$'$$' $$' || fct_getWhere(2,$$' $$')  ;
        execute(sSql) into account_receivable_id ;
           
       return new_transaction_uuid ;
        
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
     
