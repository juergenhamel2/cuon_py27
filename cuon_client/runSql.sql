
    
CREATE OR REPLACE FUNCTION fct_setNumberAndDescription(iID int, sType text ) returns bool as '
 DECLARE
    sSql text ;
    sSql2 text ;
    defaultOrderNumber text ;
    defaultOrderDesignation text;
    sNumber text ;
    sDefault text ;
    
    r record ;
    bOK bool ;
    iClient int ;
    sDB text ;
    sDBPositions text ;
    vMisc return_misc3 ;
    sDatabase return_text3;
    z1 int ;
    i text ;
    sON text ;
    sc text ;
    newSeq int ;
    fieldNumber text ;
    fieldAddressid text ;
    colname		TEXT;
    colcontent	TEXT;
    colnames	TEXT[];
    coln		INT4;
    coli		INT4;
    
    BEGIN
        bOK := true ;
        iClient = fct_getUserDataClient(  ) ;
        sc := ''_client_'' || iClient ;
     
   
        sDatabase = fct_get_database(sType);
        sDB = sDatabase.a ;
        sDBPositions = sDatabase.b ;
      
        fieldNumber := ''number'' ;
        fieldAddressid := ''addressnumber'' ;
        
        if sType = ''Proposal'' then 
            sNumber = ''proposal_number'' ;
            sDefault = ''proposal_designation'';
        elseif sType = ''Enquiry'' then 
            sNumber = ''enquiry_number'';
            sDefault = ''enquiry_designation'';
        elseif sType = ''Project'' then
            sNumber = ''project_number'';
            sDefault = ''project_designation'' ; 
            fieldNumber := ''name'' ;
            fieldAddressid := ''customer_id'' ;
        else
            sNumber = ''orderbook_number'';
            sDefault = ''orderbook_designation'';
                
        end if ;
        defaultOrderNumber = fct_get_config_option(iClient,''clients.ini'', ''CLIENT_'' || iClient, sNumber ) ; 
        defaultOrderDesignation = fct_get_config_option(iClient,''clients.ini'', ''CLIENT_'' || iClient, sDefault) ; 
raise notice ''options = %, %'', defaultOrderNumber, defaultOrderDesignation ;
        if defaultOrderNumber is not null then 
            z1 = 1 ;
            sON := '''' ;
            <<SplitNumber>> 
            LOOP 
                select into i split_part(defaultOrderNumber, '','', z1);
                    EXIT  SplitNumber  WHEN char_length(i) = 0  ;
                    raise notice ''i = % '', i ;
                    if i = ''!id'' then 
                        
                       sON :=  sON || iID ;
                        
                        raise notice ''sON = % '', sON ;
                
                   elseif i=''!year''then
                        sON :=  sON || date_part(''year'', now()) ;
                    elseif i=''!month''then 
                        sON    :=  sON || date_part(''month'', now()) ;
                    elseif i=''!day'' then 
                        sON :=  sON || date_part(''day'', now()) ;
                    elseif  substring(i from 1 for 4 ) = ''!seq'' then
                        if sType = ''Enquiry''then 
                            sSql = ''select nextval('' || quote_literal(''numerical_enquiry_enquirynumber'' || sc ) || '' )'' ;
                        elseif sType = ''Proposal''then 
                            sSql = ''select nextval('' || quote_literal(''numerical_proposal_proposalnumber'' || sc ) || '' )'' ;
                        elseif sType = ''Order''then 
                            sSql = ''select nextval('' || quote_literal(''numerical_orderbook_ordernumber'' || sc ) || '' )'' ;
                        elseif sType = ''Project''then 
                            sSql = ''select nextval('' || quote_literal(''numerical_project_projectnumber'' || sc ) || '' )'' ;
                        end if ;
                        
                        raise notice '' sSql = %'' ,  sSql ;
                        
                        execute (sSql) into newSeq ;
                        
                        if newSeq is not null then 
                            
                            sON := sON || to_char(newSeq,''FM99990000'') ;
                        end if ;
                        
                    else 
                        sON := sON ||  i ;
                        
                    raise notice ''sON = % '',  sON ;
                    end if ;
                    
                z1 := z1 + 1 ;
                
           
            
            END LOOP  SplitNumber ;
        end if ;
        
         if char_length(sON) > 30 then 
            sON = substring(sON from 1 for 30) ;
        end if ;
        sSql := ''update '' || sDB || '' set '' || fieldNumber || '' = '' || quote_literal(sON) ||  '' where id = '' || iID ;
         -- raise notice '' sSql = %'' ,  sSql ;
        execute(sSql);
            
            
        if defaultOrderDesignation is not null then 
            sSql := ''select * from address where id = ( select '' || fieldAddressid || '' from '' || sDB  || '' where id = '' || iID || '')'' ;
            raise notice '' sSql = %'' ,  sSql ;
            execute (sSql) into r ;
            
            -- raise notice ''colnames %'', r;
            
            z1 := 1 ;
            sON = '''' ;
            <<SplitDesignation>> 
            LOOP 
                select into i split_part(defaultOrderDesignation, '','', z1);
                EXIT  SplitDesignation WHEN char_length(i) = 0  ;
                if substring(i from 1 for 1 ) = ''!'' then 
                    if i = ''!id'' then 
                        sON := sON || r.id ;
                    elseif i = ''!address'' then 
                        sON := sON || r.address ;
                    elseif i = ''!lastname'' then 
                        sON := sON || r.lastname ;   
                    elseif i = ''!lastname2'' then 
                        sON := sON || r.lastname2 ;
                    elseif i = ''!firstname'' then 
                        sON := sON || r.firstname ;   
                    elseif i = ''!street'' then 
                        sON := sON || r.street ;   
                    elseif i = ''!zip'' then 
                        sON := sON || r.zip ;   
                    elseif i = ''!city'' then 
                        sON := sON || r.city ;   
                         
                    end if ;  
                        
                        
                        
                else 
                        sON := sON ||  i ;
                end if ;
                 --  raise notice ''sON = % '',  sON ;
                 
                z1 := z1 + 1 ;
                
                
            END LOOP  SplitDesignation ;
        
        end if ;
                
             -- raise notice ''sON Last = % '',  sON ;
        if char_length(sON) > 50 then 
            sON = substring(sON from 1 for 50) ;
        end if ;
        
        sSql := ''update '' || sDB || '' set designation  = '' || quote_literal(sON) ||  '' where id = '' || iID ;
         -- raise notice '' sSql = %'' ,  sSql ;
        execute(sSql);
            
        return bOK ;
        
    END ;
    
     ' LANGUAGE 'plpgsql';       
   
DROP FUNCTION fct_changeEnquiry2Proposal(int);
CREATE OR REPLACE FUNCTION fct_changeEnquiry2Proposal(iEnquiryID int ) returns int as '
 DECLARE
    
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    sSql text ;
    sSql2 text ;
    bOK bool ;
    r record ;
    BEGIN
        iNewOrderID := 0 ;
        
        sSql := ''select * from fct_duplicate_table_entry('' || quote_literal(''enquiry'') || '','' || quote_literal(''proposal'') || '','' || iEnquiryID || '', '' || quote_literal(''proposal_id'') || '')  as newid '' ;
        raise notice '' sSql = %'',sSql ;
        execute(sSql) into iNewOrderID;
        raise notice ''new order id = %'',iNewOrderID ;
        
        IF iNewOrderID > 0 THEN 
            -- copy the enquirypositions 
            
            sSql := '' select id from enquiryposition where orderid = '' || iEnquiryID  || '' '' || fct_getWhere(2,'' '')  ;
            
            FOR r in execute(sSql)  LOOP
                sSql2 := ''select * from fct_duplicate_table_entry('' || quote_literal(''enquiryposition'') || '','' || quote_literal(''proposalposition'') || '','' || r.id || '', '' || quote_literal(''proposalposition_id'') || '')  as newid '' ; 
               
                raise notice '' sSql2 = %'',sSql2 ;
                execute(sSql2) into iNewPositionID;
                update proposalposition set orderid = iNewOrderID where id =  iNewPositionID ;
                
            END LOOP ;
            
            
            
            
            sSql = ''update proposal set process_status = 300,orderedat = now(), enquiry_id = '' || iEnquiryID || '' where id =  '' || iNewOrderID ;
            execute(sSql);
             
            select into bOK * from fct_setNumberAndDescription(iNewOrderID, ''Proposal'') ;
            
            
          
        END IF ;
            
       return iNewOrderID ;
        
    END ;
    
     ' LANGUAGE 'plpgsql'; 
  

DROP FUNCTION fct_changeProposal2Order(int) ;
CREATE OR REPLACE FUNCTION fct_changeProposal2Order(iProposalID int ) returns int as '
 DECLARE
    
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    sSql text ;
    sSql2 text ;
    bOK bool ;
    r record ;
    BEGIN
        iNewOrderID := 0 ;
        
        sSql := ''select * from fct_duplicate_table_entry('' || quote_literal(''proposal'') || '','' || quote_literal(''orderbook'') || '','' || iProposalID || '', '' || quote_literal(''orderbook_id'') || '')  as newid '' ;
        raise notice '' sSql = %'',sSql ;
        execute(sSql) into iNewOrderID;
        raise notice ''new order id = %'',iNewOrderID ;
        
        IF iNewOrderID > 0 THEN 
            -- copy the proposalpositions 
            
            sSql := '' select id from proposalposition where orderid = '' || iProposalID  || '' '' || fct_getWhere(2,'' '')  ;
            
            FOR r in execute(sSql)  LOOP
                sSql2 := ''select * from fct_duplicate_table_entry('' || quote_literal(''proposalposition'') || '','' || quote_literal(''orderposition'') || '','' || r.id || '', '' || quote_literal(''orderposition_id'') || '')  as newid '' ; 
               
                raise notice '' sSql2 = %'',sSql2 ;
                execute(sSql2) into iNewPositionID;
                update orderposition set orderid = iNewOrderID where id =  iNewPositionID ;
                
            END LOOP ;
            
            
            
            
            update orderbook set process_status = 500 where id =  iNewOrderID ;
            update orderbook set orderedat = now() where id =  iNewOrderID ;
            select into bOK * from fct_setNumberAndDescription(iNewOrderID, ''Order'') ;
            
        END IF ;
            
       return iNewOrderID;
        
    END ;
    
     ' LANGUAGE 'plpgsql'; 
    
   
   
DROP FUNCTION   fct_set_top_for_cash_desk( int);
CREATE OR REPLACE FUNCTION  fct_set_top_for_cash_desk(  iOrderid int) returns int as '
    DECLARE
    iOK integer ;
    iClientID integer ;
    top_id integer ;
    orderinvoiceid integer ;
    sSql text ;
    newID integer ;

    BEGIN
	select into iClientID * from  fct_getUserDataClient();
	raise notice ''Client ID = %'', iClientID ;

	select into top_id * from fct_get_config_option(iClientID, ''clients.ini'', ''CLIENT_'' || iClientID, ''cash_desk_top'' ) ;
	
	iOK := top_id;
	raise notice ''iOK = %'', iOK ;
	-- update orderinvoice set 
	if iOK is not null then 
		sSql = ''select  id from orderinvoice where orderid = '' ||  iOrderid || '' '' ||  fct_getWhere(2,'' '')  ;
	
		execute sSql into orderinvoiceid ;
	raise notice ''sSql = %, %'',sSql,orderinvoiceid ;
		if orderinvoiceid is null then 
		        raise notice ''Not found, do an insert '' ;
			select nextval(''orderinvoice_id'') into newID ;
                        insert into orderinvoice (id,uuid,orderid, order_top) values (newID,fct_new_uuid(),iOrderID,top_id)  ;
		else
			update orderinvoice set order_top = top_id where id = orderinvoiceid ;
			
     		END IF ;

	ELSE 
	     iOK = 0;
	END IF ;

        return iOK  ;
    END ;
    ' LANGUAGE 'plpgsql'; 
    
   

CREATE OR REPLACE FUNCTION fct_getOrderTotalNetSum(  iOrderid int) returns float as '
DECLARE
    fSum     float ;

 BEGIN
    select fct_getOrderTotalSum(iOrderid,''t'') into fsum ;
    return fSum ;
    END ;
    ' LANGUAGE 'plpgsql'; 


CREATE OR REPLACE FUNCTION fct_getOrderTotalSum(  iOrderid int, sType text) returns float as '
    DECLARE
    fSum     float ;
    sClient char ;
    cur1 refcursor ;
    sCursor text ;
    sSql text ; 
    fAmount     float;
    fPrice  float;
    fDiscount   float ;
    count   integer ;
    fTaxVat float ;
    iArticleID integer ;
    bNet  bool ;
    sDatabase return_text3;
    sDB text ;
    sDBPositions text ;
    vMisc return_misc3 ;
     
     
    BEGIN
    sDatabase = fct_get_database(sType);
    sDB = sDatabase.a ;
    sDBPositions = sDatabase.b ;
   
    
            
            
            
            
        sCursor := ''SELECT amount, price, discount, articleid, tax_vat FROM '' || sDBPositions || '' WHERE  orderid = ''|| iOrderid || '' '' || fct_getWhere(2,'' '')  ;
        fSum := 0.0 ;
        -- RAISE NOTICE ''sCursor = %'', sCursor ;
        OPEN cur1 FOR EXECUTE sCursor ;
        FETCH cur1 INTO fAmount, fPrice, fDiscount,iArticleID, fTaxVat ;

        count := 0;

        WHILE FOUND LOOP
            RAISE NOTICE ''total sum for position , amount %, price % , discount %'', fAmount,fPrice,fDiscount ;
            if fDiscount IS NULL then
                fDiscount := 0.0 ;
            end if ;
            if fTaxVat IS NULL then 
                fTaxVat:= 0.00;
            END IF;
            if fPrice IS NULL then
                fPrice := 0.0 ;
            end if ;
            
            vMisc := fct_get_taxvat_for_article(iArticleID);
            fTaxVat = vMisc.b ;
            
            -- now search for brutto/netto
            bNet := fct_get_net_for_article(iArticleID);
            raise notice '' order bNet value is %'',bNet ;
            if fTaxVat > 0 THEN
                if bNet = true then 
                -- raise notice '' order calc as bnet is true'';
                    fSum := fSum + ( fAmount * ( fPrice + (fPrice *fTaxVat/100) ) * (100 - fDiscount)/100 ) ;
                else 
                    fSum := fSum + ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                end if ;
                
            ELSE    
                fSum := fSum + ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                
            END IF ;
            FETCH cur1 INTO fAmount, fPrice, fDiscount ,iArticleID, fTaxVat;
    END LOOP ;
    close cur1 ;
    
    -- now get the whole discount 
    sSql := ''select discount from '' || sDB || '' where id = '' || iOrderid || '' '' || fct_getWhere(2,'' '');
    execute sSql into fDiscount ;
    if fDiscount IS NULL then
            fDiscount := 0.0 ;
        end if ;
    fSum := fSum * ((100 - fDiscount)/100 )  ;
    
    return fSum ;
    END ;
    ' LANGUAGE 'plpgsql'; 

  
  
CREATE OR REPLACE FUNCTION fct_getOpenInvoice(  iOrderNumber int) returns float as '
    DECLARE
    fSum float ;
    iOrder integer ;
    sSql text := '''';
 BEGIN
     sSql := ''select  sum(list_of_invoices.total_amount ) -  fct_getOrderTotalSum('' || iOrderNumber || '') as residue from list_of_invoices where list_of_invoices.order_number = '' || iOrderNumber || '' '' ||  fct_getWhere(2,'' '') ;
    -- RAISE NOTICE '' get residue for Invoice sql = %'', sSql ;
    
    execute sSql into  fSum ;
   
    return fSum ;
    END ;
    ' LANGUAGE 'plpgsql'; 
    
    
 
CREATE OR REPLACE FUNCTION fct_getInpayment(  iInvoice int) returns float as '
    DECLARE
    fSum float ;
    sum_inpayment float ;
    sum_discount float ;
    iOrder integer ;
    sSql text := '''';
    r record ;
    sNumber varchar(20) := ''99999999999'' ;
    BEGIN
        sSql = ''select sum(in_payment.inpayment)   as sum_inpayment from in_payment where   to_number(in_payment.invoice_number,'' || quote_literal(sNumber)  || '') = ''  || iInvoice || '' '' ||  fct_getWhere(2,'' '') ;  
        -- RAISE NOTICE '' get residue for Invoice sql = %'', sSql ;
        execute sSql into  sum_inpayment ;
        

        if sum_inpayment is null then
            sum_inpayment := 0.00 ;
        end if ;
         sSql = ''select sum(in_payment.cash_discount)   as sum_discount from in_payment where   to_number(in_payment.invoice_number,'' || quote_literal(sNumber)  || '') = ''  || iInvoice || '' '' ||  fct_getWhere(2,'' '') ;  
        execute sSql into  sum_discount ;
        
        if sum_discount is null then
            sum_discount := 0.00 ;
        end if ;
        fSum := sum_inpayment + sum_discount ;
        
        return fSum ;
  
    END ;
    ' LANGUAGE 'plpgsql'; 
   
CREATE OR REPLACE FUNCTION fct_getResidueForInvoice(  iInvoice int) returns float as '
    DECLARE
    fSum float ;
    iOrder integer ;
    sSql text := '''';
 BEGIN
     sSql := ''select  list_of_invoices.total_amount  -  fct_getInpayment(list_of_invoices.invoice_number ) as residue from list_of_invoices where list_of_invoices.id = '' || iInvoice || '' '' ||  fct_getWhere(2,'' '') ;
    -- RAISE NOTICE '' get residue for Invoice sql = %'', sSql ;
    
    execute sSql into  fSum ;
   
    return fSum ;
    END ;
    ' LANGUAGE 'plpgsql'; 
    
CREATE OR REPLACE FUNCTION fct_getResidue( ) returns setof  record as '
 DECLARE
     iClient int ;
    searchsql text := '''';
    r  record;
    r2 record ;
    sSql text := '''' ;
    BEGIN
       
        searchsql := ''select  list_of_invoices.order_number, fct_getResidueForInvoice(list_of_invoices.id) as residue, id  from list_of_invoices   ''  || fct_getWhere(1,'' '') || '' order by list_of_invoices.id'' ;
        
        -- RAISE NOTICE '' get residue sql = %'', searchsql ;

        FOR r in execute(searchsql)  LOOP
        
        IF r.residue > 0.01  THEN
        
            
          
                sSql := ''select  list_of_invoices.total_amount as total_amount, address.lastname as lastname, address.city as city, orderbook.id as order_id, list_of_invoices.maturity as maturity, fct_getResidueForInvoice(list_of_invoices.id) as residue ,  list_of_invoices.order_number as order_number,  list_of_invoices.invoice_number as invoice_number, list_of_invoices.date_of_invoice as date_of_invoice, current_date  from list_of_invoices , orderbook, address  where list_of_invoices.id = '' ||  r.id  || '' and list_of_invoices.order_number = orderbook.id and address.id = orderbook.addressnumber order by list_of_invoices.maturity'';
            FOR r2 in execute(sSql)  LOOP
                return NEXT r2 ;
            END LOOP ;
        
        END IF ;

        
        END LOOP ;
        
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 

     
        
CREATE OR REPLACE FUNCTION fct_getReminder( iDays integer) returns setof  record as '
 DECLARE
     iClient int ;
    sSql text := '''';
    r  record;
    r2 record ;
    
    BEGIN
       sSql := '' select total_amount,lastname, city, order_id, maturity,  residue , order_number, invoice_number,date_of_invoice, this_date from fct_getResidue() as (total_amount float, lastname varchar(150),  city varchar(150),  order_id integer,  maturity date,residue float,  order_number integer, invoice_number integer, date_of_invoice date, this_date date )  ''  ;
       
        FOR r in execute(sSql)  LOOP
        
        IF r.this_date - r.maturity > iDays   THEN
            return next r;
        END IF ;
        
        END LOOP ;
        
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 
     
     
DROP FUNCTION fct_duplicateOrder(integer);

CREATE OR REPLACE FUNCTION fct_duplicateOrder( iOrderID integer, OrderType integer) returns int as '
 DECLARE
     
    newOrderID int ;
     new_table   varchar(400) ;
     
      sSql text ;
    sSql2 text ;
        sExe text ;
        sExe2 text ;
        sTable  text;
        rData  record ;
        sCursor text ;
        iPosID int ;
        cur1 refcursor ;
        partPrefix text ;
    BEGIN
    partPrefix = '''' ;
    
    select nextval(''orderbook_id'') into newOrderID ;
    
       select into rData user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,  number ,  designation ,         orderedat , deliveredat , packing_cost , postage_cost , misc_cost ,build_retry , type_retry  , supply_retry, gets_retry , invoice_retry , custom_retry_days , modul_number  ,                modul_order_number, discount  , ready_for_invoice , process_status   , proposal_number , customers_ordernumber , customers_partner_id , project_id , versions_number ,  versions_uuid   , staff_id , addressnumber from orderbook where id =  iOrderID    ;
    
        
        RAISE NOTICE ''status by rdata = %'', rData.status;
        
        
    insert into orderbook (id, user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,  number ,  designation ,         orderedat , deliveredat , packing_cost , postage_cost , misc_cost ,build_retry , type_retry  , supply_retry, gets_retry , invoice_retry , custom_retry_days , modul_number  ,                modul_order_number, discount  , ready_for_invoice , process_status   , proposal_number , customers_ordernumber , customers_partner_id , project_id , versions_number  ,versions_uuid   , staff_id , addressnumber) values ( newOrderID, rData.user_id , rData.status ,  rData.insert_time , rData.update_time, rData.update_user_id  ,  rData.client ,  rData.sep_info_1 ,  rData.sep_info_2 ,  rData.sep_info_3 ,  ''NEW-'' || rData.number || partPrefix ,  rData.designation ,         rData.orderedat , rData.deliveredat , rData.packing_cost , rData.postage_cost , rData.misc_cost ,rData.build_retry , rData.type_retry  , rData.supply_retry, rData.gets_retry , rData.invoice_retry , rData.custom_retry_days , rData.modul_number  , rData.modul_order_number, rData.discount  , rData.ready_for_invoice , rData.process_status   , rData.proposal_number , rData.customers_ordernumber , rData.customers_partner_id , rData.project_id , 0,   fct_new_uuid()  , rData.staff_id, rData.addressnumber) ;
            
       
     
        sCursor := ''SELECT id from orderposition WHERE  orderid = ''|| iOrderID || '' '' || fct_getWhere(2,'' '')  ;
       
        OPEN cur1 FOR EXECUTE sCursor ;
        FETCH cur1 INTO iPosID ;

     

        WHILE FOUND LOOP
        RAISE NOTICE ''iPosID % '', iPosID ;
        select into rData user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,orderid,   articleid, designation, amount,  position,   price , tax_vat,  discount,  versions_number  ,versions_uuid  from orderposition where id = iPosID ;
        
        insert into orderposition (id, user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,orderid,   articleid, designation, amount,  position,   price , tax_vat,  discount,  versions_number  ,versions_uuid ) values ( nextval(''orderposition_id''), rData.user_id , rData.status ,  rData.insert_time , rData.update_time, rData.update_user_id  ,  rData.client ,  rData.sep_info_1 ,  rData.sep_info_2 ,  rData.sep_info_3 , newOrderID,   rData.articleid, rData.designation, rData.amount,  rData.position,   rData.price , rData.tax_vat,  rData.discount,    0,   fct_new_uuid()  )  ; 
           
        FETCH cur1 INTO iPosID ;
    END LOOP ;
       
    close cur1 ;
    
      
       
       
     --  RAISE NOTICE ''sql = %'', sSql ; 
      --  execute(sSql) ;
    return newOrderID ;    
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 

     
DROP function fct_getUnreckonedOrder() ;
DROP function fct_getUnreckonedOrder(integer) ;
CREATE OR REPLACE FUNCTION fct_getUnreckonedOrder(OrderID integer) returns bool as '
        DECLARE
        iClient int ;
    sSql text := '''';
    t1  text := ''  -1 '' ;
    r record ;
    r2 record ;
    bInsert bool ;
    
    BEGIN
       
            bInsert = True ;
            sSql := ''select id from list_of_invoices where order_number =  '' || OrderID ||  '' '' ||  fct_getWhere(2,'' '') ;
            raise notice ''sql  = %'',sSql ;
            FOR r2 in execute(sSql)  LOOP
                raise notice ''id = %'',r2.id ;
                if r2.id > 0 then
                    bInsert = False;
                end if ;
            END LOOP ;
            
        return bInsert ;
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 

     
drop function fct_getGet_number(int) ;
CREATE OR REPLACE FUNCTION fct_getGet_number(OrderID integer) returns  int as '
 DECLARE
    iData int ;
    sSql text ;
    r2 record ;
    
    BEGIN
       iData := 0 ;
       sSql := ''select number as get_number from orderget where orderid = '' || OrderID || '' '' ||  fct_getWhere(2,'' '') ;
       
       FOR r2 in execute(sSql)  LOOP
            
            if r2.get_number is not null then 
                iData := r2.get_number ;
            else
                iData := 0 ;
            END IF ;
            
             
        END LOOP ;
     
            
     return iData ; 
       
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 
     
drop function fct_getSupply_number(int) ;
CREATE OR REPLACE FUNCTION fct_getSupply_number(OrderID integer) returns  int as '
 DECLARE
    iData int ;
    sSql text ;
    r2 record ;
    
    BEGIN
       iData := 0 ;
       
       
       sSql := ''select delivery_number as supply_number from list_of_deliveries where order_number = '' || OrderID || '' '' ||  fct_getWhere(2,'' '') ;
       raise notice '' SQL at fct_getSupply_number = % '', sSql ;
       FOR r2 in execute(sSql)  LOOP
           
            if r2.supply_number is not null then 
                iData := r2.supply_number ;
            else
                iData := 0 ;
            END IF ;
            raise notice ''iData = %'',iData ;
             
        END LOOP ;
     
            
     return iData ; 
       
    END ;
    

    
     ' LANGUAGE 'plpgsql';      

     
CREATE OR REPLACE FUNCTION  fct_getArticlePartsListForOrder(OrderID integer) returns setof record as '
 DECLARE
 
 
    sSql text ;
    r2 record ;
    rPositions record ;
    rArticlesPart record ;
    
    
    BEGIN
       
       sSql := ''select articleid  as article_id from orderposition where orderid = '' || OrderID || '' '' ||  fct_getWhere(2,'' '') ;
       
       FOR r2 in execute(sSql)  LOOP
           
           
      
            
            return next r2 ;
            
        END LOOP ;
     
            
      
       
    END ;
    

    
     ' LANGUAGE 'plpgsql';      


DROP FUNCTION  fct_getTopIDForOrder(integer);
     
CREATE OR REPLACE FUNCTION  fct_getTopIDForOrder(OrderbookID integer, sType text) returns  integer as '
    DECLARE
 
    t1 integer ;
    sSql text ;
    r2 record ;
    sDatabase return_text3 ;
    sDBInvoice text ;
    
    topID integer ;
    
    BEGIN
    
        topID := 0;
        sDatabase = fct_get_database(sType);
        sDBInvoice = sDatabase.c ;
       
    
        sSql := ''select order_top from '' || sDBInvoice || '' where orderid = '' || OrderbookID  || '' '' ||  fct_getWhere(2,'' '') ;
        raise notice ''sql = %'', sSql ;
        execute(sSql) into r2;
        
        IF found AND r2.order_top is not NULL then 
            
            topID :=  r2.order_top ;
            
        END IF ;
        
        
    
        IF topID = 0 THEN 
        
            execute  ''select addresses_misc.top_id as adr_top_id from addresses_misc,orderbook where addresses_misc.address_id = orderbook.addressnumber and orderbook.id = '' || OrderbookID || '' '' ||  fct_getWhere(2,''addresses_misc.'') INTO  t1;
       
       
            
            raise notice ''top id adr = %'', t1 ;
            
            if t1 is  not null then 
            
                if t1 > 0 then 
                    topID :=  t1;
                end if;
            end if ;
  
            
        
            
            
            
        end if ;
        
        return topID ;
        
            
      
       
    END ;
    
     ' LANGUAGE 'plpgsql';      
     

     
CREATE OR REPLACE FUNCTION  fct_getStatTaxVat() returns  setof record as '
    DECLARE
    r1 record ;
    r2 record ;
    r3 record ;
    
    iMonth int ;
    iYear int ;
    sSql text ;
    sSql2 text ;
    invoice_netto float;
    invoice_taxvat float ;
    
    br1 float ;
    br0 float ;
    BEGIN
    
        br0 = 0.00 ;
        br1 = 0.00 ;
  
        FOR i IN 0 .. 1 LOOP
            
            iMonth := date_part(''month'',current_date) - i ;
            
            iYear := date_part(''year'',current_date) ;
            if iMonth < 1 then
                iMonth = iMonth + 12 ;
                iYear = iYear -1 ;
            end if ;
            
            
            FOR  r1 in select  id, vat_value, vat_name, vat_designation,0.00 as tax_vatSum, 0.00 as sum_price_netto, i as z1 from tax_vat  LOOP
    
                sSql := ''select li.invoice_number as invoice_number,  li.date_of_invoice as li_date, '' || i || '' as z1, li.order_number  as li_orderid from list_of_invoices  as li  where  date_part(''''month'''', li.date_of_invoice) = '' ||  iMonth  || '' and date_part(''''year'''', li.date_of_invoice) = '' || iYear ||  fct_getWhere(2,'' '')   || '' order by li.invoice_number '' ; 
               
                FOR r2 in execute(sSql)  LOOP
                    invoice_taxvat := 0.00 ;
                    invoice_netto := 0.00 ;
               
                -- raise notice '' Invoice Number % Invoice Date % Order ID % '',r2.invoice_number, r2.li_date, r2.li_orderid ;

                    br0 :=   r1.tax_vatSum   +  r1.sum_price_netto ;
                     
                     
                    sSql2 := ''select ( select tax_vat_for_all_positions from orderinvoice where orderinvoice.orderid  = '' || r2.li_orderid  || '' ) as   tax_vat_for_all_positions ,   
                    orderposition.amount as amount,  orderposition.position as position, orderposition.price as price, 
                    orderposition.discount as discount, orderposition.tax_vat as position_tax_vat, 
                    (select  material_group.tax_vat from material_group,articles where  articles.material_group = material_group.id and articles.id = orderposition.articleid) as m_group_taxvat, 
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  articles.id = orderposition.articleid)
                        when true then price when false then price / (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = orderposition.articleid)) * 100  when NULL then 0.00
                    end  as end_price_netto,  
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  
                        articles.id = orderposition.articleid)  when true then price /100 * (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = orderposition.articleid)) 
                        when false then price when NULL then 0.00 
                    end as end_price_gross  
                    from  orderposition, articles, orderbook  
                    where orderbook.id = '' || r2.li_orderid  || '' and orderposition.orderid = orderbook.id and articles.id = orderposition.articleid ''  ||  fct_getWhere(2,''orderposition.'')  ;
                   
                    FOR r3 in execute(sSql2) LOOP
                        IF r3.discount IS NULL THEN 
                            r3.discount := 0.00 ;
                        END IF ;
                        
                        IF r3.position_tax_vat IS NOT NULL and  r3.position_tax_vat > 0.00  and r1.vat_value = r3.position_tax_vat THEN
                             r1.tax_vatSum :=   r1.tax_vatSum +( (r3.end_price_netto -r3.discount)  * r3.amount * r3.position_tax_vat / 100 ) ;
                             r1.sum_price_netto:=   r1.sum_price_netto +( r3.end_price_netto  * r3.amount );
                             raise notice '' position taxvat = % '', r3.position_tax_vat ;
                        ELSEIF r3.tax_vat_for_all_positions IS NOT NULL and r3.tax_vat_for_all_positions > 0 and  r3.tax_vat_for_all_positions =  r1.id  THEN
                             r1.tax_vatSum :=   r1.tax_vatSum +  ( (r3.end_price_netto -r3.discount) * r3.amount * r1.vat_value /100 );
                             r1.sum_price_netto:=   r1.sum_price_netto + ( r3.end_price_netto  * r3.amount );
                             raise notice  ''tax vatforalpositions ''  ;
                      
                        ELSEIF  r3.m_group_taxvat IS NOT NULL and r3.m_group_taxvat > 0 and   r3.m_group_taxvat= r1.id and not (r3.tax_vat_for_all_positions IS NOT NULL and r3.tax_vat_for_all_positions > 0 ) THEN
                             r1.tax_vatSum :=   r1.tax_vatSum +( (r3.end_price_netto-r3.discount)  * r3.amount * r1.vat_value / 100 );
                             r1.sum_price_netto:=   r1.sum_price_netto + (r3.end_price_netto  * r3.amount );
                            raise notice '' materialgroup taxvat '' ;
                              
                       
                        END IF ;
                       
                         -- raise notice '' Orderid % Posion ID % TaxVat %  Menge % Netto % '', r2.li_orderid,r3.position,r1.vat_value,  r3.amount,r3.end_price_netto *  r3.amount ;
                    
                        
                        
                        
                    END LOOP ;
                      br1 :=  r1.tax_vatSum   +  r1.sum_price_netto ;
                      raise notice ''  Invoice Number %           Br1 = % '',r2.invoice_number,  br1 - br0  ;
                     
                END LOOP ;
            RETURN NEXT r1;
        
            END LOOP ;
             -- raise notice '' Discount =   % '',  invoice_netto ;

        END LOOP;
    
     
       
    END ;
    
     ' LANGUAGE 'plpgsql';      

     
     
CREATE OR REPLACE FUNCTION  fct_getInvoiceGross() returns  setof record as '
    DECLARE
    r1 record ;
    BEGIN 
    
        for i in 4614 .. 4714 LOOP 
            select into r1 list_of_invoices.invoice_number,  list_of_invoices.order_number, sum(amount*price) from orderposition, list_of_invoices where list_of_invoices.invoice_number = i and orderposition.orderid = list_of_invoices.order_number group by list_of_invoices.invoice_number, list_of_invoices.order_number ;
             
            return next r1 ;
        END LOOP ;
        
     
     
          
    END ;
    
     ' LANGUAGE 'plpgsql';      

     
DROP FUNCTION fct_getOrderGifts() CASCADE  ;
CREATE OR REPLACE FUNCTION  fct_getOrderGifts() returns OPAQUE  as '
    DECLARE
    r1 record ;
    r2 record ;
    addArticleIDs text;
    iClient int ;
    aMArticles text[] ;
    i int ;
    sSql text ;
    sSql2 text ;
    bDoit bool ;
    iOrderType integer ;


    BEGIN  
        select allow_direct_debit into bDoit from addresses_misc  where address_id = NEW.addressnumber  ;
        raise notice '' allow direct debit = % '', bDoit ;
        if bDoit is not null AND bDoit = true then 
            iClient = fct_getUserDataClient(  ) ;
            addArticleIDs = fct_get_config_option(iClient,''clients.ini'', ''CLIENT_'' || iClient, ''order_add_position_gift_bank'') ;
            iOrderType = NEW.order_type;
	    if iOrderType IS NULL then 
	       iOrderType = 1 ;
	    END IF ;
 	    if iOrderType = 1 then 
                aMArticles =  string_to_array(addArticleIDs, '','') ; 
                if array_length(aMArticles, 1) >= 1 then
                FOR i IN 1..array_length(aMArticles, 1) LOOP
                
                    sSql := ''select id from orderposition where orderid = '' || NEW.id || '' and articleid = '' || aMArticles[i]  ||  fct_getWhere(2,'' '') ;
                    execute(sSql) into r1 ;
                    
                    IF r1.id is not null then 
                        raise notice '' successfull %'',r1.id ;
                    ELSE 
                        raise notice '' successfull %'',r1.id ;
                        raise notice '' We need to insert a line '';
                        sSql2 := ''select * from articles where id = '' || aMArticles[i]  || fct_getWhere(2,'' '') ;
                        raise notice '' sSql2 = %'', sSql2 ;
                        
                        execute(sSql2) into r2 ;
                        raise notice '' r2.id = %'', r2.id ;
                        if r2.id is not null then 
                            insert into orderposition (id, uuid, orderid, articleid, amount,position,price, designation) values (nextval(''orderposition_id''),fct_new_uuid() ,NEW.id, aMArticles[i]::INTEGER,1,0,r2.sellingprice1,quote_literal('' '') );
                        end if ;
                        
                        
                    END IF ;
                END LOOP; 

	      end if ;		

            END IF ;
        END IF ;
        
        RETURN NEW ;
    END ;
    
     ' LANGUAGE 'plpgsql';    
     
 
CREATE OR REPLACE FUNCTION  fct_getPositionSingleNetPrice(iOrderID int ,iPositionID int ,sType text) returns float  as '

    DECLARE
        sSql text ;
        sSql2 text ;
        netPrice float ;
        r1 record ;
        
    BEGIN
  
        netPrice := 0.00 ;
        
        sSql := ''select *  from  fct_getPositionNetPrice('' || iOrderID || '', '' || iPositionID || '', '' || quote_literal(sType) || '') as (tax_vat_all_postion int, amount float , position int, price float, discount float, pos_tax_vat float, group_tax_vat int, net float , br float ) '' ;
        raise notice ''sSql = % '',sSql ;
       
        execute (sSql) into r1 ;
       
        if r1.net is not null then 
            netPrice := r1.net ;
        end if ;
       
        
        return netPrice ;
        
      END ;
    
     ' LANGUAGE 'plpgsql';   
DROP FUNCTION fct_getPositionNetPrice(int, int, text);

CREATE OR REPLACE FUNCTION  fct_getPositionNetPrice(iOrderID int ,iPositionID int ,sType text) returns record  as '
    DECLARE
    r1 record ;
    addArticleIDs text;
    iClient int ;
    aMArticles text[] ;
    i int ;
    sSql text ;
    sSql2 text ;
    bDoit bool ;
    
   
    sDatabase return_text3;
    sDB text ;
    sDBPositions text ;
     sDBInvoice text ;
    BEGIN
    
    sDatabase = fct_get_database(sType);
    sDB = sDatabase.a ;
    sDBPositions = sDatabase.b ;
    sDBInvoice = sDatabase.c ;
    
 sSql2 := ''select ( select tax_vat_for_all_positions from '' || sDBInvoice || '' where '' || sDBInvoice || ''.orderid  = '' || iOrderID || '' ) as   tax_vat_for_all_positions ,   
                    '' || sDBPositions || ''.amount as amount,  '' || sDBPositions || ''.position as position, '' || sDBPositions || ''.price as price, 
                    '' || sDBPositions || ''.discount as discount, '' || sDBPositions || ''.tax_vat as position_tax_vat, 
                    (select  material_group.tax_vat from material_group,articles where  articles.material_group = material_group.id and articles.id = '' || sDBPositions || ''.articleid) as m_group_taxvat, 
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  articles.id = '' || sDBPositions || ''.articleid)
                        when true then price when false then price / (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = '' || sDBPositions || ''.articleid)) * 100  when NULL then 0.00
                    end  as end_price_netto,  
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  
                        articles.id = '' || sDBPositions || ''.articleid)  when true then price /100 * (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = '' || sDBPositions || ''.articleid)) 
                        when false then price when NULL then 0.00 
                    end as end_price_gross  
                    from  '' || sDBPositions || '', articles, '' || sDB || ''  
                    where '' || sDB || ''.id = '' || iOrderID  || '' and '' || sDBPositions || ''.orderid = '' || sDB || ''.id and articles.id = '' || sDBPositions || ''.articleid and '' || sDBPositions || ''.id = '' ||iPositionID || '' ''  ||  fct_getWhere(2, sDBPositions || ''.'')  ;
                   
            
        execute (sSql2) into r1 ;
        raise notice ''sSql2 = % ergibt % '',sSql2, r1 ;
        return r1 ;
        
      END ;
    
     ' LANGUAGE 'plpgsql';   


CREATE OR REPLACE FUNCTION  fct_getCDL(searchfields text[], sType text) returns setof record  as '
    DECLARE  
    rData record ;
    sSql text ;
    total_sum float ;


    BEGIN
	-- searchfields 
	-- 0, 1 = cash_date
	

	-- sType :
	-- AllValues, AllDaily, AllUser, UserDaily

	total_sum := 0.0 ;
	raise notice '' searchfields = % % '',searchfields[1],searchfields[2] ;
	sSql = '' select id, address_id, cash_desk_number,  order_id, cash_time, cash_desk_user_short_cut, 

	case order_sum
	     when 0 then incomming_total  
	     else order_sum
	     end  as incomming_sum ,	     

	incomming_total, cash_date, cash_procedure , description, 
	case 
	     when address_id > 0 then (select lastname || '''',  '''' || city from address where id = address_id)
	     else '''' '''' 
	end as address ,

	0.0::float as r1, 0.0::float as r2, 0.0:: float as r3, 0.0:: float as r4 

	from cash_desk where cash_date between '' || quote_literal(searchfields[1]) || '' and '' || quote_literal(searchfields[2]) ;



 
	if sType = ''AllValues'' then 
	      sSql = sSql || '' order by cash_date,  cash_time '' ;
	elseif sType =  ''AllDaily''  then
	     sSql = sSql || '' order by cash_date,  cash_time  '' ;

	elseif sType =  ''AllDaily''  then
	     sSql = sSql || '' order by  cash_desk_user_short_cut, cash_date,  cash_time '';

	elseif sType =  ''AllDaily''  then
	     sSql = sSql  || '' order by  cash_desk_user_short_cut, cash_date,  cash_time '';

	end if ;

	 

	raise notice '' sSQl = % '', sSql ; 

	FOR rData in execute(sSql)  LOOP
           total_sum = total_sum + rData.incomming_sum ;

	   if sType = ''AllValues'' then 
	    
           elseif sType =  ''AllDaily''  then


	   elseif sType =  ''AllDaily''  then


	   elseif sType =  ''AllDaily''  then


	   end if ;


	     raise notice ''sums %, % '',  total_sum , rData.incomming_sum ;
	     rData.r1 = total_sum ;
	     return next rData ;
            
        END LOOP ;


	 

    END ;
    
     ' LANGUAGE 'plpgsql';   
