

CREATE OR REPLACE FUNCTION fct_get_dms_document_image(dms_id int) returns text as $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
        
        BEGIN
          
      
    
            select into iPaired_ID paired_id from dms where id = dms_id ;
            if iPaired_ID is null then 
                iPaired_ID = 0 ;
            end if ;
            
            if iPaired_ID > 0 then 
                iSearchID := iPaired_ID ;
            else
                iSearchID := dms_id ;
            end if ;
            
            sSql := ''select document_image from dms where id = '' || iSearchID ;
            -- set later more restrictions
            
            execute(sSql) into sImage ;
            
            return sImage ;
            
        END ;
    $$ LANGUAGE 'plpgsql'; 
 
CREATE OR REPLACE FUNCTION fct_ExtractTextFromDMS(dms_id int) returns text as $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sText text ;
        sSql text ;
        
        BEGIN
          
      
    
            select into iPaired_ID paired_id from dms where id = dms_id ;
            if iPaired_ID is null then 
                iPaired_ID = 0 ;
            end if ;
            
            if iPaired_ID > 0 then 
                iSearchID := iPaired_ID ;
            else
                iSearchID := dms_id ;
            end if ;
            
            sSql := ''select dms_extract from dms where id = '' || iSearchID ;
            -- set later more restrictions
            
            execute(sSql) into sText ;
            
            if sText is null then 
                sText = ''NONE'' ;
            end if ;
                
            return sText ;
            
        END ;
    $$ LANGUAGE 'plpgsql'; 
    

