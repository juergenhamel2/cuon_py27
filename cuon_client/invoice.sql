CREATE OR REPLACE FUNCTION fct_isPaidInvoice(invoice_id int) returns boolean AS $$
 DECLARE
     bPaid boolean ;
    r record;
    sSql text := $$'$$';

    BEGIN
       
        bPaid := true ;
        
        sSql := $$'select inpayment from in_payment where invoice_number  = trim( $$' || quote_literal(to_char(invoice_id,$$'9999999999$$')) || $$') $$'  || fct_getWhere(2,$$'in_payment.$$') ;

        execute(sSql) into r ;
      
         if r.inpayment is null then 
            bPaid := false ;
        end if ;
        
        
         return bPaid ;

      
        
    END ;
    

    
     $$ LANGUAGE 'plpgsql'; 




CREATE OR REPLACE FUNCTION fct_getLastYearIncomming(adr_id int) returns record AS $$
       DECLARE
	r record ;
       	sSql text ;
        lastyear int;
	dNow date ;
	iClient int;
	giftBonus float;
	percentBonus float ;

       BEGIN

        iClient = fct_getUserDataClient(  ) ;
	giftBonus = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'order_gift_bonus$$' ) ;	
	percentBonus = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'order_percent_bonus$$' ) ;
	if giftBonus is null then 
	   giftBonus = 0.00 ;
	end if ;
	if percentBonus is null then 
	   percentBonus = 0.00 ;
	end if ;


	raise notice $$' gift/percent = %, % $$', giftBonus,percentBonus ;

	
        select now() into dNow ;

        lastyear = fct_getlastYear(dNow) ;
	raise notice $$' last year = % $$',lastyear ;
	sSql := $$'select 0.00::float as total_sum,  fct_round_2_digits( sum(in_payment.inpayment)) as incomming_sum , 0.00::float as total_bonus, fct_round_2_digits(  sum(in_payment.inpayment) * $$' || percentBonus || $$' + $$' || giftBonus || $$')as incomming_bonus ,$$'$$'0.00$$'$$'::text as total_sum_string, $$'$$'0.00$$'$$'::text as incomming_sum_string,  $$'$$'0.00$$'$$'::text as  total_bonus_string, $$'$$'0.00$$'$$'::text as incomming_bonus_string  from in_payment,orderbook where in_payment.order_id = orderbook.id and orderbook.addressnumber = $$' || adr_id || $$' and in_payment.status != $$' || quote_literal($$'delete$$') || $$' and orderbook.status != $$' || quote_literal($$'delete$$') || $$' and orderbook.client = $$' || iClient || $$'  and in_payment.client = $$' || iClient || $$' and extract(year from date_of_paid) = $$' || lastyear  ;


       execute(sSql) into r ;
	if r.total_sum is null then
	   r.total_sum = 0.00 ;
	   r.total_bonus = fct_round_2_digits( 0.00 + giftBonus ) ;
	end if ;

	if r.incomming_sum is null then 
	    r.incomming_sum = 0.00;
	    r.incomming_bonus =  fct_round_2_digits(0.00 + giftBonus) ;

	end if ;
       	return  r ;
       END ;
    

    
$$ LANGUAGE 'plpgsql'; 
